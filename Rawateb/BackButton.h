//
//  BackButton.h
//  Rwewtab
//
//  Created by Rohit Mishra on 4/18/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BackButton : UIView
{
    UIButton *backButton;
    UIView *view;
    
    
}
@property (nonatomic, strong) NSMutableArray *stack;

-(void) previousView :(UIView *)view1;
+ (id)sharedManager;

-(void) goBack : (UIView *)vieww :(NSString *)category;
-(UIView *) back;



@end
