//
//  SharingImageView.h
//  Rawateb
//
//  Created by Neuerung on 5/19/16.
//  Copyright (c) 2016 Rohit Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SharingImageView : UIView
{
    UIView *sharingView;
    
    //cancel recognizer
    
    UITapGestureRecognizer *cancle_recognizer;
    
    
    
    //depositing icon
    UIImageView *deposite;
    
    //MOney
    
    UIImageView *money;
    
    
    //sharing image view
    UIImageView *sharee;
    
    
    
}
-(id) initWithFrame:(CGRect)frame :(NSString *)sharingstring;
@property (nonatomic, strong)NSString *salaryiscredited;

@end
