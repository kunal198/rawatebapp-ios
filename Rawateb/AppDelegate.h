//
//  AppDelegate.h
//  Rwewtab
//
//  Created by Neuerung on 4/8/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OneSignal/OneSignal.h>
#import "MBProgressHUD.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    UIImageView *backgroundImageView;
    UIImage *image;
    
    MBProgressHUD *pregresss;
    
}
//+(NSString *)get:(NSString *)key alter:(NSString *)alternate;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *dToken;
@property (strong, nonatomic) OneSignal *oneSignal;

@end

