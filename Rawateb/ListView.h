//
//  ListView.h
//  Rwewtab
//
//  Created by Neuerung on 4/29/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface ListView : UIView<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,UISearchDisplayDelegate, UIAppearance>

{
    UITableView *bankslist;
    NSDictionary *cate_list;
    NSArray *cate_array;
    
    //bank
    NSDictionary *bank_list;
    NSArray *banks_array;
    
    //Progress for bank list
    MBProgressHUD *hud;
    UILabel *searchCrasherror;
    
    
    //Components for searching in uitableview
    NSMutableArray *filteredResult;
    NSMutableArray *tableData;
    
    //To Dissmiss Keyboard
    UITapGestureRecognizer *dissmiss;
    
    
    
//    UISearchController *searchDisplayController;
    
    MBProgressHUD *progress;
    
    
    //bool variable fo r changing the data in the table view
    
    
    BOOL isFiltered;
    
    //recruiter list
    NSDictionary *graduate_list;
    
    
    NSArray *graduate_array;
    
    
    NSString *local_icon_image_path;
    
    
    NSDictionary *response_ictionary_after_bank;
    
    
    NSArray *act_cae_array ;
    
    
    
}
-(id) initWithFrame:(CGRect)frame catagory:(NSString *)cataagory;
@property(nonatomic, strong) NSString *catagory;
@property (nonatomic, strong) NSArray *banks_list;
@property (nonatomic, strong) NSArray *cates_array;
@property(nonatomic,strong) UISearchBar *searchbar;
@property (nonatomic, strong) NSArray *graduates_array;
@property (nonatomic, strong) NSMutableArray *graduateSearch;
@property (nonatomic)BOOL internal_calling_recruiter_flag;

-(void) setSearchArray ;


@end
