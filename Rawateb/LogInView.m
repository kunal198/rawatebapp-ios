//
//  LogInView.m
//  Rwewtab
//
//  Created by Neuerung on 4/8/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import "LogInView.h"
#import <sqlite3.h>
#import "BankList.h"
#import "MBProgressHUD.h"
#import "Dashboard.h"
#define GET_CATE @"http://admin.elatikprojects.com/api/v1/get_cate"
#define REGISTER @"http://admin.elatikprojects.com/api/v1/register"
#define BACKGROUND @"http://elatikprojects.com/uploads/KuwaitNL.jpg"
#define kAuthenticationendpoint @"http://api.instagram.com/oauth/authorize/?"
#define DEVICEINFO @"http://admin.elatikprojects.com/api/v1/deviceInfo"
BOOL isapplogin;

@implementation LogInView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id) initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if(self)
    {
        //[self setBackgroundColor:[UIColor whiteColor]];
        UIImageView *backgroundImageView=[[UIImageView alloc]initWithFrame:self.bounds];
        NSString *postRegisterUser = [NSString stringWithFormat:@"%@", BACKGROUND];
        
        
        
        
        NSString *LocalPathForImage = [NSString stringWithFormat:@"%@/%@", kDocumentDirectoryPath,[MyPref valueForKey:@"actualBackgroundImagePath"]];
//
        if (![[NSFileManager defaultManager] fileExistsAtPath:LocalPathForImage]) {
//
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
        
        [request setURL:[NSURL URLWithString:postRegisterUser]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"aplication/json" forHTTPHeaderField:@"Content-Type"];
        
        
       
        
        NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
            
            
            NSLog(@"response from server is in login view:%@", response);
            dispatch_async(dispatch_get_main_queue(), ^{
                if (data) {
                    image=[UIImage imageWithData:data];
                }
                
                
                backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
                backgroundImageView.clipsToBounds = YES;
                backgroundImageView.center = [[UIApplication sharedApplication]keyWindow].rootViewController.view.center;
                backgroundImageView.image= image;
//                backgroundImageView.image= image;
                
                
                
                
                [self addSubview:backgroundImageView];
                
                
                UIImageView *appicon = [[UIImageView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*40, ([[UIScreen mainScreen] bounds].size.height/100)*20, ([[UIScreen mainScreen] bounds].size.width/100)*20, ([[UIScreen mainScreen] bounds].size.width/100)*20)];
                
                
                appicon.contentMode = UIViewContentModeScaleAspectFit;
                
                
                appicon.image = [UIImage imageNamed:@"app_icon.png"];
                [self addSubview:appicon];
                
                [self logIn];
                
            
            });
            
         
            
            
        }]resume];
        }
                else{
            
            
            image = [UIImage imageWithContentsOfFile:LocalPathForImage];
            
                    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
                    backgroundImageView.clipsToBounds = YES;
                    backgroundImageView.center = [[UIApplication sharedApplication]keyWindow].rootViewController.view.center;
                    backgroundImageView.image= image;
                    
                    [self addSubview:backgroundImageView];
                    
                    UIImageView *appicon = [[UIImageView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*40, ([[UIScreen mainScreen] bounds].size.height/100)*20, ([[UIScreen mainScreen] bounds].size.width/100)*20, ([[UIScreen mainScreen] bounds].size.width/100)*20)];
                    
                    appicon.contentMode = UIViewContentModeScaleAspectFit;
                    
                    
                    appicon.image = [UIImage imageNamed:@"app_icon.png"];
                    [self addSubview:appicon];
                    
                    [self logIn];
            
            
        }
        
        
        
        NSLog(@"LOG IN view");
        
    }
    return  self;
    
}


-(void) logIn
{
 
    
    
    
    
    //Login Button
    UIButton *LogIN=[[UIButton alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*30, ([[UIScreen mainScreen] bounds].size.height/100)*66, ([[UIScreen mainScreen] bounds].size.width/100)*35, ([[UIScreen mainScreen] bounds].size.width/100)*12)];
    
    LogIN.center = CGPointMake(self.bounds.size.width/2, ([[UIScreen mainScreen] bounds].size.height/100)*68);
    
    
    
    LogIN.backgroundColor=[UIColor colorWithRed:99/255.0f green:184/255.0f blue:255/255.0f alpha:1.0f];
    
    [LogIN setTitle:NSLocalizedString(@"Log In", nil) forState:UIControlStateNormal];
    
    [LogIN addTarget:self action:@selector(logiingin) forControlEvents:UIControlEventTouchUpInside];
    
    
        
        

   
    
    LogIN.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:textSizeLarge];
    
    
    LogIN.titleLabel.tintColor=[UIColor whiteColor];
    [self addSubview:LogIN];
    
    //Instagram login Button
    
  /*  UIButton *InstagramLogIN=[[UIButton alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*10, ([[UIScreen mainScreen] bounds].size.height/100)*80, ([[UIScreen mainScreen] bounds].size.width/100)*80, ([[UIScreen mainScreen] bounds].size.width/100)*12)];
    
    [InstagramLogIN setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"insta_login_btn" ofType:@"gif"]] forState:UIControlStateNormal];
    
    
//    [InstagramLogIN setTitle:@"Log In" forState:UIControlStateNormal];
    
    
    InstagramLogIN.titleLabel.tintColor=[UIColor whiteColor];
    
    
    
        [InstagramLogIN addTarget:self action:@selector(instagramLogIn) forControlEvents:UIControlEventTouchUpInside];
    
    
//    [self addSubview:InstagramLogIN];*/
    
    UIView *textmailview = [[UIView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*10, ([[UIScreen mainScreen] bounds].size.height/100)*50, ([[UIScreen mainScreen] bounds].size.width/100)*80, ([[UIScreen mainScreen] bounds].size.width/100)*12)];
    
    textmailview.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.4f];
    
    [self addSubview:textmailview];
    
    
    
    //Email Text Box
    email=[[UITextView alloc]initWithFrame:textmailview.bounds];
    
    CGRect emailframe = email.frame;
    
    
    
    emailframe.size.width = (textmailview.bounds.size.width/100)*83;
    
    emailframe.origin.y = emailframe.origin.y + (textmailview.bounds.size.height/100)*6;
    
    
    email.frame = emailframe;
    
    
    
    UIImageView *emailMSg=[[UIImageView alloc]initWithFrame:CGRectMake((textmailview.bounds.size.width/100)*83, (textmailview.bounds.size.height/100)*15, (textmailview.bounds.size.width/100)*13, (textmailview.bounds.size.height/100)*70)];
    emailMSg.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_mail" ofType:@"png"]];
    
    
    email.font=[UIFont fontWithName:@"Helvetica" size:textSizeLarge];
    
    
    
    emailMSg.contentMode = UIViewContentModeScaleAspectFit;
    
    //Adding placeholdertext
    email.text =NSLocalizedString(@"Email Id", nil);
    email.textColor =[UIColor lightGrayColor];
    email.delegate =self;
    email.textAlignment = NSTextAlignmentRight;
    
    
    
    email.backgroundColor =[UIColor clearColor];
    [textmailview addSubview:emailMSg];
    
    
    [textmailview addSubview:email];
    email.alpha =1.0f;
    
    
    
    
    
    
    
}

-(BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
//    NSLog(@"started writing code");
    
    //[textView becomeFirstResponder];
//    [email becomeFirstResponder];
    
    email.text = @"";
    email.textColor = [UIColor whiteColor];
    return YES;
    
}


-(void) textViewDidChange:(UITextView *)textView
{
    if (email.text.length ==0) {
        email.textColor  = [UIColor lightGrayColor];
        email.text = NSLocalizedString(@"Email Id", nil);
        
        NSLog(@"textfield appeared");
        
        
        [email resignFirstResponder];
        
    }
    
    if ([email.text isEqualToString:@"\n"]) {
        [email resignFirstResponder];
        
    }
    
}


- (void)textViewDidEndEditing:(UITextView *)textView;
{
    
   // [textView resignFirstResponder];
    
    
    
    
}


-(BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [email resignFirstResponder];
        return NO;
        
    }
    
    
    
    return YES;
    
}



-(void) logiingin
{
    
    [email resignFirstResponder];
    
  BOOL log =  [self NSStringIsValidEmail:email.text];
    
    NSLog(@"checking for email %d", log);
    
    if (log)
    {
        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"LoginDone"];
        [[BankList sharedManager]addView:nil :@"BANK LIST"];
        
        MBProgressHUD *progress = [[MBProgressHUD alloc]init] ;//]WithWindow:[[UIApplication sharedApplication] keyWindow].rootViewController.view ];
        progress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
        
        
        progress.labelText = @"Processing..";
        
        
        
        //Sending data to the server of login
        NSString *postRegisterUser = [NSString stringWithFormat:@"%@", REGISTER];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
        
        [request setURL:[NSURL URLWithString:postRegisterUser]];
        [request setHTTPMethod:@"POST"];
        
        
        NSDictionary *params = [[NSDictionary alloc]init ];
        
        params = [NSDictionary dictionaryWithObjectsAndKeys:@"xyz",@"name",email.text,@"email" ,nil];
        
        [MyPref setValue:email.text forKey:@"email"];
        [MyPref synchronize];
        
        
        NSString *BoundaryConstant = @"a1u4orfleyuo2ruo3h4";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        
        for (NSString *param in [params allKeys]) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/json\r\n\r\n"dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[params objectForKey:param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData: [@"\r\n"dataUsingEncoding:NSUTF8StringEncoding]];
        }
       
        [request setHTTPBody:body];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        NSLog(@"request body is :%@", [[NSString alloc]initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
        
        
        
        NSLog(@"the request for server for login is :%@", [request allHTTPHeaderFields] );
        
        
        
        
        NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
            NSString *dataFromLogIn = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
           
            
            NSDictionary *loginresponse;
            
            if (!data) {
                NSLog(@"data is nil");
                
            }
            else{
            loginresponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            }
            
            
            
            NSLog(@"response from server is :%@", response);
            
             NSLog(@"login dictionary is:%@",loginresponse);
            
            
            NSDictionary *userinfo = [loginresponse valueForKey:@"user"];
            
            
            NSArray *bank = [userinfo valueForKey:@"bank"];
            NSArray *category = [userinfo valueForKey:@"category"];
            
            NSLog(@"bank and category array is ;%@ and :%@", bank, category);
            
            
            
            [MyPref setValue:[bank lastObject] forKey:@"bankname"];
            [MyPref synchronize];
            
            NSLog(@"bank name for this mail id is :%@", [MyPref valueForKey:@"bankname"]);
            
            
            
            [MyPref setValue:[category lastObject] forKey:@"category_hindi"];
            [MyPref synchronize];
            NSLog(@"category from the server in first time is :%@", [MyPref valueForKey:@"category_hindi"]);
            
            
            
            NSLog(@"userinfo dictionary is :%@", userinfo);
            
            NSLog(@"the key for the new user is :%@", [userinfo valueForKey:@"isNewUser"]);
            
            
            NSArray *newUser = [userinfo valueForKey:@"isNewUser"];
            
            NSString *userstring = [NSString stringWithFormat:@"%@", [newUser lastObject]];
            
            
            NSLog(@"user string is :%@", userstring);
            
            
            BOOL check = (BOOL)[newUser lastObject];
            
            NSLog(@"value of check is :%d and last object of the array is %@", check, [newUser lastObject]);
            
            
            NSString *userin = check ? @"true" : @"false";
            if ([userstring isEqualToString:@"0"]) {
                userin  =[[NSString alloc]initWithFormat:@"0"];
                
            }
            else if([userstring isEqualToString:@"1"])
            {
                userin  =[[NSString alloc]initWithFormat:@"1"];
            }
            [MyPref setValue:userin forKey:@"isNewUser"];
            [MyPref synchronize];
            
            
            
            //api key for the device is :
            
            newUser = nil;
            newUser = [userinfo valueForKey:@"api_key"];
            
            
            [MyPref setValue:[newUser lastObject] forKey:@"api_key"];
            [MyPref synchronize];
            
            
            //saving activation key for the new user
            
            
            
            [MyPref setValue:[userinfo valueForKey:@"activation_key"] forKey:@"activation_key"];
            [MyPref synchronize];
            
            
            
            //saving  the id for the user
            
           
            
            [MyPref setValue:[userinfo valueForKey:@"id"] forKey:@"id"];
            [MyPref synchronize];
            
            
            //email for the new user
            
           
            
            [MyPref setValue:[userinfo valueForKey:@"email"] forKey:@"email"];
            [MyPref synchronize];
            
            
            
            
            BOOL user = true;
            
           
            NSLog(@"wehave some new user info%@ and ture is :%d",[MyPref valueForKey:@"isNewUser"],(BOOL)user);
            
            
            if ([[MyPref valueForKey:@"isNewUser"]  isEqualToString:@"1"]) {
                NSLog(@"we have a new user%@", [MyPref valueForKey:@"isNewUser"]);
                
            }
            else{
                NSLog(@"user is already registered%@", [MyPref valueForKey:@"isNewUser"]);
                
            }
            
//            dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
            
            [request setURL:[NSURL URLWithString:DEVICEINFO]];
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[MyPref valueForKey:@"api_key"] forHTTPHeaderField:@"Authorization-Basic"] ;
            
            NSLog(@"actual request is :%@", [request allHTTPHeaderFields]);
            //Setting body for the request
            
            
            
            
            
            
            
            
            
            
            
            NSMutableData *body = [NSMutableData data];
            
            
            NSString *bodydata = [NSString stringWithFormat:@"imei=0&token=%@&OSType=iOS&mac=%@",[MyPref valueForKey:@"DevToken"],VendorID];
            
            
            
            
            
            
            
            [body appendData:[bodydata dataUsingEncoding:NSASCIIStringEncoding]];
            
            
            
            
            [request setHTTPBody:body];
            
            NSString *bodyofrequest = [[NSString alloc]initWithData:body encoding:NSUTF8StringEncoding];
            
            
            NSLog(@"request body is:%@", bodyofrequest);
            
            NSLog(@"actual request is :%@", [request allHTTPHeaderFields]);
            
            
            
            
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            
            
            
            //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
            
            
            NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
                
                NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                
                
                
                NSLog(@"response from server is :%@ \nand data from the server is%@", response,
                      data_server);
                
                
                
                
            }]resume];

            
                
                
                if ([[MyPref valueForKey:@"isNewUser"]  isEqualToString:@"1"]) {
//                    [self removeFromSuperview];
                    
                    Dashboard *bank_salary = [[Dashboard alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
                    
                    
                    [MyPref setValue:@"1" forKey:@"selection"];
                    [MyPref synchronize];
                    
                    
                    [bank_salary bank_salary];
                    
                    [MyPref setValue:@"1" forKey:@"login"];
                    [MyPref synchronize];
                    
                    //first time login only
                    
                    [MyPref setValue:@"1" forKey:@"firstlogin"];
                    [MyPref synchronize];
                    
                    
                    
                    
                }
                else{
//                    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isapplogin"];
                    BankList *list=[[BankList alloc]initWithFrame:self.bounds];
                    [self addSubview:list];
                    
                    Dashboard *bank_salary = [[Dashboard alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
                    
                    
                    [MyPref setValue:@"0" forKey:@"selection"];
                    [MyPref synchronize];
                    
                    
                    [bank_salary home];
                    
                    [MyPref setValue:@"1" forKey:@"login"];
                    [MyPref synchronize];
                    
                    
                    
                }
//            }
//            else{
//                
//                BankList *list=[[BankList alloc]initWithFrame:self.bounds];
//                [self addSubview:list];
//                
//            }

            
            
            
//            });
            NSLog(@"data from the server is :%@",dataFromLogIn);
            dispatch_async(dispatch_get_main_queue(), ^{
                          progress.hidden = YES;
            
            
            [progress removeFromSuperview];
            
            });
            
            
        }]resume];
        
        
        
      
        
        
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    documentsDirectory =[documentsDirectory stringByAppendingPathComponent:@"login"];
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:NO attributes:nil error:nil];
        
    }
    
    
    [self createEditableCopyOfDatabaseIfNeeded:@"login"];
    
    documentsDirectory =[documentsDirectory stringByAppendingPathComponent:@"RwewtabDatabase.sqlite"];
    
    //Saving user emailid into the database
    sqlite3 *contactDB;
    sqlite3_stmt *statement=NULL;
    
    
    const char *dbpath=[documentsDirectory UTF8String];
    
    if(sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
    {
    
        char *err;
        
        const char *sql_stmt ="SELECT name FROM sqlite_master WHERE type='table' AND name='LogIn'";
        if (sqlite3_prepare_v2(contactDB, sql_stmt, -1, &statement, NULL) != SQLITE_OK)
        {
            
            
            NSLog(@"Table does not exists in tha database");
            
            
            
           
            
        NSLog(@"the error of the execution statement in creating table is :%d",sqlite3_prepare_v2(contactDB, sql_stmt, -1, &statement, NULL) );
        
        
        sqlite3_close(contactDB);
        NSLog(@"We are unable to create table in the database");
        
        // sqlite3_finalize(sql_stmt );
        NSAssert(0, @"Table failed to create.");
    }
    else{
        NSLog(@"Table Exist in the database is %d", sqlite3_prepare_v2(contactDB, sql_stmt, -1, &statement, NULL));
        
        
        const char *sql_stmt3 =
        "CREATE TABLE IF NOT EXISTS LogIn(Id INTEGER PRIMARY KEY, Email TEXT)";
        
        if (sqlite3_exec(contactDB, sql_stmt3, NULL, NULL, &err) != SQLITE_OK) {
            NSLog( @"Failed to create table with error %d",sqlite3_exec(contactDB, sql_stmt3, NULL, NULL, &err) );
            
        }
        
        
        
        else
        {
            
            NSLog(@"Table is successfully created");
            sqlite3_close(contactDB);
            sqlite3_finalize(statement);
            
            
            
        }
        
        
        
    }
    }
        
        NSLog(@"Email id is :%@", email.text);
        
        //Inserting Email id into database
        sqlite3_stmt *statement3=NULL;
        
        if(sqlite3_open([documentsDirectory UTF8String], &contactDB) == SQLITE_OK)
        {
            NSLog(@" opened database to insert data");
            NSString *insert=[NSString stringWithFormat:@"INSERT INTO LogIn (Id , Email) VALUES('%@', '%@')", @"1", email.text];
            NSLog(@"inserted data with query is :%@", insert);
            
            
            
            if (sqlite3_prepare_v2(contactDB, [insert UTF8String], -1, &statement3, NULL) == SQLITE_OK) {
        
        
                NSLog(@"inseted successfully");
                
        
    
    
                }
        }
                else
                    {
    NSLog(@"Failed to open database at %s with error %s", dbpath, sqlite3_errmsg(contactDB));
    sqlite3_close (contactDB);
    sqlite3_finalize(statement);
}

       // }
    
    
//    for (UIView *view in [self subviews]) {
//        if (![view isKindOfClass:[UIImageView class]]) {
//            [view removeFromSuperview];
//            
//        }
//        else
//        {
//            [view removeFromSuperview];
//             
//        }
//    }
        
//    [list popoverWindow];
     }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"ERROR" message:@"Please enter email id in correct form" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        
    }
    
    


}


- (void)createEditableCopyOfDatabaseIfNeeded:(NSString *)dir2{
    // First, test for existence.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSLog(@"document directory path is  == :%@", documentsDirectory);
    
    
    
    if (dir2) {
        NSLog(@"Directory in dir is: == %@", dir2);
        documentsDirectory = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",dir2]];
        
        NSLog(@"now document directory path is :Directory in dir is: == %@", documentsDirectory);
        
    }
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"RwewtabDatabase.sqlite"];
    
    NSLog(@"writable path after creating database i s:%@", writableDBPath);
    
    success = [fileManager fileExistsAtPath:writableDBPath];
    if (success) {//database already exist
        
        NSLog(@"We are in success");
        
        
        
        return;
    }
    else
    {
        NSLog(@"Creating a new database at the path ");
        
    }
    
    // The writable database does not exist, so copy the default to the appropriate location.
  
        success = [[NSFileManager defaultManager] createFileAtPath:writableDBPath contents:nil attributes:nil];
    if (!success) {
        NSLog(@"\n\n ********* DB NOT CREATED ************\n\n");
        NSLog(@"%@",[error localizedDescription]);
    }
}


//Checking for valid mail
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    NSLog(@"check string is :%@", checkString);
    
    
    
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}




+(void) addView :(UIView *)view
{
    LogInView *list = [[LogInView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    
    
    [[[UIApplication sharedApplication] keyWindow].rootViewController.view addSubview:list];
    
    
    
    
    
}




//Network request delegates


//adding dictionary to the body of request

- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters
                             paths:(NSArray *)paths
                         fieldName:(NSString *)fieldName
{
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    // add image data
    
    for (NSString *path in paths) {
        NSString *filename  = [path lastPathComponent];
        NSData   *data      = [NSData dataWithContentsOfFile:path];
//        NSString *mimetype  = [self mimeTypeForPath:path];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldName, filename] dataUsingEncoding:NSUTF8StringEncoding]];
//        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

- (NSString *)generateBoundaryString
{
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];

}


-(void) instagramLogIn
{
    NSLog(@"we are in instagram login");
//
    
    UITapGestureRecognizer *hidewebview = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hiding)];
    
    
    backwebview = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    [self addSubview:backwebview];
    backwebview.backgroundColor = [UIColor clearColor];
   
    
    UIImageView *webimage = [[UIImageView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    
    webimage.backgroundColor = [UIColor grayColor];
    webimage.alpha = 0.4f;
    webimage.userInteractionEnabled = YES;
    
    
    [backwebview addSubview:webimage];
    
    [backwebview addGestureRecognizer:hidewebview];
    
    [webimage addGestureRecognizer:hidewebview];
    
    
    self.webview = [[UIWebView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*10, ([[UIScreen mainScreen] bounds].size.height/100)*0, (webimage.bounds.size.width/100)*80, (webimage.bounds.size.width/100)*130)];
    
//    [[[UIApplication sharedApplication] keyWindow].rootViewController.view addSubview:self.webview];
    self.webview.backgroundColor = [UIColor whiteColor];
    
    self.webview.delegate =self;
    
    self.webview.clipsToBounds = YES;
    [self.webview sizeToFit];
    
    
    
    
    self.webview.center = [[UIApplication sharedApplication] keyWindow].center;
    
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@client_id=%@&redirect_uri=%@&response_type=code",kAuthenticationendpoint,@"b50246e022a34731aa499c6594b48849",@"http://elatikprojects.com/" ]]];
    //@"7d2a650122b44ebd8e58b47b18b30621"
    
    [self.webview loadRequest:request];
    
    
    [backwebview addSubview:self.webview];

    
    
    // Now we need to hit the aopi of login
    
    
    
    
    
    
    
}



- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
    NSLog(@"view started loading the instagram api");
    
    
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"webview finished loading the url");
    
    
    
    if (![self.webview isLoading]) {
        NSLog(@"webview actually finished loading now");
        
    }
    
}




-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
     [receivedData appendData:data];
}





- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    
    NSLog(@"url of the current request is :%@", request.URL);
    
    NSString *reqString = [NSString stringWithFormat:@"%@",[request URL]];
    
    
    
    if ([reqString  hasPrefix:@"http://elatikprojects.com"]) {
        
        // Extract oauth_verifier from URL query
        NSString* verifier = nil;
        NSArray* urlParams = [[[request URL] query] componentsSeparatedByString:@"&"];
        for (NSString* param in urlParams) {
            NSArray* keyValue = [param componentsSeparatedByString:@"="];
            NSString* key = [keyValue objectAtIndex:0];
            if ([key isEqualToString:@"code"]) {
                verifier = [keyValue objectAtIndex:1];
                break;
            }
        }
        NSLog(@"verifier is :%@", verifier);
        
        
        if (verifier) {
            
            NSString *data = [NSString stringWithFormat:@"client_id=%@&client_secret=%@&grant_type=authorization_code&redirect_uri=%@&code=%@",@"b50246e022a34731aa499c6594b48849",@"8beae924369d43d38d2717e3775203ad",@"http://elatikprojects.com/",verifier];
            
            NSLog(@"data string is:%@", data);
            
            
            
            
            NSString *url = [NSString stringWithFormat:@"https://api.instagram.com/oauth/access_token"];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
            NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
            receivedData = [[NSMutableData alloc] init];
        } else {
            // ERROR!
        }
        
        [webView removeFromSuperview];
        
        return NO;
    }
    
    
    
    
    
    
    
    return YES;
    
}




- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:[NSString stringWithFormat:@"%@", error]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //    NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    NSDictionary *tokenData = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableContainers error:nil];
    //  WebServiceSocket *dconnection = [[WebServiceSocket alloc] init];
    //   dconnection.delegate = self;
    
    if (tokenData !=nil) {
        
        
        
        NSLog(@"token data in request is :%@", tokenData);
        
        NSLog(@"maill from insta gram is :%@", [tokenData valueForKey:@"user"]);
        
        
        NSDictionary *user=[tokenData valueForKey:@"user"];
        
        
        
        [MyPref setValue:[user valueForKey:@"username"] forKey:@"instaemail"];
        [MyPref synchronize];
        
        NSLog(@"email from instagram is :%@", [MyPref valueForKey:@"instaemail"]);
        
        
        [self instagramLoggedIn];
        
        
        
    }
    
    
    //    NSString *pdata = [NSString stringWithFormat:@"type=3&token=%@&secret=123&login=%@", [tokenData objectForKey:@"access_token"], @"null"];
    //  NSString *pdata = [NSString stringWithFormat:@"type=3&token=%@&secret=123&login=%@",[tokenData accessToken.secret,self.isLogin];
    //  [dconnection fetch:1 withPostdata:pdata withGetData:@"" isSilent:NO];
    
    
    //    UIAlertView *alertView = [[UIAlertView alloc]
    //                              initWithTitle:@"Instagram Access TOken"
    //                              message:pdata
    //                              delegate:nil
    //                              cancelButtonTitle:@"OK"
    //                              otherButtonTitles:nil];
    //    [alertView show];
}

-(void) instagramLoggedIn
{
    
    MBProgressHUD *progress = [[MBProgressHUD alloc]init] ;//]WithWindow:[[UIApplication sharedApplication] keyWindow].rootViewController.view ];
    progress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
    
    
    progress.labelText = @"Processing..";
    
    
    
    //Sending data to the server of login
    NSString *postRegisterUser = [NSString stringWithFormat:@"%@", REGISTER];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    
    [request setURL:[NSURL URLWithString:postRegisterUser]];
    [request setHTTPMethod:@"POST"];
    
    
    NSDictionary *params = [[NSDictionary alloc]init ];
    
    params = [NSDictionary dictionaryWithObjectsAndKeys:@"xyz",@"name",[MyPref valueForKey:@"instaemail"],@"email" ,nil];
    
    [MyPref setValue:[MyPref valueForKey:@"instaemail"] forKey:@"email"];
    [MyPref synchronize];
    
    
    NSString *BoundaryConstant = @"a1u4orfleyuo2ruo3h4";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *param in [params allKeys]) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/json\r\n\r\n"dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[params objectForKey:param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData: [@"\r\n"dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [request setHTTPBody:body];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSLog(@"request body is :%@", [[NSString alloc]initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    
    
    
    NSLog(@"the request for server for login is :%@", [request allHTTPHeaderFields] );
    
    
    
    
    NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
        NSString *dataFromLogIn = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        
        NSDictionary *loginresponse;
        
        if (!data) {
            NSLog(@"data is nil");
            
        }
        else{
            loginresponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        }
        
        NSLog(@"response from server is :%@", response);
        
        NSLog(@"login dictionary is:%@",loginresponse);
        
        
        NSDictionary *userinfo = [loginresponse valueForKey:@"user"];
        
        
        NSLog(@"userinfo dictionary is :%@", userinfo);
        
        NSLog(@"the key for the new user is :%@", [userinfo valueForKey:@"isNewUser"]);
        
        
        NSArray *bank = [userinfo valueForKey:@"bank"];
        NSArray *category = [userinfo valueForKey:@"category"];
        
        NSLog(@"bank and category array is ;%@ and :%@", bank, category);
        
        
        
        [MyPref setValue:[bank lastObject] forKey:@"bankname"];
        [MyPref synchronize];
        
        
        
        
       
        
        [MyPref setValue:[category lastObject] forKey:@"category_hindi"];
        [MyPref synchronize];
        
        
        NSArray *newUser = [userinfo valueForKey:@"isNewUser"];
        
        NSString *userstring = [NSString stringWithFormat:@"%@", [newUser lastObject]];
        
        
        NSLog(@"user string is :%@", userstring);
        
        
        BOOL check = (BOOL)[newUser lastObject];
        
        NSLog(@"value of check is :%d and last object of the array is %@", check, [newUser lastObject]);
        
        
        NSString *userin = check ? @"true" : @"false";
        if ([userstring isEqualToString:@"0"]) {
            userin  =[[NSString alloc]initWithFormat:@"0"];
            
        }
        else if([userstring isEqualToString:@"1"])
        {
            userin  =[[NSString alloc]initWithFormat:@"1"];
        }
        [MyPref setValue:userin forKey:@"isNewUser"];
        [MyPref synchronize];
        
        
        
        //api key for the device is :
        
        newUser = nil;
        newUser = [userinfo valueForKey:@"api_key"];
        
        
        [MyPref setValue:[newUser lastObject] forKey:@"api_key"];
        [MyPref synchronize];
        
        
        //saving activation key for the new user
        
        
        
        [MyPref setValue:[userinfo valueForKey:@"activation_key"] forKey:@"activation_key"];
        [MyPref synchronize];
        
        
        
        //saving  the id for the user
        
        
        
        [MyPref setValue:[userinfo valueForKey:@"id"] forKey:@"id"];
        [MyPref synchronize];
        
        
        //email for the new user
        
        
        
        [MyPref setValue:[userinfo valueForKey:@"email"] forKey:@"email"];
        [MyPref synchronize];
        
        
        
        
        BOOL user = true;
        
        
        NSLog(@"wehave some new user info%@ and ture is :%d",[MyPref valueForKey:@"isNewUser"],(BOOL)user);
        
        
        if ([[MyPref valueForKey:@"isNewUser"]  isEqualToString:@"1"]) {
            NSLog(@"we have a new user%@", [MyPref valueForKey:@"isNewUser"]);
            
        }
        else{
            NSLog(@"user is already registered%@", [MyPref valueForKey:@"isNewUser"]);
            
        }
        
        //            dispatch_async(dispatch_get_main_queue(), ^{
        
        
        
        
        
        if ([[MyPref valueForKey:@"isNewUser"]  isEqualToString:@"1"]) {
            //                    [self removeFromSuperview];
            
            Dashboard *bank_salary = [[Dashboard alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
            
            
            [MyPref setValue:@"1" forKey:@"selection"];
            [MyPref synchronize];
            
            
            [bank_salary bank_salary];
            
            [MyPref setValue:@"1" forKey:@"login"];
            [MyPref synchronize];
            
            
            
        }
        else{
            BankList *list=[[BankList alloc]initWithFrame:self.bounds];
            [self addSubview:list];
            
            Dashboard *bank_salary = [[Dashboard alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
            
            
            [MyPref setValue:@"0" forKey:@"selection"];
            [MyPref synchronize];
            
            
            [bank_salary home];
            
            [MyPref setValue:@"1" forKey:@"login"];
            [MyPref synchronize];
            
            
            
        }
        //            }
        //            else{
        //
        //                BankList *list=[[BankList alloc]initWithFrame:self.bounds];
        //                [self addSubview:list];
        //
        //            }
        
        
        
        
        //            });
        NSLog(@"data from the server is :%@",dataFromLogIn);
        dispatch_async(dispatch_get_main_queue(), ^{
            progress.hidden = YES;
            
            
            [progress removeFromSuperview];
            
        });
        
        
    }]resume];
    
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    documentsDirectory =[documentsDirectory stringByAppendingPathComponent:@"login"];
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:NO attributes:nil error:nil];
        
    }
    
    
    [self createEditableCopyOfDatabaseIfNeeded:@"login"];
    
    documentsDirectory =[documentsDirectory stringByAppendingPathComponent:@"RwewtabDatabase.sqlite"];
    
    //Saving user emailid into the database
    sqlite3 *contactDB;
    sqlite3_stmt *statement=NULL;
    
    
    const char *dbpath=[documentsDirectory UTF8String];
    
    if(sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
    {
        
        char *err;
        
        const char *sql_stmt ="SELECT name FROM sqlite_master WHERE type='table' AND name='LogIn'";
        if (sqlite3_prepare_v2(contactDB, sql_stmt, -1, &statement, NULL) != SQLITE_OK)
        {
            
            
            NSLog(@"Table does not exists in tha database");
            
            
            
            
            
            NSLog(@"the error of the execution statement in creating table is :%d",sqlite3_prepare_v2(contactDB, sql_stmt, -1, &statement, NULL) );
            
            
            sqlite3_close(contactDB);
            NSLog(@"We are unable to create table in the database");
            
            // sqlite3_finalize(sql_stmt );
            NSAssert(0, @"Table failed to create.");
        }
        else{
            NSLog(@"Table Exist in the database is %d", sqlite3_prepare_v2(contactDB, sql_stmt, -1, &statement, NULL));
            
            
            const char *sql_stmt3 =
            "CREATE TABLE IF NOT EXISTS LogIn(Id INTEGER PRIMARY KEY, Email TEXT)";
            
            if (sqlite3_exec(contactDB, sql_stmt3, NULL, NULL, &err) != SQLITE_OK) {
                NSLog( @"Failed to create table with error %d",sqlite3_exec(contactDB, sql_stmt3, NULL, NULL, &err) );
                
            }
            
            
            
            else
            {
                
                NSLog(@"Table is successfully created");
                sqlite3_close(contactDB);
                sqlite3_finalize(statement);
                
                
                
            }
            
            
            
        }
    }
    
    NSLog(@"Email id is :%@", email.text);
    
    //Inserting Email id into database
    sqlite3_stmt *statement3=NULL;
    
    if(sqlite3_open([documentsDirectory UTF8String], &contactDB) == SQLITE_OK)
    {
        NSLog(@" opened database to insert data");
        NSString *insert=[NSString stringWithFormat:@"INSERT INTO LogIn (Id , Email) VALUES('%@', '%@')", @"1", email.text];
        NSLog(@"inserted data with query is :%@", insert);
        
        
        
        if (sqlite3_prepare_v2(contactDB, [insert UTF8String], -1, &statement3, NULL) == SQLITE_OK) {
            
            
            NSLog(@"inseted successfully");
            
            
            
            
        }
    }
    else
    {
        NSLog(@"Failed to open database at %s with error %s", dbpath, sqlite3_errmsg(contactDB));
        sqlite3_close (contactDB);
        sqlite3_finalize(statement);
    }
    
}

//Hiding for webview
-(void)hiding
{
    NSLog(@"hiding the webview and its background view here");
    
    
    
    [backwebview removeFromSuperview];
    [self.webview removeFromSuperview];
    
    
    
    
    
}


@end
