//
//  ViewController.m
//  Rwewtab
//
//  Created by Neuerung on 4/8/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import "ViewController.h"
#import "LogInView.h"
#import "BackButton.h"
#import "UIColor+Additions.h"

#define BACKGROUND @"http://elatikprojects.com/uploads/KuwaitNL.jpg"
#define GET_BANNER @"http://admin.elatikprojects.com/api/v1/get_banner"

#import "MBProgressHUD.h"
#define Backgroundimage @"http://admin.elatikprojects.com/api/v1/get_background"
#define DEVICEINFO @"http://admin.elatikprojects.com/api/v1/deviceInfo"
#define BASE_URL @"http://elatikprojects.com"
#import "AdvertisementView.h"
#define GET_CATE @"http://admin.elatikprojects.com/api/v1/get_cate"
#define BANK_REGISTER @"http://admin.elatikprojects.com/api/v1/get_banks"
#define REGISTER @"http://admin.elatikprojects.com/api/v1/register"
#import "ViewController.h"
#import "Dashboard.h"
#import "BankList.h"
#import "AdvertisementView.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [MyPref setValue:@"start" forKey:@"start"];
    [MyPref synchronize];
    
    
    
    
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
    backgroundImageView.clipsToBounds = YES;
    backgroundImageView.center = self.view.center;
    
    
    
    UILabel *contilabel = [[UILabel alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*86, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.width/100)*0.5)];
    [contilabel setBackgroundColor:[UIColor colorWithHexString:@"1d8841"]];
    
    
    
    UIView *backview = [[UIView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*100 - ([[UIScreen mainScreen] bounds].size.height/100)*14, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*14)];
    
    
    backview.backgroundColor = [UIColor colorWithHexString:@"ededed"];
    
    
    
    contin  = [[UIButton alloc]initWithFrame:CGRectMake(0,0, ([[UIScreen mainScreen] bounds].size.width/100)*30, ([[UIScreen mainScreen] bounds].size.width/100)*15)];
    
    NSLog(@"x and y position of  the back view is :%f and %f and screen height and width is :%f and %f", backview.center.x, backview.center.y,[[UIScreen mainScreen] bounds].size.height,[[UIScreen mainScreen] bounds].size.width);
    
    
    
//    contin.center = CGPointMake([[UIScreen mainScreen] bounds].size.width/2, backview.center.y);
    
    
     contin.center = backview.center;
    
    [contin setTitle:NSLocalizedString(@"Continue", nil) forState:UIControlStateNormal];
    
    
    contin.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:textSizeLarge];
    
    
    [contin setBackgroundColor:[UIColor colorWithHexString:@"1d8841"]];
    
    
    contin.titleLabel.textColor = [UIColor whiteColor];
    
    
    
    [contin addTarget:self action:@selector(secondsplash) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backview];
    
    [self.view addSubview:contin];
    [self.view addSubview:contilabel];
    
    
    
    NSString *getbannner = [NSString stringWithFormat:@"%@",GET_BANNER];
    
    NSMutableURLRequest *reequest = [[NSMutableURLRequest alloc]init];
    
    [reequest setURL:[NSURL URLWithString:getbannner]];
    [reequest setHTTPMethod:@"POST"];
    [reequest setValue:@"aplication/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    
    
    //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
    
    pregresss = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    pregresss.labelText = @"Processing...";
    
    if([self connectedToInternet])
    {
    
    NSURLSession *ssesssion= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[ssesssion dataTaskWithRequest:reequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
        
        
    
        if (data) {
            
        
        
        
        NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        
        
        NSLog(@"response from server is :%@ \nand data from the server is%@", response,
              data_server);
        
        
       NSDictionary *advertise_list = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
        
        
       

        NSLog(@"banner list from the server is :%@", advertise_list);
        
      NSMutableArray  *adverstise_array = [advertise_list valueForKey:@"banner"];
        NSLog(@"catagory data in array %@", adverstise_array);
        
        
        NSMutableArray *promoCount;
        
        
        int indexx = 0;
        
        for (indexx=0; indexx<[adverstise_array count]; indexx++) {
            
            NSDictionary *image_dict = [adverstise_array objectAtIndex:indexx];
            if ([[image_dict valueForKey:@"ad_unit"] isEqualToString:@"promotion"]) {
                
                
                if ([promoCount count] ==0) {
                    promoCount =[NSMutableArray arrayWithObject:image_dict];
                    
                }
                else{
                    [promoCount addObject:image_dict];
                    
                }
                
                
            }
            
        }
        
        
        [MyPref setValue:[NSString stringWithFormat:@"%d",(int) [promoCount count]] forKey:@"promotionCount"];
        [MyPref synchronize];

        
        for (UIView *view in [[[UIApplication sharedApplication] keyWindow].rootViewController.view subviews]) {
            [view removeFromSuperview];
            
        }
//        [self removeFromSuperview];
            AdvertisementView *advertise;
//            = [[AdvertisementView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*0, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*80) :@"splash" add_arrray:adverstise_array];
      
       
            
            if ([[MyPref valueForKey:@"banner_enable"] boolValue] == 1 ) {
                if ([MyPref valueForKey:@"banner"] !=nil) {
                  
                    
                    
                    advertise = [[AdvertisementView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*0, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*80) :@"Notification_banner" add_arrray:adverstise_array];
                    
                    
                }
                else{
                   
                      advertise = [[AdvertisementView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*0, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*80) :@"Notification_banner" add_arrray:adverstise_array];
                }
                
            }
            else{
                
                
                advertise = [[AdvertisementView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*0, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*80) :@"splash" add_arrray:adverstise_array];
                
                
            }

       
        
        
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
//        advertise.clas =[       start];


        
        dispatch_async(dispatch_get_main_queue(), ^{
//            [[BackButton sharedManager] goBack:advertise :@"advertisement"];
//            [[BankList sharedManager]addView:advertise :@""];
            
            
            [self.view addSubview:advertise];
            
            
            [self.view addSubview:backview];
            
            [self.view addSubview:contin];
            [self.view addSubview:contilabel];
            self.view.contentMode = UIViewContentModeScaleAspectFit;
            
          
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
                pregresss.hidden = YES;
                [pregresss removeFromSuperview];
                pregresss = nil;
                
            });

            
            [advertise slidingImageView];
           
            
            
        });
        }
        
       
        
        
        
    }]resume];
}
else
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You are not connected to internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    
}

     [self backgroundcalls];
    
    
       NSLog(@"new project started");
//    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(logInMenu) userInfo:nil repeats:NO];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void) logInMenu
{
    [backgroundImageView removeFromSuperview];
        LogInView *login=[[LogInView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:login];
//    [login logIn];
    
}
 

-(void) loogin
{
   
   
        
        backgroundImageView=[[UIImageView alloc]initWithFrame:self.view.bounds];
        NSString *postRegisterUser = [NSString stringWithFormat:@"%@", BACKGROUND];
        
        
        NSString *LocalPathForImage = [NSString stringWithFormat:@"%@/%@", kDocumentDirectoryPath,[MyPref valueForKey:@"actualBackgroundImagePath"]];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:LocalPathForImage]) {
            
            
            
            if ([self connectedToInternet]) {
                
           
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
            
            [request setURL:[NSURL URLWithString:postRegisterUser]];
            [request setHTTPMethod:@"GET"];
            [request setValue:@"aplication/json" forHTTPHeaderField:@"Content-Type"];
            
            
            
            
            NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            
            
            [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
                
                
                NSLog(@"response from server is :%@", response);
                dispatch_async(dispatch_get_main_queue(), ^{
                    image=[UIImage imageWithData:data];
                    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
                    backgroundImageView.clipsToBounds = YES;
                    backgroundImageView.center = self.view.center;
                    backgroundImageView.image= image;
                    
                    [self.view addSubview:backgroundImageView];
                    
                    
                    [[NSFileManager defaultManager] createFileAtPath:LocalPathForImage contents:data attributes:nil];
                    
                    
                    if ([[MyPref valueForKey:@"login"] isEqualToString:@"1"]) {
                        CATransition *transition = [CATransition animation];
                        transition.duration = animationTime;
                        transition.type = kCATransitionPush;
                        transition.subtype = kCATransitionFromRight;
                        [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
                        [self.view.layer addAnimation:transition forKey:nil];
                        
                        
                        BankList *list=[[BankList alloc]initWithFrame:self.view.bounds];
                        [self.view addSubview:list];
                        
                        Dashboard *bank_salary = [[Dashboard alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
                        
                        
                        [MyPref setValue:@"0" forKey:@"selection"];
                        [MyPref synchronize];
                        
                        
                        [bank_salary home];
                        
                        
                    }
                    else if ([[MyPref valueForKey:@"login"] isEqualToString:@"2"])
                    {
                        CATransition *transition = [CATransition animation];
                        transition.duration = animationTime;
                        transition.type = kCATransitionPush;
                        transition.subtype = kCATransitionFromRight;
                        [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
                        [self.view.layer addAnimation:transition forKey:nil];
                        
                        
                        BankList *list=[[BankList alloc]initWithFrame:self.view.bounds];
                        [self.view addSubview:list];
                        
                        Dashboard *bank_salary = [[Dashboard alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
                        
                        
                        [MyPref setValue:@"1" forKey:@"selection"];
                        [MyPref synchronize];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                        [bank_salary bank_salary];
                        });
                        
                    }
                    else{
                        
                        CATransition *transition = [CATransition animation];
                        transition.duration =animationTime;
                        transition.type = kCATransitionPush;
                        transition.subtype = kCATransitionFromRight;
                        [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
                        [self.view.layer addAnimation:transition forKey:nil];
                        
                        
                        [self logInMenu];
                    }
                    
                });
                
                
            }]resume];
            
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You are not connected to internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                BankList *list=[[BankList alloc]initWithFrame:self.view.bounds];
                [self.view addSubview:list];
                
            }
            
        }
        else{
            
            
            image = [UIImage imageWithContentsOfFile:LocalPathForImage];
            
            backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
            backgroundImageView.clipsToBounds = YES;
            backgroundImageView.center = self.view.center;
            backgroundImageView.image= image;
            
            [self.view addSubview:backgroundImageView];
            
            
           
                
            
            if ([[MyPref valueForKey:@"login"] isEqualToString:@"1"]) {
                
                CATransition *transition = [CATransition animation];
                transition.duration = animationTime;
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromRight;
                [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
                [self.view.layer addAnimation:transition forKey:nil];
                
                BankList *list=[[BankList alloc]initWithFrame:self.view.bounds];
                [self.view addSubview:list];
                
                if ([self connectedToInternet]) {
                    
                
                Dashboard *bank_salary = [[Dashboard alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
                
                
                [MyPref setValue:@"0" forKey:@"selection"];
                [MyPref synchronize];
                
                
                [bank_salary home];
                }
            
            else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You are not connected to internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
            
            }
            else if ([[MyPref valueForKey:@"login"] isEqualToString:@"2"])
            {
                CATransition *transition = [CATransition animation];
                transition.duration = animationTime;
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromRight;
                [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
                [self.view.layer addAnimation:transition forKey:nil];
                
               
                BankList *list=[[BankList alloc]initWithFrame:self.view.bounds];
                [self.view addSubview:list];
                if([self connectedToInternet])
                {
                Dashboard *bank_salary = [[Dashboard alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
                
                
                [MyPref setValue:@"1" forKey:@"selection"];
                [MyPref synchronize];
                
                
                [bank_salary bank_salary];
                }
                else{
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You are not connected to internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                }
            }

            else{
                
                
                CATransition *transition = [CATransition animation];
                transition.duration = animationTime;
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromRight;
                [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
                [self.view.layer addAnimation:transition forKey:nil];
                
                
               
                        [self logInMenu];
                           }
            
           
            
        }
    
    

    
    
}



-(void) secondsplash
{
    NSLog(@"we are loading second splash screen ");
    
    UIView *actualsplash = [[UIView alloc]initWithFrame:self.view.bounds];
    
    [self.view addSubview:actualsplash];
    
    
    actualsplash.backgroundColor = [UIColor whiteColor];
    
    
    
    UIImageView *splashview = [[UIImageView alloc]initWithFrame:self.view.bounds];
    
            NSString *LocalPathForImage = [NSString stringWithFormat:@"%@/%@", kDocumentDirectoryPath,[MyPref valueForKey:@"actualBackgroundImagePath"]
                                           ];
    splashview.image = [UIImage imageWithContentsOfFile:LocalPathForImage];
    
    splashview.contentMode = UIViewContentModeScaleAspectFill;
    splashview.clipsToBounds = YES;
    splashview.center = self.view.center;
    
    
    
    
    [actualsplash addSubview:splashview];
    
    UIImageView *appicon = [[UIImageView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*40, ([[UIScreen mainScreen] bounds].size.height/100)*30, ([[UIScreen mainScreen] bounds].size.width/100)*25,([[UIScreen mainScreen] bounds].size.width/100)*25)];
    
    appicon.image = [UIImage imageNamed:@"app_icon.png"];
     [actualsplash addSubview:appicon];
    
    actualsplash.contentMode = UIViewContentModeScaleAspectFit;
    
    actualsplash.center = self.view.center;
    
    appicon.center = self.view.center;
    
    
//    float hfactor = appicon.bounds.size.width / [[UIScreen mainScreen] bounds].size.width;
//    float vfactor = appicon.bounds.size.height / [[UIScreen mainScreen] bounds].size.height;
//    
//    float factor = fmax(hfactor, vfactor);
//    
//    // Divide the size by the greater of the vertical or horizontal shrinkage factor
//    float newWidth = appicon.bounds.size.width / factor;
//    float newHeight = appicon.bounds.size.height / factor;
    
    // Then figure out if you need to offset it to center vertically or horizontally
//    float leftOffset = ([[UIScreen mainScreen] bounds].size.width - newWidth) / 2;
//    float topOffset = ([[UIScreen mainScreen] bounds].size.height - newHeight) / 2;
    
//    CGRect newRect = CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*40, ([[UIScreen mainScreen] bounds].size.height/100)*30, newWidth, newHeight);
//    
//    appicon.frame = newRect;
    
    
    //Getting banks from the server
    
    
    
//      MBProgressHUD      *progresss = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//            progresss.labelText = @"Processing..";
    
    
    if ([self connectedToInternet]) {
        
   
    
        NSString *postRegisterUser = [NSString stringWithFormat:@"%@", BANK_REGISTER];
    
        NSMutableURLRequest *requesst = [[NSMutableURLRequest alloc]init];
    
        [requesst setURL:[NSURL URLWithString:postRegisterUser]];
        [requesst setHTTPMethod:@"POST"];
        [requesst setValue:@"aplication/json" forHTTPHeaderField:@"Content-Type"];
    
        NSURLSession *sessioon= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
        [[sessioon dataTaskWithRequest:requesst completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
    
            NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
                if(data)
                {
    
            NSLog(@"response from server is :%@ \nand data from the server is%@", response,
                  data_server);
    
    
            NSDictionary  *bank_list = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    
            //        NSLog(@"bank list from the server is :%@", bank_list);
    
            NSArray *banks_array = [bank_list valueForKey:@"banks"];
    
            NSLog(@"banks array is :%@", banks_array);
    
    
    
    
    
    
            [MyPref setValue:banks_array forKey:@"bank_salary"];
            [MyPref synchronize];
    
            NSLog(@"shared preferences bank salary is :%@", [MyPref valueForKey:@"bank_salary"]);
    
    
    
    
//            dispatch_async(dispatch_get_main_queue(), ^{
//    
//    
//                progresss.hidden = YES;
//                
//                [progresss removeFromSuperview];
//                
////                progresss = nil;
//            });
            
            
                }
            
        }]resume];
    }
    
    
     [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(loogin) userInfo:nil repeats:NO];
    
    
    
    
    CATransition *transition = [CATransition animation];
    transition.duration = animationTime;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [actualsplash.layer addAnimation:transition forKey:nil];
    
    
    
    
}


+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)size {
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


-(void) backgroundcalls

{
    if([self connectedToInternet])
    {
        
   
    
    
    NSString *posttRegisterUser = [NSString stringWithFormat:@"%@", REGISTER];
    
    NSMutableURLRequest *requeesst = [[NSMutableURLRequest alloc]init];
    
    [requeesst setURL:[NSURL URLWithString:posttRegisterUser]];
    [requeesst setHTTPMethod:@"POST"];
    
    
    NSDictionary *params = [[NSDictionary alloc]init ];
    
    NSLog(@"email for the email tag is :%@",[MyPref valueForKey:@"email"]);
    
    if ([MyPref valueForKey:@"email"]) {
        
        NSArray *email =[MyPref valueForKey:@"email"];
        NSString *actmail = [email objectAtIndex:0];
        
        NSLog(@"mail from user is :%@", email);
        
        
        params = [NSDictionary dictionaryWithObjectsAndKeys:@"xyz",@"name",actmail,@"email" ,nil];
        
        NSString *BoundaryConstant = @"a1u4orfleyuo2ruo3h4";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [requeesst setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        
        for (NSString *param in [params allKeys]) {
            NSLog(@"making keys visible");
            
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            NSLog(@"making keys visible1");
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            NSLog(@"making keys visible2");
            [body appendData:[@"Content-Type: application/json\r\n\r\n"dataUsingEncoding:NSUTF8StringEncoding]];
            NSLog(@"making keys visible3 with key%@ and value%@",param,[params objectForKey:param]);
            [body appendData:[[params objectForKey:param] dataUsingEncoding:NSUTF8StringEncoding]];
            NSLog(@"making keys visible4");
            [body appendData: [@"\r\n"dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [requeesst setHTTPBody:body];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [requeesst setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        NSLog(@"request body is :%@", [[NSString alloc]initWithData:[requeesst HTTPBody] encoding:NSUTF8StringEncoding]);
        
        NSLog(@"body set");
        
        
        NSLog(@"the request for server for login is :%@", [requeesst allHTTPHeaderFields] );
        
        
        pregresss = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        pregresss.labelText = @"Processing...";
        
        NSURLSession *sessionn= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        [[sessionn dataTaskWithRequest:requeesst completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
            //        NSString *dataFromLogIn = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            
            NSDictionary *loginresponse;
            
            if (!data) {
                NSLog(@"data is nil");
                
            }
            else{
                loginresponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            }
            
            NSLog(@"response from server is for login is :%@", response);
            
            NSDictionary *userinfo = [loginresponse valueForKey:@"user"];
            
            
            NSLog(@"userinfo dictionary is :%@", userinfo);
            
            NSLog(@"the key for the new user is :%@", [userinfo valueForKey:@"isNewUser"]);
            
            NSArray *bank = [userinfo valueForKey:@"bank"];
            NSArray *category = [userinfo valueForKey:@"category"];
            
            NSLog(@"bank and category array is ;%@ and :%@", bank, category);
            
            
            
            [MyPref setValue:[bank lastObject] forKey:@"bankname"];
            [MyPref synchronize];
            
            
          
            
            
            
            
            
            
            [MyPref setValue:[category lastObject] forKey:@"category_hindi"];
            [MyPref synchronize];
            
            NSArray *newUser = [userinfo valueForKey:@"isNewUser"];
            
            NSString *userstring = [NSString stringWithFormat:@"%@", [newUser lastObject]];
            
            
            NSLog(@"user string is :%@", userstring);
            
            
            BOOL check = (BOOL)[newUser lastObject];
            
            NSLog(@"value of check is :%d and last object of the array is %@", check, [newUser lastObject]);
            
            
            NSString *userin = check ? @"true" : @"false";
            if ([userstring isEqualToString:@"0"]) {
                userin  =[[NSString alloc]initWithFormat:@"0"];
                
            }
            else if([userstring isEqualToString:@"1"])
            {
                userin  =[[NSString alloc]initWithFormat:@"1"];
            }
//            [MyPref setValue:@"1" forKey:@"isNewUser"];
//            [MyPref synchronize];
            
            [MyPref setValue:@"0" forKey:@"isNewUser"];
                        [MyPref synchronize];
            
            NSArray *newUsser =[userinfo valueForKey:@"category"];
            NSString *frshnewuser = [newUsser objectAtIndex:0];
            NSArray *newuserbank =[userinfo valueForKey:@"bank"];
            NSString *newbank = [newuserbank objectAtIndex:0];
            
            
            if ([frshnewuser length] == 0) {
                [MyPref setValue:@"2" forKey:@"login"];
                [MyPref synchronize];
                [MyPref setValue:@"1" forKey:@"isNewUser"];
                            [MyPref synchronize];
                
            }
            else if([newbank length] == 0)
            {
                
                [MyPref setValue:@"2" forKey:@"login"];
                [MyPref synchronize];
                [MyPref setValue:@"1" forKey:@"isNewUser"];
                [MyPref synchronize];
                
            }
            
            newUser = nil;
            newUser = [userinfo valueForKey:@"api_key"];
            
            [MyPref setValue:[newUser lastObject] forKey:@"api_key"];
            [MyPref synchronize];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
                pregresss.hidden = YES;
                [pregresss removeFromSuperview];
                pregresss = nil;
                
            });
            
        }]resume];
        
        
        
    }
    
    
    [MyPref setValue:@"0" forKey:@"selection"];
    [MyPref synchronize];
    
    
    [MyPref setValue:@"0" forKey:@"internal_bank_recruiter_list"];
    [MyPref synchronize];
    
    [MyPref setValue:@"0" forKey:@"monthof"];
    [MyPref synchronize];
    
        pregresss = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        pregresss.labelText = @"Processing..";
    
    
    
    
    NSString *getbanner = [NSString stringWithFormat:@"%@",Backgroundimage];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    
    [request setURL:[NSURL URLWithString:getbanner]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"aplication/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    NSLog(@"request for the background is :%@", [request allHTTPHeaderFields]);
    
    
    
    
    NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
        
        
        
         if (data) {
        NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary *image_dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
        NSLog(@"response from server is :%@ \nand data from the server is%@ and error is %@", response,
              data_server,err);
        
       
            
            
            NSLog(@"image dictionary is :%@", image_dict);
            
            
            NSDictionary *image_main_dict = [image_dict valueForKey:@"background"];
            
            NSLog(@"image array is :%@", image_main_dict);
            
            
            NSArray *image_path_array  = [image_main_dict valueForKey:@"path"];
            
            
            
            NSString *image_path= [NSString stringWithFormat:@"%@%@", BASE_URL,[image_path_array lastObject]];
            
            NSArray *actimage = [[image_path_array lastObject] componentsSeparatedByString:@"/"];
            
            
            [MyPref setValue:[actimage lastObject] forKey:@"actualBackgroundImagePath"];
            [MyPref synchronize];
            
            
            NSLog(@"image url is :%@", image_path);
            
            
            
            NSString *local_image_path =[NSString stringWithFormat:@"%@/%@", kDocumentDirectoryPath,[actimage lastObject]];
            
            
            NSLog(@"local image path is :%@", local_image_path);
            
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 
                 
                 pregresss.hidden = YES;
                 
                 [pregresss removeFromSuperview];
                 pregresss = nil;
                 
             });

            
            
            
            NSURL *image_url = [NSURL URLWithString:image_path];
            
            NSData *image_data = [NSData dataWithContentsOfURL:image_url];
            
            if (image_data) {
                
                
                
                
                if (![[NSFileManager defaultManager] fileExistsAtPath:local_image_path]) {
                    [[NSFileManager defaultManager] createFileAtPath:local_image_path contents:image_data attributes:nil];
                    
                }
                
                else{
                    
                    [[NSFileManager defaultManager] removeItemAtPath:local_image_path error:nil];
                    [[NSFileManager defaultManager] createFileAtPath:local_image_path contents:image_data attributes:nil];
                    
                }
            }
            
        }
        
        //        pregress.hidden = YES;
        
    }]resume];
    
    
    
    
    
    
    
    
    
    
    //sending device token to the server
    
    
    
    if (![[MyPref valueForKey:@"api_key"] isEqualToString:@""]) {
        
        
        NSLog(@"api key is :%@", [MyPref valueForKey:@"api_key"]);
        
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
        
        [request setURL:[NSURL URLWithString:DEVICEINFO]];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[MyPref valueForKey:@"api_key"] forHTTPHeaderField:@"Authorization-Basic"] ;
        
        NSLog(@"actual request is :%@", [request allHTTPHeaderFields]);
        //Setting body for the request
        
        
        
        
        
        
        
        
        
        
        
        NSMutableData *body = [NSMutableData data];
        
        
        NSString *bodydata = [NSString stringWithFormat:@"imei=0&token=%@&OSType=iOS&mac=%@",[MyPref valueForKey:@"DevToken"],VendorID];
        
        
        
        
        
        
        
        [body appendData:[bodydata dataUsingEncoding:NSASCIIStringEncoding]];
        
        
        
        
        [request setHTTPBody:body];
        
        NSString *bodyofrequest = [[NSString alloc]initWithData:body encoding:NSUTF8StringEncoding];
        
        
        NSLog(@"request body is:%@", bodyofrequest);
        
        NSLog(@"actual request is :%@", [request allHTTPHeaderFields]);
        
        
        
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        
        
        //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
        
        
        NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
            
            NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            
            
            NSLog(@"response from server is :%@ \nand data from the server is%@", response,
                  data_server);
            
            
            
            
            
            
            
            
            
            
            
         
            
            
            
        }]resume];
        
    }
    
    
    
//
//    
//    NSDate *currentDate = [NSDate date];
//    
//    
//    NSLog(@"current date of the device is :%@", currentDate);
//    
//    
//    NSCalendar *hijiri = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    
//    NSDateFormatter *format=[[NSDateFormatter alloc ] init];
//    
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
//    [format setLocale:locale];
//    
//    
//    [format setDateStyle:NSDateFormatterLongStyle];
//    [format setTimeStyle:NSDateFormatterNoStyle];
//    [format setCalendar:hijiri];
//    
//    [format setDateFormat:@"yyyy-MM-dd HH:mm:ss" ];
//    
//    
//    NSLog(@"date in english is :%@", [format stringFromDate:[NSDate date]]);
//    
//    
//    
//    NSString *date = [format stringFromDate:currentDate];
//    
//    NSLog(@"string from date is:%@",date);
//    
//    
//    date= [date substringToIndex:7];
//    
//    NSLog(@"date with year and month%@", date);
//    
//    NSString *month = [date substringFromIndex:5];
//    
//    NSLog(@"month from [date is :%@",month);
//    
//    NSString *year = [date substringToIndex:4];
//    
//    NSLog(@"year from the date is :%@", year);
//    
//    
//    
//    
       
    
//
   //
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        
//        pregresss.hidden = YES;
//        
//        [pregresss removeFromSuperview];
//        
//        pregresss = nil;
//    });

     }
  
}


- (BOOL)connectedToInternet
{
    NSString *urlString = @"http://www.google.com/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    
    return ([response statusCode] == 200) ? YES : NO;
}






@end
