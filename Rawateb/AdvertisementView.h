//
//  AdvertisementView.h
//  Rwewtab
//
//  Created by Neuerung on 4/8/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface AdvertisementView : UIView
{
    NSDictionary *bannerserver;
    
    NSArray *advertiseData;
    
    UIImageView *advertisement;
    
    UIPageControl *pagecontrol;
    
    
    UISwipeGestureRecognizer *swiperight;
    
    UISwipeGestureRecognizer *swipeleft;
    
    //Image components
    
    
    //progresshud for background processing
    MBProgressHUD *pregress;
    
    
    
}

-(id) initWithFrame:(CGRect)frame :(NSString *)ad_unit add_arrray:(NSArray *)ad_array ;
-(void) slidingImageView;

@property (nonatomic) int index;
@property (nonatomic, strong) NSMutableArray *add_array;
@property (nonatomic, strong)NSURL *image_url ;
@property (nonatomic, strong)NSData *image_data;
@property (nonatomic , strong)NSString *clas;
@property  (nonatomic , strong)NSDictionary *currentDictionary;


@end
