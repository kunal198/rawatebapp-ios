//
//  UIColor+Additions.h
//  MADP
//
//  Created by Pankaj Dhiman on 23/09/14.
//  Copyright (c) 2014 NTPL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Additions)
+(UIColor*)colorWithHexString:(NSString*)hex;
@end
