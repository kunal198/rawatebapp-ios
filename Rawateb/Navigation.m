//
//  Navigation.m
//  Rwewtab
//
//  Created by Neuerung on 4/22/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import "Navigation.h"

@implementation Navigation

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (id)sharedManager {
    static Navigation *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}



@end
