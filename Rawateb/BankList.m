//
//  BankList.m
//  Rwewtab
//
//  Created by Rohit Mishra on 4/8/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import "BankList.h"
#import "BackButton.h"
#define BACKGROUND @"http://elatikprojects.com/uploads/KuwaitNL.jpg"
#import "ListView.h"
#import "AppDelegate.h"
//#import "Dashboard.h"
@implementation BankList
@synthesize title;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id) initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if(self)
    {
        //[self setBackgroundColor:[UIColor whiteColor]];
        UIImageView *backgroundImageView=[[UIImageView alloc]initWithFrame:self.bounds];
        NSString *postRegisterUser = [NSString stringWithFormat:@"%@", BACKGROUND];
        
        
        //Back Button to go back from the view
        
        backButton =[[UIButton alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*3, ([[UIScreen mainScreen] bounds].size.height/100)*4, ([[UIScreen mainScreen] bounds].size.width/100)*11, ([[UIScreen mainScreen] bounds].size.width/100)*11)];
        backButton.contentMode = UIViewContentModeScaleAspectFill;
        backButton.clipsToBounds =YES;
       
        backgroundImageView.center = [[UIApplication sharedApplication] keyWindow].rootViewController.view.center;
        
        [backButton setBackgroundImage:[UIImage imageNamed:@"icon_back"] forState:UIControlStateNormal];
        
        backButton.imageView.center = backButton.center;
        
        
        
        [backButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *LocalPathForImage = [NSString stringWithFormat:@"%@/%@", kDocumentDirectoryPath,[MyPref valueForKey:@"actualBackgroundImagePath"]];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:LocalPathForImage]) {

        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
        
        [request setURL:[NSURL URLWithString:postRegisterUser]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"aplication/json" forHTTPHeaderField:@"Content-Type"];
        
        
        
        
        NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
            
            
            NSLog(@"response from server is :%@", response);
            dispatch_async(dispatch_get_main_queue(), ^{
                image=[UIImage imageWithData:data];
                backgroundImageView.image= image;
                backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
                backgroundImageView.clipsToBounds = YES;
                backgroundImageView.center = [[UIApplication sharedApplication] keyWindow].rootViewController.view.center;
                [self addSubview:backgroundImageView];
                [self popoverWindow];
                [self addSubview:backButton];
                
            });
            
           
            
            
        }]resume];
        }
        else{
            
            
            image = [UIImage imageWithContentsOfFile:LocalPathForImage];
            
            backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
            backgroundImageView.clipsToBounds = YES;
            backgroundImageView.center = [[UIApplication sharedApplication] keyWindow].rootViewController.view.center;
            backgroundImageView.image= image;
            
            [self addSubview:backgroundImageView];
            [self popoverWindow];
            [self addSubview:backButton];

            
            
        }
     
        UITapGestureRecognizer *imageTapping = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTapped)];
        backgroundImageView.userInteractionEnabled = YES;
        
        
        [backgroundImageView addGestureRecognizer:imageTapping];
        
        title = [[UILabel alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*10, ([[UIScreen mainScreen] bounds].size.height/100)*10, ([[UIScreen mainScreen] bounds].size.width/100)*24, ([[UIScreen mainScreen] bounds].size.height/100)*7)];
        
        title.center = CGPointMake([[UIScreen mainScreen] bounds].size.width/2, ([[UIScreen mainScreen] bounds].size.height/100)*20);
        
        
        
        
        monthof = [[UILabel alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*42, ([[UIScreen mainScreen] bounds].size.height/100)*24, ([[UIScreen mainScreen] bounds].size.width/100)*35, ([[UIScreen mainScreen] bounds].size.height/100)*7)];
        
        monthof.center = CGPointMake(([[UIScreen mainScreen] bounds].size.width/100)*49, ([[UIScreen mainScreen] bounds].size.height/100)*26);
        
        
        monthof.textAlignment = NSTextAlignmentCenter;
        
        
        NSDate *currentDate = [NSDate date];
        
        
        NSLog(@"current date of the device is :%@", currentDate);
        
        
        NSCalendar *hijiri = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        NSDateFormatter *format=[[NSDateFormatter alloc ] init];
        
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
        [format setLocale:locale];
        
        
        [format setDateStyle:NSDateFormatterLongStyle];
        [format setTimeStyle:NSDateFormatterNoStyle];
        [format setCalendar:hijiri];
        
        [format setDateFormat:@"yyyy-MM-dd HH:mm:ss" ];
        
        
        NSLog(@"date in english is :%@", [format stringFromDate:[NSDate date]]);
        
        
        
        NSString *date = [format stringFromDate:currentDate];
        
        NSLog(@"string from date is:%@",date);
        
        
        date= [date substringToIndex:7];
        
        NSLog(@"date with year and month%@", date);
        
        NSString *month = [date substringFromIndex:5];
        
        NSLog(@"month from [date is :%@",month);
        
        NSString *year = [date substringToIndex:4];
        
        NSLog(@"year from the date is :%@", year);
        NSDateFormatter *monthformatter = [[NSDateFormatter alloc] init];
        
        [monthformatter setDateFormat:@"MM"];
        
        NSDate *monthdate = [monthformatter dateFromString:month];
        
        
        NSDateFormatter *monthname = [[NSDateFormatter alloc]init];
        
        [monthname setDateFormat:@"MMMM"];
        
        
        
        
         NSString *monthwithname = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"Month of", nil),NSLocalizedString([monthname stringFromDate:monthdate], nil)];
        
        NSLog(@"month name is :%@", monthwithname);
        
        
        monthof.text = monthwithname;
        [monthof setFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]];
        
        [monthof setTextColor:[UIColor colorWithRed:255.0/255 green:192.0/255 blue:0.0/255 alpha:0.85f]];
        
        
        
       //Adding title to list if text present
        
        
        
       //image view to take the screen shot of the screen
        imageViewww=[[UIImageView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*70, ([[UIScreen mainScreen] bounds].size.height/100)*2, ([[UIScreen mainScreen] bounds].size.width/100)*30, ([[UIScreen mainScreen] bounds].size.height/100)*100)];
        
                
//        bankslist =[bankslist regi]
        
        
        if (![[MyPref valueForKey:@"selection"] isEqualToString:@"0"]) {
           
        
        
                   }
        
       bankslist.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.0f];
        
        bankslist.delegate=self;
        
        bankslist.dataSource = self;
        
      
       
        
    
        
        
        NSLog(@"all banks lists");
        
    }
    return  self;
    
}


//shared manager

+ (id)sharedManager {
    static BankList *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellidentifier = @"cell";
    
    
    UITableViewCell *cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    
    cell.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.4f];
    
    return cell;
    
}


-(void) popoverWindow
{
    //Setting uibutton to open uipopover menu
    bar=[[UIButton alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*88, ([[UIScreen mainScreen] bounds].size.height/100)*4, ([[UIScreen mainScreen] bounds].size.width/100)*10, ([[UIScreen mainScreen] bounds].size.width/100)*10)];
    
    
    bar.imageEdgeInsets = UIEdgeInsetsMake(4, 6, 4, 5);
    
    bar.imageView.center = bar.center;
    
    
    
    [self addSubview:bar];
    [bar addTarget:self action:@selector(popMenu) forControlEvents:UIControlEventTouchUpInside];
   // bar.appearance = UIBarButtonSystemItemAction;
    [bar setImage:[UIImage imageNamed:@"icon_menu"] forState:UIControlStateNormal];
    
    bar.contentMode = UIViewContentModeScaleAspectFit;
    [bar setBackgroundColor:[UIColor colorWithRed:255.0/255 green:192.0/255 blue:0.0/255 alpha:0.85f]];
    
    
//    bar.backgroundColor =[UIColor blackColor];
//    [bar setTitle:@"po/Volumes/UNTITLED 1/Rwewtab/Rwewtab/BankList.mpover" forState:UIControlStateNormal];
//    bar.titleLabel.font =[UIFont fontWithName:@"Helvetica" size:([[UIScreen mainScreen] bounds].size.height/100)*2];
    
    
}

-(void) popMenu
{
    actualDashboard = [[Dashboard alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*50, ([[UIScreen mainScreen] bounds].size.height/100)*4, ([[UIScreen mainScreen] bounds].size.width/100)*50, ([[UIScreen mainScreen] bounds].size.height/100)*100)];
    
    
 //
    
   // [rootview addSubview:imageViewww];
    
    
//    newView=[[UIView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*70, ([[UIScreen mainScreen] bounds].size.height/100)*2, ([[UIScreen mainScreen] bounds].size.width/100)*30, ([[UIScreen mainScreen] bounds].size.height/100)*100)];
//    
//    [self addSubview:newView];
//    
//    [newView addSubview:imageViewww];
    
    
   // [rootview bringSubviewToFront:imageViewww];
    
    
//    CGRect frame = newView.bounds;
//    frame.origin.x+=newView.frame.size.width;
//    newView.frame = frame ;
 
  //  imageViewww.backgroundColor=[UIColor blackColor];
    
    
    
    
    
    newView=[[UIView alloc]initWithFrame:self.bounds];
    
    
    [self addSubview:newView];
    
    [newView addSubview:actualDashboard];
    
  
    newView.backgroundColor=[UIColor colorWithRed:1.0/255 green:18.0/255 blue:36.0/255 alpha:1.0f];
    
    CGRect frame = newView.bounds;
    frame.origin.x+=newView.frame.size.width;
    frame.origin.y = actualDashboard.frame.origin.y;
    newView.frame = frame ;
    
    
    [UIView beginAnimations:@"flipopen" context:nil];
    
    
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:newView cache:TRUE];
    
    [UIView setAnimationDuration:0.60];
    
    CGRect rt = [newView frame];
    
    rt.origin.x-=actualDashboard.frame.size.width;
    
    [newView setFrame:rt] ;
    
    [UIView commitAnimations];
    
    
   
    double delayInSeconds = 0.50;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
      
        
        [self addSubview:actualDashboard];
        newView.hidden=YES;
        
        
        
    });
//    [UIView beginAnimations:@"bucketoff" context:nil];
//    [UIView setAnimationDuration:2.0];
//    [UIView setAnimationDelegate:self];
//    
//     [UIView commitAnimations];
    
//    CGRect rt = [newView frame];
//
//    rt.origin.x-=newView.frame.size.width;
//
//    [newView setFrame:rt] ;
//
//
//    [newView removeFromSuperview];
    //[self addSubview:actualDashboard];
    
   
    
    
    NSLog(@"uipopover menu is called here");
    
}


//if background of the view is tapped then sideview should be dissappeared
-(void) imageTapped
{
    /*UIImageView *imageview = [[UIImageView alloc]init ];//]WithImage:[self screenshot1]];
    
    imageview.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*60, ([[UIScreen mainScreen] bounds].size.height/100)*2, ([[UIScreen mainScreen] bounds].size.width/100)*40, ([[UIScreen mainScreen] bounds].size.height/100)*100);
    
    imageview.image = [self screenshot1];
    
    
    newView = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    
    
    [self addSubview:newView];
    
    
    newView.backgroundColor =[UIColor blackColor];
    
    [actualDashboard removeFromSuperview];
    CGRect frame = newView.bounds;
    frame.origin.x+=newView.frame.size.width;
    frame.origin.y = actualDashboard.frame.origin.y;
    NSLog(@"y position of actualdashboard is :%f", actualDashboard.frame.origin.y);
    
    frame.size.width = actualDashboard.frame.size.width;
    
    newView.frame = frame ; // self.view is view "B"
    [newView addSubview:imageview];
    
    // Pre-transform our view (s=target/current, d=target-current)
    CGAffineTransform transform = CGAffineTransformIdentity ;
    
    // Translate our view
    CGRect r = [UIScreen mainScreen].bounds;
    CGPoint center  = CGPointMake(r.origin.x + (newView.frame.size.width+newView.frame.size.width), r.origin.y +([[UIScreen mainScreen] bounds].size.height/100)*2+ (newView.frame.size.height/2.0)) ;
    float dX = center.x - newView.center.x ;
    float dY = center.y - newView.center.y ;
    
    
    transform = CGAffineTransformTranslate(transform, dX, dY) ;
    
    // Scale our view
    float scaleWFactor = actualDashboard.frame.size.width / actualDashboard.frame.size.width ;
    float scaleHFactor = actualDashboard.frame.size.height / actualDashboard.frame.size.height ;
    transform = CGAffineTransformScale(transform,scaleWFactor, scaleHFactor) ;
    
    newView.transform = transform ;
    
    
    //    [UIView beginAnimations:@"flipopen" context:nil];
    //    //[UIView animateWithDuration:2.00 animations:nil];
    //
    //        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:newView cache:TRUE];
    
    //        [UIView setAnimationDuration:0.60];
    
    // Perform the animation later since implicit animations don't seem to work due to
    // view "B" just being added above and hasn't had a chance to become visible. Right now
    // this is just on a timer for debugging purposes. It'll probably be moved elsewhere, probably
    // to view "B"'s -didMoveToSuperview FlipFromRight| UIViewAnimationOptionCurveEaseOut
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [UIView transitionWithView:self duration:0.60 options:UIViewAnimationOptionTransitionNone animations:^(void)
         {
             newView.transform = CGAffineTransformIdentity ;
         }
                        completion:^(BOOL finished){
                            CGRect rt = [newView frame];
                            rt.origin.x-=actualDashboard.frame.size.width;
                            [newView setFrame:rt] ;
                            
                        }] ;
    });
    
    
    
    
    double delayInSecondss = 0.70;
    dispatch_time_t popTimes = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSecondss * NSEC_PER_SEC));
    dispatch_after(popTimes, dispatch_get_main_queue(), ^(void){
        
        
        //        [rootview addSubview:app];
        newView.hidden=YES;
        
        [imageview removeFromSuperview];
        
        
        
    });
     */
//
//    CATransition *transition = [CATransition animation];
//    transition.duration = 1.0;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromLeft;
//    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
//    [actualDashboard.layer addAnimation:transition forKey:nil];
    
    [actualDashboard removeFromSuperview];
    
    NSLog(@"background of the view is tapped");
    
    
    
}

//Method to take screen shot of view

- (UIImage *) screenshot {
    UIView *rootview=[[[UIApplication sharedApplication] keyWindow] rootViewController].view ;
    UIGraphicsBeginImageContextWithOptions(rootview.bounds.size, NO, [UIScreen mainScreen].scale);
    //[self.view drawViewHierarchyInRect:self.view.bounds afterScreenUpdates:YES];
    //    CGContextRef context = UIGraphicsGetCurrentContext();
    //    [self.view.layer renderInContext:context];
    
    
    [rootview drawViewHierarchyInRect:rootview.bounds afterScreenUpdates:NO];
    
    UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    //self.imageView.hidden = YES;
    return image1;
}
- (UIImage *) screenshot1 {
//    UIView *rootview=[[[UIApplication sharedApplication] keyWindow] rootViewController].view ;
//    rootview.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*60, ([[UIScreen mainScreen] bounds].size.height/100)*2, ([[UIScreen mainScreen] bounds].size.width/100)*40, ([[UIScreen mainScreen] bounds].size.height/100)*100);
    
    UIGraphicsBeginImageContextWithOptions(actualDashboard.bounds.size, NO, 0);
    //[self.view drawViewHierarchyInRect:self.view.bounds afterScreenUpdates:YES];
    //    CGContextRef context = UIGraphicsGetCurrentContext();
    //    [self.view.layer renderInContext:context];
    
    
    [actualDashboard drawViewHierarchyInRect:actualDashboard.bounds afterScreenUpdates:NO];
    
    UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    //self.imageView.hidden = YES;
    return image1;
}


//Going back
-(void) goBack
{
   
    
    
    
    
    
//        [ListView sharedManager]->sea
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
    
         dispatch_async(dispatch_get_main_queue(), ^{
             
           
                 
                   NSString *list = (NSString *)[[BackButton sharedManager] back];
                 if ([list isEqualToString:@"recruiters"]) {
                     dispatch_async(dispatch_get_main_queue(), ^{
                     [MyPref setValue:@"2" forKey:@"selection"];
                     [MyPref synchronize];
                     Dashboard *tableview = [[Dashboard alloc]init];
                     [tableview recruiters];
                     
                     [[BackButton sharedManager] back];
                     
                     });
                     
                 }
                 else if ([list isEqualToString:@"graduates"]) {
                     [MyPref setValue:@"3" forKey:@"selection"];
                     [MyPref synchronize];
                     dispatch_async(dispatch_get_main_queue(), ^{
                     Dashboard *tableview = [[Dashboard alloc]init];
                     [tableview graduates];
                     
                    [[BackButton sharedManager] back];
                     });
                     
                 }
                 else if ([list isEqualToString:@"advertisement"]) {
                     [MyPref setValue:@"4" forKey:@"selection"];
                     [MyPref synchronize];
                     dispatch_async(dispatch_get_main_queue(), ^{
                     Dashboard *tableview = [[Dashboard alloc]init];
                     [tableview advertisement];
                     [[BackButton sharedManager] back];
                     });
                     
                    
                 }
                 else if([list isEqualToString:@"bank_salary"])
                     
                 {
                     [MyPref setValue:@"1" forKey:@"selection"];
                     [MyPref synchronize];
                     dispatch_async(dispatch_get_main_queue(), ^{
                     Dashboard *tableview = [[Dashboard alloc]init];
                     [tableview bank_salary];
                     [[BackButton sharedManager] back];
                     });
                     
                     
                 }
                 else if([list isEqualToString:@"home"])
                     
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                     [MyPref setValue:@"0" forKey:@"selection"];
                     [MyPref synchronize];
                     Dashboard *tableview = [[Dashboard alloc]init];
                     [tableview home];
                     [[BackButton sharedManager] back];
                     });
                     
                 }
            
             
            
                 else
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                     [MyPref setValue:@"0" forKey:@"selection"];
                     [MyPref synchronize];
                     Dashboard *tableview = [[Dashboard alloc]init];
                     [tableview home];
                     [[BackButton sharedManager] back];
                     });
                 }
             
         });
        
    
//    });
    
    
    NSLog(@"going to backin the view");
    
}


//Adding list to the bank list view
-(void) addView :(UIView *)view :(NSString *) titlee
{
    BankList *list = [[BankList alloc]initWithFrame:[UIScreen mainScreen].bounds];
    [[[UIApplication sharedApplication] keyWindow].rootViewController.view addSubview:list];
    list.title.font = [UIFont fontWithName:@"Helvetica" size:textSizeLarge];
    
           list.title.text = NSLocalizedString(titlee, nil);
    
    NSLog(@"title for trhe bank list is :%@ and list text title is :%@", titlee,list.title.text);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
   
    if (![list.title.text isEqualToString:@"home"]  && ![list.title.text isEqualToString:@""]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
           
            CGRect frame = list.title.frame;
            frame.size.width = [titlee sizeWithFont:[UIFont fontWithName:@"Helvetica" size:([UIScreen mainScreen].bounds.size.height/100)*2]].width;
            
            
            list.title.frame = frame;
            
            
            frame.size.width= list.title.frame.size.width +([UIScreen mainScreen].bounds.size.width/100)*4;
            
            
            
            list.title.frame = frame;

            
            
            
            
             list.title.backgroundColor = [UIColor colorWithRed:99/255.0f green:184/255.0f blue:255/255.0f alpha:1.0f];
             list.title.textColor = [UIColor whiteColor];
            
            
            list.title.center = CGPointMake(([[UIScreen mainScreen] bounds].size.width/100)*50, ([[UIScreen mainScreen] bounds].size.height/100)*13.5);
            
            
             list.title.textAlignment = NSTextAlignmentCenter;
             
        [list addSubview:list.title];
            
            if ([[MyPref valueForKey:@"monthof"] isEqualToString:@"1"]) {
                [list addSubview:monthof];
            
                [MyPref setValue:@"0" forKey:@"monthof"];
                
                
            }
            else{
                NSLog(@"month of disabled");
                
            }
            
            
            
            if ([titlee isEqualToString:@"NEW GRADUATE LIST"]) {
//                list.search_bar = [[UISearchBar alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*10, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*5)];
//                
//                list.search_bar.backgroundColor = [UIColor clearColor];
//                list.search_bar.translucent=YES;
                
                NSLog(@"new graduate list is here");
                
                
                frame.origin.y = ([[UIScreen mainScreen] bounds].size.height/100)*17;
                frame.size.width = [titlee sizeWithFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]].width;
                frame.size.width = frame.size.width+([[UIScreen mainScreen] bounds].size.width/100)*2;
                
                
                list.title.frame = frame;
                list.title.center = CGPointMake(([[UIScreen mainScreen] bounds].size.width/100)*50, ([[UIScreen mainScreen] bounds].size.height/100)*20.5);
                
                [list.title removeFromSuperview];
                
            }
//            else if([[MyPref valueForKey:@"bankselect"] isEqualToString:@"1"])
//            {
//                
//                
//                frame.origin.y = ([[UIScreen mainScreen] bounds].size.height/100)*17;
//                frame.size.width = [titlee sizeWithFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]].width;
//                 frame.size.width = frame.size.width+([[UIScreen mainScreen] bounds].size.width/100)*7;
//                
//                list.title.frame = frame;
//                
//                title.center = CGPointMake(([[UIScreen mainScreen] bounds].size.width/100)*50, ([[UIScreen mainScreen] bounds].size.height/100)*20);
//                
////                list.title.center = CGPointMake(([[UIScreen mainScreen] bounds].size.width/100)*50, ([[UIScreen mainScreen] bounds].size.height/100)*20.5);
//            }
            else if([titlee isEqualToString:@"CATAGORY"])
            {
                NSLog(@"in selected category");
                
                
                frame.origin.y = ([[UIScreen mainScreen] bounds].size.height/100)*17;
                frame.size.width = [titlee sizeWithFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]].width;
                frame.size.width = frame.size.width+([[UIScreen mainScreen] bounds].size.width/100)*6;
                
                
                list.title.frame = frame;
                list.title.center = CGPointMake(([[UIScreen mainScreen] bounds].size.width/100)*50, ([[UIScreen mainScreen] bounds].size.height/100)*14);
            }
            else if ([titlee isEqualToString:@"BANK LIST"])
            {
                frame.origin.y = ([[UIScreen mainScreen] bounds].size.height/100)*17;
                frame.size.width = [titlee sizeWithFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]].width;
                frame.size.width = frame.size.width+([[UIScreen mainScreen] bounds].size.width/100)*6;
                
                
                list.title.frame = frame;
                list.title.center = CGPointMake(([[UIScreen mainScreen] bounds].size.width/100)*50, ([[UIScreen mainScreen] bounds].size.height/100)*14);
            }
            else if ([titlee length] !=0)
            {
               
                NSLog(@"in selected category and title %@ and list title is :%@",titlee, list.title.text);
                
                frame.size.width = [list.title.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]].width;
                frame.size.width = frame.size.width+([[UIScreen mainScreen] bounds].size.width/100)*6;
                
                
                list.title.frame = frame;
                
                if ([[MyPref valueForKey:@"bankselect"] isEqualToString:@"1"]) {
                    
                    
                    frame.origin.y = ([[UIScreen mainScreen] bounds].size.height/100)*17;
                    list.title.center = CGPointMake(([[UIScreen mainScreen] bounds].size.width/100)*50, ([[UIScreen mainScreen] bounds].size.height/100)*20);
                    
                }
                else{
                    
                    if (![[MyPref valueForKey:@"isNewUser"]  isEqualToString:@"1"]) {
                         list.title.center = CGPointMake(([[UIScreen mainScreen] bounds].size.width/100)*50, ([[UIScreen mainScreen] bounds].size.height/100)*20);
                    }
                    else{
                       list.title.center = CGPointMake(([[UIScreen mainScreen] bounds].size.width/100)*50, ([[UIScreen mainScreen] bounds].size.height/100)*14);
                    }
                    
                    frame.origin.y = ([[UIScreen mainScreen] bounds].size.height/100)*17;
                   
                }
                
               
                
                
            }
            
          });
        
    }
      
   
    if (view !=nil) {
         dispatch_async(dispatch_get_main_queue(), ^{
        [list addSubview:view];
             
             if ([titlee isEqualToString:@"home"]) {
                 
                 
                 list->backButton.hidden = YES;
                 [list->backButton removeFromSuperview];
                 
             }
             else if([[MyPref valueForKey:@"firstlogin"] isEqualToString:@"1"])
             {
                 list->backButton.hidden = YES;
                 [list->backButton removeFromSuperview];
             }
             else{
                 
                 NSLog(@"titlee is not home");
                 
                 
             }

             
//             [list addSubview:list.search_bar];
             
         });
        

    }else
    {
        NSLog(@"view is nil loading only bank list");
        
    }
        
        
        
    });
//           [list popoverWindow];
    
}



@end
