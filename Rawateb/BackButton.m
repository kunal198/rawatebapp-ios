//
//  BackButton.m
//  Rwewtab
//
//  Created by Rohit Mishra on 4/18/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import "BackButton.h"

@implementation BackButton
@synthesize  stack;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


+ (id)sharedManager {
    static BackButton *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


- (id)init {
    if (self = [super init]) {
        
        stack = [[NSMutableArray alloc]init];
        
        
        
        
    }
    return self;
}


        
-(void) previousView :(UIView *)view1
{
    view = (UIView *)view1;
    
}

-(void) goBack : (UIView *)vieww :(NSString *)category
{
    [stack addObject:category];
    
    
    NSLog(@"going to backin the view with stack entrance%@", [stack lastObject]);
    
}

-(UIView *) back
{
    
    NSLog(@"lastobject of the stack is :%@", [stack lastObject]);
    [stack removeLastObject];
    
    UIView *backview = (UIView *)[stack lastObject];
    
    NSLog(@"after removing stack is :%@",stack);
    
    
    return backview;
    
}


@end
