//
//  AppDelegate.m
//  Rwewtab
//
//  Created by Neuerung on 4/8/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import "AppDelegate.h"
#import "MBProgressHUD.h"
#define Backgroundimage @"http://admin.elatikprojects.com/api/v1/get_background"
#define DEVICEINFO @"http://admin.elatikprojects.com/api/v1/deviceInfo"
#define BASE_URL @"http://elatikprojects.com"
#import "AdvertisementView.h"
#define GET_CATE @"http://admin.elatikprojects.com/api/v1/get_cate"
#define BANK_REGISTER @"http://admin.elatikprojects.com/api/v1/get_banks"
#define REGISTER @"http://admin.elatikprojects.com/api/v1/register"
#import "ViewController.h"
#import "LogInView.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
//static NSBundle *bundle = nil;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    
    [MyPref setInteger:0 forKey:@"banner_enable"];
    [MyPref synchronize];
    
    
    
   
    [MyPref setInteger:0 forKey:@"banner"];
    [MyPref synchronize];
   
    

    //to remove icluod backup of local directories
    NSArray *urlArray = [[NSFileManager defaultManager] URLsForDirectory: NSDocumentDirectory inDomains: NSUserDomainMask];
    NSURL *documentsUrl = [urlArray firstObject];
    
    NSError *error = nil;
    BOOL success;
    [documentsUrl setResourceValue: [NSNumber numberWithBool: YES]
                            forKey: NSURLIsExcludedFromBackupKey error: &error];
    
    success=YES;
    
    if(!success){
        NSLog(@"Error in disabling %@ from backup %@", [documentsUrl lastPathComponent], error);
    }
    

    
    // iOS 8 Porting
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
        
    }
    
    
    
    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (localNotif) {
        NSLog(@"Clicked on Notification when app is closed ...... and app starts:%@",[launchOptions valueForKey:@"aps"]);
        //[self AddNotificationToDB:localNotif];
        NSDictionary *remoteNotificationDict = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
        
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Notification Message:" message:[NSString stringWithFormat:@"%@", remoteNotificationDict] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
        
     
        
        NSDictionary *notificationData =[remoteNotificationDict valueForKey:@"aps" ];
      
        
      
        BOOL banner_enable = [[notificationData valueForKey:@"banner_enable"] boolValue];
        [MyPref setInteger:banner_enable forKey:@"banner_enable"];
        [MyPref synchronize];
        
      
        
        NSInteger banner = [[notificationData valueForKey:@"banner"] integerValue];
        [MyPref setInteger:banner forKey:@"banner"];
        [MyPref synchronize];
        
    }
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
    
    
    
}

- (void)application:(UIApplication *) application handleActionWithIdentifier: (NSString *) identifier forRemoteNotification: (NSDictionary *) notification completionHandler: (void (^)()) completionHandler {
    
    
    
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
    
}



- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    
    NSLog(@"my device tocken is %@",deviceToken);
    
    NSString* deviceTokenStr = [deviceToken description];
    
    
    deviceTokenStr = [deviceTokenStr stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    deviceTokenStr = [deviceTokenStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"actual device token is :%@", deviceTokenStr);
 
    
   
    
    [MyPref setObject:deviceTokenStr forKey:@"DevToken"];
    
    [MyPref synchronize];
    
}

- (void)application:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Device Token" message:[NSString stringWithFormat:@"failed to register for remote notifications error :%@", error ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//    [alert show];
    NSLog(@"Failed to register: %@", error);
}

- (NSString *)modeString
{
#if DEBUG
    return @"Development (sandbox)";
#else
    return @"Production";
#endif
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
//    NSDate *todayDate = [NSDate date]; //Get todays date
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date.
//    [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm a"]; //Here we can set the format which we need
//    NSString *str = [dateFormatter stringFromDate:todayDate];// Here convert date in NSString
//    
//    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
//    [formatter setDateFormat:@"dd-MM-yyyy hh:mm a"];
//    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
//    [formatter setTimeZone:gmt];
//    NSDate *date = [formatter dateFromString:str];
//    
//    NSLog(@"%@",date);
    
    if(  [[NSUserDefaults standardUserDefaults]boolForKey:@"LoginDone"])
    {
        if ([[NSUserDefaults standardUserDefaults]boolForKey:@"isapplogin"] == false)
        {
            UILocalNotification *localNotif = [[UILocalNotification alloc] init];
            localNotif.fireDate = [NSDate dateWithTimeIntervalSinceNow:10];
            localNotif.timeZone =[NSTimeZone localTimeZone];
            localNotif.alertBody =NSLocalizedString(@"Local Notification", nil);;
            //    localNotif.userInfo=userDict;
            localNotif.soundName=UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
            
            
            NSLog(@"llllllllllllllllllllllll %@",[[[UIApplication sharedApplication] scheduledLocalNotifications] copy]);
        }
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    if(  [[NSUserDefaults standardUserDefaults]boolForKey:@"LoginDone"])
    {
        if ([[NSUserDefaults standardUserDefaults]boolForKey:@"isapplogin"] == false)
        {
            //        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isapplogin"];
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            UILocalNotification *localNotif = [[UILocalNotification alloc] init];
            localNotif.fireDate = [NSDate dateWithTimeIntervalSinceNow:10];
            localNotif.timeZone =[NSTimeZone localTimeZone];
            
            localNotif.alertBody =NSLocalizedString(@"Local Notification", nil);
            //    localNotif.userInfo=userDict;
            localNotif.soundName=UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
            
            
            NSLog(@"llllllllllllllllllllllll %@",[[[UIApplication sharedApplication] scheduledLocalNotifications] copy]);
            
            return;
        }
    }
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    
    
    NSLog(@"url received is :%@", url);
    
    
    
    return YES;
    
}


- (BOOL)application:(UIApplication *) application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    NSLog(@"url to be clicked is :%@", url);
    
    
    return YES;
    
}



+(void)setLanguage:(NSString *)l {
    NSLog(@"preferredLang: %@", l);
//    NSString *path = [[ NSBundle mainBundle ] pathForResource:l ofType:@"lproj" ];
//  bundle = [[NSBundle bundleWithPath:path] retain];
}

//+(NSString *)get:(NSString *)key alter:(NSString *)alternate {
//    return [bundle localizedStringForKey:key value:alternate table:nil];
//}

@end
