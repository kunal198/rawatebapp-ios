//
//  Dashboard.h
//  Rwewtab
//
//  Created by Rohit Mishra on 4/18/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

//#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "MBProgressHUD.h"


@interface Dashboard : UIView<UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>
{
 UITableView *dashboardTable;  
    NSArray *rowTitles;
    NSArray *imageArray;
    MFMailComposeViewController *mailcontroller;
    NSDictionary *graduate_list;
    NSArray *graduate_array;
    
    
    NSDictionary *recruiter_list;
    NSArray *recruiter_array;
    
    
    //Advertisement api
    NSDictionary *advertise_list;
    NSArray *adverstise_array;
    MBProgressHUD *progress;
    
    
    
    
}
-(id) initWithFrame:(CGRect)frame;

@property (nonatomic, strong) NSDictionary *bank_list;

@property (nonatomic, strong) NSArray *banks_array;

-(void)bank_salary;

-(void) recruiters;

-(void) graduates;

-(void) advertisement;

-(void) signOut;

-(void) home;


@end
