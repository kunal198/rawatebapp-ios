//
//  AdvertisementView.m
//  Rwewtab
//
//  Created by Neuerung on 4/8/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import "AdvertisementView.h"
#import "UIColor+Additions.h"


#define GET_BANNER @"http://admin.elatikprojects.com/api/v1/get_banner"
#define BASE_URL @"http://elatikprojects.com"
#define IMPRESSION @"http://admin.elatikprojects.com/api/v1/impression"
#define CLICK @"http://admin.elatikprojects.com/api/v1/click"

@implementation AdvertisementView
@synthesize index, add_array, image_url, image_data;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}*/

-(id) initWithFrame:(CGRect)frame :(NSString *)ad_unit add_arrray:(NSArray *)ad_array{
    self =[super initWithFrame:frame];
    if(self)
    {
        
        
       
        
        
        advertisement = [[UIImageView alloc]initWithFrame:frame];
//
        advertisement.frame = self.frame;
        
advertisement.contentMode = UIViewContentModeScaleToFill;

        
       
        UITapGestureRecognizer *imageTapped =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clicked)];
        
        [advertisement addGestureRecognizer:imageTapped];
        
            
            int indexx = 0;
        int arrind = 1;
        int promoFlag=0;
        
        
        NSLog(@"add array count isd :%d", (int)[ad_array count]);
        
        
        if([ad_unit isEqualToString:@"Notification_banner"])
        {
            
            for (indexx=0; indexx<=(int)[ad_array count]-1; indexx++) {
                NSLog(@"indexx value is :%d", indexx);
                
                
                NSDictionary *image_dict = [ad_array objectAtIndex:indexx];
               
                
                if ([[image_dict valueForKey:@"ad_unit"] isEqualToString:@"promotion"]) {
                    
                    if ([add_array count] == 0) {
                        image_dict = nil;
                        
                        for (image_dict in ad_array) {
                           
                            
                            if ([[image_dict valueForKey:@"id"] integerValue] == [[MyPref valueForKey:@"banner"] integerValue]) {
                                add_array = [NSMutableArray arrayWithObject:image_dict];
                              
                                
                            }else{
                                if ([[image_dict valueForKey:@"ad_unit"] isEqualToString:@"promotion"]) {
                                    if ([add_array count] > 1) {
                                        
                                        for (NSDictionary *test in add_array) {
                                            if([[test valueForKey:@"id"] integerValue] == [[image_dict valueForKey:@"id"] integerValue])
                                            {
                                                promoFlag = 1;
                                            }
                                        }
                                        if (promoFlag !=1) {
                                            [add_array insertObject:image_dict atIndex:arrind];
                                            
                                        }
                                        
                                    }
                                    else{
                                [add_array insertObject:image_dict atIndex:arrind];
                                    }
                                arrind++;
                                    promoFlag=0;
                                }
                                
                            }
                            
                            
                        }
                        
                    }
                    else{
                        if (!([[image_dict valueForKey:@"id"] integerValue] == [[MyPref valueForKey:@"banner"] integerValue] )) {
                            if ([[image_dict valueForKey:@"ad_unit"] isEqualToString:@"promotion"]) {
                                if ([add_array count] > 1) {
                                    
                                    for (NSDictionary *test in add_array) {
                                        if([[test valueForKey:@"id"] integerValue] == [[image_dict valueForKey:@"id"] integerValue])
                                        {
                                            promoFlag = 1;
                                        }
                                    }
                                    if (promoFlag !=1) {
                                        [add_array insertObject:image_dict atIndex:arrind];
                                        
                                    }
                                    
                                }
                                else{
                                    [add_array insertObject:image_dict atIndex:arrind];
                                }
                                arrind++;
                                promoFlag=0;
                       
                            }
                        }
                        else
                        {
                            image_dict = nil;
                            
                            for (image_dict in ad_array) {
                              
                                
                                if ([[image_dict valueForKey:@"id"] integerValue] == [[MyPref valueForKey:@"banner"] integerValue]) {
                                    [add_array insertObject:image_dict atIndex:0];
                                    
                                    
                                    
                                }
                            }
                        }
                    }
                    
                
                
                }
                
                
                
            }
            
            
            
        }
        else{
            
            for (indexx=0; indexx<=(int)[ad_array count]-1; indexx++) {
                
                NSDictionary *image_dict = [ad_array objectAtIndex:indexx];
                
                if ([[image_dict valueForKey:@"ad_unit"] isEqualToString:ad_unit]) {
                    
                    if ([add_array count] == 0) {
                        add_array = [NSMutableArray arrayWithObject:image_dict];
                    }
                    else{
                        
                        [add_array addObject:image_dict];
                        
                    }
                    
                    
                }
                
                
                
            }
        }
        
        NSLog(@"add arrya from all sections is :%@", add_array);
        
       
        
               
        
        
        
//        advertisement.frame = framee;
        
        NSLog(@"y position of the advertisement is :%f", advertisement.frame.origin.y);
        
        
        
        swiperight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipped:)];
        swipeleft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipped:)];
        
        [swiperight setDirection:UISwipeGestureRecognizerDirectionRight];
        [swipeleft setDirection:UISwipeGestureRecognizerDirectionLeft];
        
        
        advertisement.userInteractionEnabled =YES;
        advertisement.clipsToBounds = YES;
        [advertisement addGestureRecognizer:swiperight];
        [advertisement addGestureRecognizer:swipeleft];
        
       
        
        
        
        
        [self addSubview:advertisement];
        
        
//        advertisement.contentMode = UIViewContentModeScaleToFill;
        
//
        
        
        
        
        
        NSLog(@"class string is %@", self.clas);
        
        
//        if ([[MyPref valueForKey:@"start"] isEqualToString:@"start"]) {
//            NSLog(@"same class");
//
//        pagecontrol = [[UIPageControl alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*38, ([[UIScreen mainScreen] bounds].size.height/100)*75, ([[UIScreen mainScreen] bounds].size.width/100)*20, ([[UIScreen mainScreen] bounds].size.height/100)*10)];
//        pagecontrol.numberOfPages = [add_array count];
//        }
//        else{
        
        if ([[MyPref valueForKey:@"start"] isEqualToString:@"start"]) {
            pagecontrol = [[UIPageControl alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*38, (self.bounds.size.height/100)*97, ([[UIScreen mainScreen] bounds].size.width/100)*20, ([[UIScreen mainScreen] bounds].size.height/100)*13)];
            pagecontrol.numberOfPages = [add_array count];
            
            
            CGRect framee = CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*0, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*80);
            
            framee.origin.y -=([[UIScreen mainScreen] bounds].size.height/100)*0;
            framee.size.height = ([[UIScreen mainScreen] bounds].size.height/100)*86;
//           framee.size.width -= ([[UIScreen mainScreen] bounds].size.height/100)*2;
            advertisement.frame = framee;
//            advertisement.center = self.center;
//            advertisement.center = [[UIApplication sharedApplication] keyWindow].center;
        }
        else{
        
            pagecontrol = [[UIPageControl alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*38, (self.bounds.size.height/100)*90, ([[UIScreen mainScreen] bounds].size.width/100)*20, ([[UIScreen mainScreen] bounds].size.height/100)*10)];
            pagecontrol.numberOfPages = [add_array count];
        
            CGRect framee = advertisement.frame;
            
            framee.origin.y -=([[UIScreen mainScreen] bounds].size.height/100)*8;
            
            framee.size.height +=([[UIScreen mainScreen] bounds].size.height/100)*8;
            
            advertisement.frame = framee;
            advertisement.center = self.center;
            
            
            
        }
        
        
        
        NSLog(@"add array count is :%d",(int) [add_array count]);
        
        
//        }
        
        [self addSubview:pagecontrol];
        pagecontrol.pageIndicatorTintColor = [UIColor colorWithHexString:@"ffc000"];
        
        if([add_array count] == 1)
        {
        
       pagecontrol.currentPage = [add_array count];
            
            pagecontrol.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"ffc000"];
            
        }
        else{
            pagecontrol.currentPage =0;
        }
//        pagecontrol.backgroundColor =[UIColor blackColor];
        
//        self.backgroundColor = [UIColor blackColor];
        
        NSLog(@"current page count is :%d", (int)pagecontrol.currentPage);
        
        
        
     
        index = 0;
        
        
        
    }
         return  self;
    
}

-(void) slidingImageView
{
    
    
    //Getting all the banner from the server to show it on the advertisement view
    
    
      
    NSLog(@"sliding image view method is called with add array %@",add_array);
      dispatch_async(dispatch_get_main_queue(), ^{
    pregress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
    pregress.labelText = @"Processing..";
      });
    
    
    NSDictionary *image_dict = [add_array objectAtIndex:index];
    
    self.currentDictionary = image_dict;self.currentDictionary = image_dict;
    
    
    NSString *impid= [NSString stringWithFormat:@"%@", [image_dict valueForKey:@"id"]];
    
    NSLog(@"impressio id is :%@", impid);
    
    
    [self addImpression:impid];
    
    
    NSString *image_path= [NSString stringWithFormat:@"%@%@", BASE_URL,[image_dict valueForKey:@"path"]];
    
    
    
    NSArray *path_components = [image_path componentsSeparatedByString:@"/"];
    
    
    NSString *local_image_path =[NSString stringWithFormat:@"%@/%@", kDocumentDirectoryPath,[path_components lastObject]];
    
//    if (![[NSFileManager defaultManager] fileExistsAtPath:local_image_path]) {
    
        
        
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            
            if([self connectedToInternet])
            {
            
            for (int i = 0; i<(int)[add_array count]; i++) {
                
                
                NSDictionary *image_dict = [add_array objectAtIndex:i];
                NSString *image_path= [NSString stringWithFormat:@"%@%@", BASE_URL,[image_dict valueForKey:@"path"]];
                
                image_url = [NSURL URLWithString:image_path];
                
                image_data = [NSData dataWithContentsOfURL:image_url];
                
                NSArray *path_components = [image_path componentsSeparatedByString:@"/"];
                
                
                NSString *local_image_path =[NSString stringWithFormat:@"%@/%@", kDocumentDirectoryPath,[path_components lastObject]];
                
                if (![[NSFileManager defaultManager] fileExistsAtPath:local_image_path]) {
                    [[NSFileManager defaultManager] createFileAtPath:local_image_path contents:image_data attributes:nil];
                    
                }
                else
                {
                    [[NSFileManager defaultManager] removeItemAtPath:local_image_path error:nil];
                    [[NSFileManager defaultManager] createFileAtPath:local_image_path contents:image_data attributes:nil];
                    
                    
                }
                
                
            }
            }
            else{
                 dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You are not connected to internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                 });
                
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                
                [pregress hide:YES];
                [pregress removeFromSuperview];
                advertisement.image = [UIImage imageWithContentsOfFile:local_image_path];
                
                
                
            });
            
            
        });
        
        
//    }
//    else{
//        
//        NSLog(@"local image path is :%@", local_image_path);
//        
//        
//        
//        advertisement.image = [UIImage imageWithContentsOfFile:local_image_path];
//        
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            [pregress hide:YES];
//            [pregress removeFromSuperview];
//            advertisement.image = [UIImage imageWithContentsOfFile:local_image_path];
//            
//            
//            
//        });
//        
//    }
    
    

}



-(void)swipped: (UISwipeGestureRecognizer *)sender
{
    NSLog(@"image is swiped we need to show another image for the image view");
    
    UISwipeGestureRecognizerDirection direction = [(UISwipeGestureRecognizer *)sender direction];
    
    if (index <=[add_array count]-1) {
       
   
    switch (direction) {
        case UISwipeGestureRecognizerDirectionLeft:
            
            if (index !=[add_array count]-1) {
                NSLog(@"index value here is :%d", index);
                
                if (index == [add_array count]-1) {
                    
                }
                else{
            index++;
                }
                NSDictionary *image_dict = [add_array objectAtIndex:index];
               
                self.currentDictionary = image_dict;
                
                NSString *impid= [NSString stringWithFormat:@"%@", [image_dict valueForKey:@"id"]];
                
                NSLog(@"impressio id is :%@", impid);
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self addImpression:impid];
                });
                               
                
                 NSString *image_path= [NSString stringWithFormat:@"%@%@", BASE_URL,[image_dict valueForKey:@"path"]];
                
                NSArray *path_components = [image_path componentsSeparatedByString:@"/"];
                
                
                NSString *local_image_path =[NSString stringWithFormat:@"%@/%@", kDocumentDirectoryPath,[path_components lastObject]];
                CATransition *transition = [CATransition animation];
                transition.duration = 0.4;
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromRight;
                [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
                [advertisement.layer addAnimation:transition forKey:nil];
                

                
                
                advertisement.image =[UIImage imageWithContentsOfFile:local_image_path];
                
                
            NSLog(@"swiped to left %d", (int)pagecontrol.currentPage);
            pagecontrol.currentPage += 1;
                 }
            break;
            
        case UISwipeGestureRecognizerDirectionRight:
            if (index != 0) {
            
                NSLog(@"index value is :%d", index);
                
                
            
            index--;
                NSDictionary *image_dict = [add_array objectAtIndex:index];
                self.currentDictionary = image_dict;
                
                NSString *image_path= [NSString stringWithFormat:@"%@%@", BASE_URL,[image_dict valueForKey:@"path"]];
                
                NSString *impid= [NSString stringWithFormat:@"%@", [image_dict valueForKey:@"id"]];
                
                NSLog(@"impressio id is :%@", impid);
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self addImpression:impid];
                });
                
                    
                NSArray *path_components = [image_path componentsSeparatedByString:@"/"];
                
                
                NSString *local_image_path =[NSString stringWithFormat:@"%@/%@", kDocumentDirectoryPath,[path_components lastObject]];
                
                CATransition *transition = [CATransition animation];
                transition.duration = 0.4;
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromLeft;
                [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
                [advertisement.layer addAnimation:transition forKey:nil];
                

                
                advertisement.image =[UIImage imageWithContentsOfFile:local_image_path];
                
            NSLog(@"swipped to right and current pagfe is :%d", (int)pagecontrol.currentPage);
            pagecontrol.currentPage -= 1;
                    }
            break;
        
        default:
            break;
    }
     }
    else{
        NSLog(@"reached to the max swipe");
        
    }
    
    if (index >0) {
        
        NSLog(@"change image according to the indeex of the swipe");
        
        
        
    }
    
    
    
    
}




-(void) addImpression :(NSString *)addid
{
    NSLog(@"impresssiong ading to thye server here" );
    
//    if ([[MyPref valueForKey:@"api_key"] length] !=0) {
    
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    
    [request setURL:[NSURL URLWithString:IMPRESSION]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:[MyPref valueForKey:@"api_key"] forHTTPHeaderField:@"Authorization-Basic"] ;
    
    NSLog(@"actual request is :%@", [request allHTTPHeaderFields]);
    //Setting body for the request
    
    
    
    
    
    
    
    
    
    
    
    NSMutableData *body = [NSMutableData data];
    
    
    NSString *bodydata = [NSString stringWithFormat:@"adId=%@",addid];
    
    
    
    
    
    
    
    [body appendData:[bodydata dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    
    [request setHTTPBody:body];
    
    NSString *bodyofrequest = [[NSString alloc]initWithData:body encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"request body is:%@", bodyofrequest);
    
    NSLog(@"actual request is :%@", [request allHTTPHeaderFields]);
    
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    
    //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
    
    
    if([self connectedToInternet])
    {
    
    NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
        
        NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        
        
        NSLog(@"response from server for impression  is :%@ \nand data from the server is%@", response,
              data_server);
        
        
        
        
    }]resume];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You are not connected to internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }

//    }
//    else{
//        NSLog(@"api key is nil can not send imporession");
//        
//    }
//    
    
    
}


-(void) clicked
{
    NSLog(@"clicked on the image need to send the report to server");
   
    NSString *openurlforsafari;
    
    if (![[self.currentDictionary valueForKey:@"url"] hasPrefix:@"http"]) {
        
        
        openurlforsafari = [NSString stringWithFormat:@"https://%@",[self.currentDictionary valueForKey:@"url"] ];
    }
    else
    {
        openurlforsafari = [NSString stringWithFormat:@"%@",[self.currentDictionary valueForKey:@"url"] ];
    }
    
    NSLog(@"url to open is :%@", openurlforsafari);
    
    
    UIApplication *app =[UIApplication sharedApplication];
    
    NSURL *url = [NSURL URLWithString:openurlforsafari];
    
    
    [app openURL:url];

    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    
    [request setURL:[NSURL URLWithString:CLICK]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    //    [request setValue:[MyPref valueForKey:@"api_key"] forHTTPHeaderField:@"Authorization-Basic"] ;
    
    NSLog(@"actual request is :%@", [request allHTTPHeaderFields]);
    //Setting body for the request
    
    
    
    
    
    
    
    
    
    
    
    NSMutableData *body = [NSMutableData data];
    
    
    NSString *bodydata = [NSString stringWithFormat:@"adId=%@",[self.currentDictionary valueForKey:@"id"]];
    
    
    
    
    
    
    
    [body appendData:[bodydata dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    
    [request setHTTPBody:body];
    
    NSString *bodyofrequest = [[NSString alloc]initWithData:body encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"request body is:%@", bodyofrequest);
    
    NSLog(@"actual request is :%@", [request allHTTPHeaderFields]);
    
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    
    //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
    
    if ([self connectedToInternet]) {
        
   
    
    NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
        
        NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        
        
        NSLog(@"response from server for impression  is :%@ \nand data from the server is%@", response,
              data_server);
        
        
        
        
    }]resume];
       
       }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You are not connected to internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }

    
}

- (BOOL)connectedToInternet
{
    NSString *urlString = @"http://www.google.com/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    
    return ([response statusCode] == 200) ? YES : NO;
}

@end
