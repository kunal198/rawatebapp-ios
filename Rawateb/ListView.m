//
//  ListView.m
//  Rwewtab
//
//  Created by Rohit Mishra on 4/29/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import "ListView.h"
#import "UIColor+Additions.h"
#import "BackButton.h"
#import "BankList.h"
#import "Home.h"
#define GET_CATE @"http://admin.elatikprojects.com/api/v1/get_cate"
#define GET_SALARYSTATUS @"http://admin.elatikprojects.com/api/v1/getSalaryStatus_new"
#define BASE_URL @"http://elatikprojects.com/uploads"
#import "RecruiterList.h"
#define GET_GRADUATES @"http://admin.elatikprojects.com/api/v1/get_graduate"
#define GET_RECRUITER @"http://admin.elatikprojects.com/api/v1/get_recruiter"
#define GET_USER_UPDATE @"http://admin.elatikprojects.com/api/v1/update_user"

@implementation ListView
@synthesize catagory, banks_list,cates_array,graduates_array, graduateSearch,searchbar;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/




-(id) initWithFrame:(CGRect)frame catagory:(NSString *)cataagory
{
    self =[super initWithFrame:frame];
    if(self)
    {
        
       
        if (![[MyPref valueForKey:@"selection"] isEqualToString:@"0"]) {
    
    
            if ([cataagory isEqualToString:@"graduates"]) {
                
                NSLog(@"having catagory graduates");
                
                
                bankslist = [[UITableView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*2, ([[UIScreen mainScreen] bounds].size.height/100)*16, ([[UIScreen mainScreen] bounds].size.width/100)*95, ([[UIScreen mainScreen] bounds].size.height/100)*70)];
                
                
                
                searchbar =   [[UISearchBar alloc]initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width/100)*-2.5, ([UIScreen mainScreen].bounds.size.height/100)*0, ([UIScreen mainScreen].bounds.size.width/100)*105, ([UIScreen mainScreen].bounds.size.height/100)*5)];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                  
                    searchbar.barStyle = UISearchBarStyleMinimal;
                    searchbar.searchBarStyle = UISearchBarIconSearch;

                    
                    searchbar.backgroundColor = [UIColor clearColor];
                    
                    
                    searchbar.barTintColor = [UIColor clearColor];
                    
                    searchbar.alpha = 0.4f;
                    
                    
                    [searchbar isTranslucent];
                    searchbar.delegate = self;
                    
                    
                [self addSubview:searchbar];
                });
                
                
                
            }
            else if ([cataagory isEqualToString:@"Otherrecruiters"])
            {
                bankslist = [[UITableView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*18, ([[UIScreen mainScreen] bounds].size.width/100)*95, ([[UIScreen mainScreen] bounds].size.height/100)*70)];
                
               
                
                searchbar =   [[UISearchBar alloc]initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width/100)*-2.5, ([UIScreen mainScreen].bounds.size.height/100)*2, ([UIScreen mainScreen].bounds.size.width/100)*105, ([UIScreen mainScreen].bounds.size.height/100)*5)];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    searchbar.barStyle = UISearchBarStyleMinimal;
                    searchbar.searchBarStyle = UISearchBarIconSearch;
                    
                    
                    searchbar.backgroundColor = [UIColor clearColor];
                    
                    
                    searchbar.barTintColor = [UIColor clearColor];
                    
                    searchbar.alpha = 0.4f;
                    
                    
                    [searchbar isTranslucent];
                    searchbar.delegate = self;
                    
                    
                    [self addSubview:searchbar];
                });
                
            }
            else{
    bankslist =[[UITableView alloc]initWithFrame:self.bounds];
            }
    
    [self addSubview:bankslist];
    }
        self.backgroundColor = [UIColor clearColor];
        
        bankslist.backgroundColor = [UIColor clearColor];

        bankslist.delegate=self;

        bankslist.dataSource = self;
        
        
//      bankslist.allowsSelection = NO;
       
        
        searchbar.delegate = self;
        
    }
    return self;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
//    if ([catagory isEqualToString:@"recruiters"]) {
//        
//        if ([filteredResult count] !=0) {
//            return [filteredResult count];
//            
//        }
//        else{
//        
//        return [cates_array count];
//        }
//    }
//    else if ([catagory isEqualToString:@"graduates"]) {
//      return [graduates_array count];
//    }
//    else if ([catagory isEqualToString:@"advertisement"]) {
//       return [graduates_array count];
//    }
//    else if([catagory isEqualToString:@"bank_salary"])
//        
//    {
//        
//    
//    return [banks_list count];
//    }
    return 1;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([catagory isEqualToString:@"recruiters"]) {
        
        if ([filteredResult count] !=0) {
            return [filteredResult count];
            
        }
        else{
//            graduateSearch = nil;
            return [cates_array count];
        }
    }
    else if ([catagory isEqualToString:@"graduates"]) {
        
        NSLog(@"graduate array count isd :%d", (int)[graduates_array count]);
         if ([filteredResult count] !=0) {
             
             return [filteredResult count];
             
                 }
         else{
             if ([graduates_array count] ==0) {
                 tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
                 
                 tableView.hidden = YES;
                 
             }
             else{
//                 graduateSearch = nil;
                 return [graduates_array count];
             }

         }
       
    }
    else if ([catagory isEqualToString:@"advertisement"]) {
        return 27;
    }
    else if([catagory isEqualToString:@"bank_salary"])
        
    {
        
        
        return [banks_list count];
    }
    return 0;

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"number of sections in the tableview are %d", (int)[tableView numberOfSections]);
    
    
    NSLog(@"table created");
    
    
    static NSString *cellidentifier = @"cell";
    
    NSDictionary *cell_elements;
    
    UITableViewCell *cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    
    [tableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    UILabel *cellText = [[UILabel alloc]initWithFrame:CGRectMake((cell.bounds.size.width/100)*0 ,(cell.bounds.size.height/100)*0, ([UIScreen mainScreen].bounds.size.width/100)*100, (cell.bounds.size.height/100)*100)];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    if ([catagory isEqualToString:@"recruiters"]) {
        
        //search bar  logics
        
        
        
        
        
        NSDictionary *cate_dict = [cates_array objectAtIndex:indexPath.section];
        
        NSString  *arabicBank;
        @try{
        if([cate_dict valueForKey:@"keyword"] !=NULL && ![[cate_dict valueForKey:@"keyword"] isEqualToString:@"<null>"])
        {

         arabicBank = [NSString stringWithCString:[[cate_dict valueForKey:@"keyword"] cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
            NSLog(@"arabic bank in category secion is :%@", arabicBank);
        }
            NSLog(@"arabic bank after if in category section is: %@", arabicBank);
            
        } @catch (NSException *exception) {
            NSLog(@"exception occured because of null string %@", exception);
            
        } @finally {
            NSLog(@"finallly executed");
            
        }
        NSLog(@"filter results srray is :%@", filteredResult);
        
        
        if ([filteredResult count] !=0) {
       
            
        
            
            
            cell.textLabel.text =[filteredResult objectAtIndex:indexPath.section];
            
            
        }
        else
        {
           
            
            

            cell.textLabel.text = arabicBank;
            
            
        }
        
        cell.textLabel.textColor =  [UIColor whiteColor];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        if ([tableData count]  == 0) {
            tableData=[[NSMutableArray alloc]init];
            
            
            [tableData addObject:[cate_dict valueForKey:@"category"]];
            
        }
        else{
            
            [tableData addObject:[cate_dict valueForKey:@"category"]];
            
        }
        
        UIView *backgroundView = [[UIView alloc]init];
        backgroundView.backgroundColor = [UIColor blackColor];
        backgroundView.alpha = 0.4f;
        
        cell.backgroundView = backgroundView;

        
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:textSizeLarge];
        tableView.separatorColor =  [UIColor clearColor];
    }
    else if ([catagory isEqualToString:@"graduates"]) {
        
        cell.userInteractionEnabled =NO;
        
        
        if ([filteredResult count] !=0) {
            
            cellText.textColor =  [UIColor whiteColor];
            cellText.textAlignment = NSTextAlignmentCenter;
            
            UIView *backgroundView = [[UIView alloc]init];
            backgroundView.backgroundColor = [UIColor blackColor];
            backgroundView.alpha = 0.4f;

            
            cell.backgroundView = backgroundView;
            
            cellText.font = [UIFont fontWithName:@"Helvetica" size:textSizeLarge];
            tableView.separatorColor =  [UIColor clearColor];
            
            
            cellText.text =[filteredResult objectAtIndex:indexPath.section];

            
            
            
        }
        else{
        
            
        
        
        NSDictionary *graduate = [graduates_array objectAtIndex:indexPath.section];
            NSString  *arabicGrad ;
            
            @try {
                
           
            if([graduate valueForKey:@"name"] !=NULL && ![[graduate valueForKey:@"name"] isEqualToString:@"<null>"])
            {
            
       arabicGrad = [NSString stringWithCString:[[graduate valueForKey:@"name"] cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
            }
            } @catch (NSException *exception) {
                NSLog(@"exception occured because of null string %@", exception);
                
            } @finally {
                NSLog(@"finallly executed");
                
            }
            NSLog(@"arabic graduastion is :%@ and gradutae dict is :L%@", arabicGrad, graduate);
            
        
           

            cellText.text =[graduate valueForKey:@"name"] ;
            
            cellText.textColor =  [UIColor whiteColor];
            cellText.textAlignment = NSTextAlignmentCenter;
            
            cellText.font = [UIFont fontWithName:@"Helvetica" size:textSizeLarge];
            
        UIView *backgroundView = [[UIView alloc]init];
        backgroundView.backgroundColor = [UIColor blackColor];
        backgroundView.alpha = 0.4f;
        
        cell.backgroundView = backgroundView;
            
        tableView.separatorColor =  [UIColor clearColor];
        
        }
    }
    else if ([catagory isEqualToString:@"advertisement"]) {
        cell.textLabel.textColor =  [UIColor whiteColor];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        UIView *backgroundView = [[UIView alloc]init];
        backgroundView.backgroundColor = [UIColor blackColor];
        backgroundView.alpha = 0.4f;
        
        cell.backgroundView = backgroundView;
        
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:textSizeLarge];
        tableView.separatorColor =  [UIColor clearColor];
        cell.textLabel.text = @"advertisement";
    }
    else if([catagory isEqualToString:@"bank_salary"])
     
    {
        
       
        cell_elements = [banks_list objectAtIndex:indexPath.section];
        
        const char *ch = [[cell_elements valueForKey:@"keyword"] UTF8String];
        
        NSLog(@"const char from utf8 string i s:%s", ch);
        
//        char cString = [cell_elements valueForKey:@"keyword"];
       
        NSString  *arabicBank;
        @try{
        if([cell_elements valueForKey:@"keyword"] !=NULL && ![[cell_elements valueForKey:@"keyword"] isEqualToString:@"<null>"])
        {
        
        arabicBank = [NSString stringWithCString:[[cell_elements valueForKey:@"keyword"] cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
        NSString *decode_string = [[NSString alloc]initWithCString:ch encoding:NSUTF8StringEncoding];
       
        NSLog(@"%@",decode_string);
        }
        } @catch (NSException *exception) {
            NSLog(@"exception occured because of null string %@", exception);
            
        } @finally {
            NSLog(@"finallly executed");
            
        }
        
        NSLog(@"arabic bank name is :%@", arabicBank);
        
        
        
        cell.textLabel.text = arabicBank;
        
        UIView *backgroundView = [[UIView alloc]init];
        backgroundView.backgroundColor = [UIColor colorWithHexString:[cell_elements valueForKey:@"hex_color"]];
        backgroundView.alpha = 0.4f;
        
        
        
        
        
        cell.backgroundView = backgroundView;
        
//        [cell setAlpha:0.2f];
        cell.textLabel.textColor =  [UIColor whiteColor];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:textSizeLarge];
        tableView.separatorColor =  [UIColor clearColor];
        tableView.scrollEnabled = NO;
        
        
        
      
        
        
        
    }
    
    for (UIView *subview1 in [cell.contentView subviews])
    {
        if ([subview1 isKindOfClass:[UILabel class]])
        {
            [subview1 removeFromSuperview];
        }
        
        
        
        
    }
    
    
    if ([catagory isEqualToString:@"graduates"]) {
        [cell.contentView addSubview:cellText];
        
    
    }

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [searchbar resignFirstResponder];
    
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    [MyPref setValue:@"0" forKey:@"internal_bank_recruiter_list"];
    [MyPref synchronize];
    
    
    NSLog(@"checking for the banks from the list %@", cell.textLabel.text);
    
    NSLog(@"is new user key important is :%@", [MyPref valueForKey:@"isNewUser"]);
    
      NSDictionary *cell_elements = [banks_list objectAtIndex:indexPath.section];
   
    [MyPref setValue:[cell_elements valueForKey:@"name"] forKey:@"selectBank"];
    [MyPref synchronize];
    
    
   
    
    
//    [MyPref  setValue:[cell_elements valueForKey:@"name"] forKey:@"bankname"];
//    [MyPref synchronize];
    
    
    //Checking for the new user in cell selection section
    if ([[MyPref valueForKey:@"isNewUser"]  isEqualToString:@"1"]) {
        
        
        
        
        if ([catagory isEqualToString:@"bank_salary"]) {
            
            [[BankList sharedManager]addView:nil :@"CATAGORY"];
            hud = [[MBProgressHUD alloc]init];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                hud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
                hud.labelText = @"Processing..";
                
                
            });

            
        
            
             NSDictionary *cell_elements = [banks_list objectAtIndex:indexPath.section];
            
            
            [MyPref  setValue:[cell_elements valueForKey:@"name"] forKey:@"bankname"];
            [MyPref synchronize];
    
            [MyPref  setValue:[cell_elements valueForKey:@"name"] forKey:@"bank"];
            [MyPref synchronize];
            
            
            
            NSLog(@"nbank and bankname preferences is %@ and %@", [MyPref valueForKey:@"bank"], [MyPref valueForKey:@"bankname"]);
            
            
            
            
            
            
            
//
//
            UIColor *color = cell.backgroundView.backgroundColor;
            
            [MyPref setValue:[self heStringForColor:color] forKey:@"bankcolor"];
                        [MyPref synchronize];

            
            NSLog(@"background color if the bank is :%@", cell.backgroundView.backgroundColor);
            
            NSLog(@"background color of the cell is :%@", cell.backgroundView.backgroundColor);
            
            
            
            
            
        
//        /setting the list for recruiters
        
            
            
            if ([MyPref valueForKey:@"get_category"] != nil) {
                
                
                ListView *recruiters = [[ListView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*2, ([[UIScreen mainScreen] bounds].size.height/100)*20, ([[UIScreen mainScreen] bounds].size.width/100)*95, ([[UIScreen mainScreen] bounds].size.height/100)*80) catagory:@"recruiters"];
                recruiters.catagory = @"recruiters";
                recruiters.cates_array =[MyPref valueForKey:@"get_category"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[BackButton sharedManager] goBack:recruiters :@"recruiters"];
                    [[BankList sharedManager]addView:recruiters :cell.textLabel.text];
                    
                    
                    
                    hud.hidden = YES;
                    
                    
                    [hud removeFromSuperview];
                    
                    
                    hud = nil;
                    
                 
                    
                });

                
            }
            else{
            NSString *getcate = [NSString stringWithFormat:@"%@",GET_CATE];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
            
            [request setURL:[NSURL URLWithString:getcate]];
            [request setHTTPMethod:@"POST"];
            [request setValue:@"aplication/json" forHTTPHeaderField:@"Content-Type"];
            //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                progress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
                progress.labelText = @"Processing..";
            });
            if ([self connectedToInternet]) {
                
            
            NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
                
                NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                
                
                
                NSLog(@"response from server is :%@ \nand data from the server is%@", response,
                      data_server);
                
                
                cate_list = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                
                
                NSLog(@"bank list from the server is :%@", cate_list);
                
                cate_array = [cate_list valueForKey:@"category"];
                NSLog(@"catagory data in array %@", cate_array);
                
                
                for (UIView *view in [[[UIApplication sharedApplication] keyWindow].rootViewController.view subviews]) {
                    [view removeFromSuperview];
                    
                }
                [self removeFromSuperview];
                ListView *recruiters = [[ListView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*2, ([[UIScreen mainScreen] bounds].size.height/100)*20, ([[UIScreen mainScreen] bounds].size.width/100)*95, ([[UIScreen mainScreen] bounds].size.height/100)*80) catagory:@"recruiters"];
                recruiters.catagory = @"recruiters";
                recruiters.cates_array =cate_array;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[BackButton sharedManager] goBack:recruiters :@"recruiters"];
                    [[BankList sharedManager]addView:recruiters :@"CATAGORY"];
                    
                    progress.hidden = YES;
                    
                    
                    [progress removeFromSuperview];
                    
                    
                    
                    hud.hidden = YES;
                    
                    
                    [hud removeFromSuperview];
                    
                    
                    hud = nil;
                    
                    
                   
                    
                });
                
                
            }]resume];
            
            }
            else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    progress.hidden = YES;
                    
                    
                    [progress removeFromSuperview];
                    
                });
                
                
            }
            }

        }
        else if([catagory isEqualToString:@"recruiters"]) {
            
            
            NSLog(@"we are ib the recu=ruiters after bank list");
            
              NSDictionary *cate_dict = [cates_array objectAtIndex:indexPath.section];
            
            [MyPref setValue:cell.textLabel.text   forKey:@"category"];
            [MyPref synchronize];
            
            NSString *encodedCategory = [self encodeToPercentEscapeString:[cate_dict valueForKey:@"category"]];
            
            NSLog(@"encoded category is :%@", encodedCategory);
            
            
            
            
            [MyPref setValue:[cate_dict valueForKey:@"category"]  forKey:@"category_hindi"];
            [MyPref synchronize];
            
            
            NSString *getsalarystatuss = [NSString stringWithFormat:@"%@",GET_USER_UPDATE];
            
            NSMutableURLRequest *requestt = [[NSMutableURLRequest alloc]init];
            
            [requestt setURL:[NSURL URLWithString:getsalarystatuss]];
            [requestt setHTTPMethod:@"POST"];
            [requestt setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [requestt setValue:[MyPref valueForKey:@"api_key"] forHTTPHeaderField:@"Authorization-Basic"] ;
            
            
            //Setting body for the request
            
            
            
            
            
            NSMutableData *bodyy = [NSMutableData data];
            
            NSLog(@"the actual value of hindiu category is :%@", [MyPref valueForKey:@"category_hindi"]);
            
            
            
//            NSDictionary *cate_dictt = [cates_array objectAtIndex:indexPath.section];
            
            
            NSString *bodydataa = [NSString stringWithFormat:@"bank=%@&category=%@",[MyPref valueForKey:@"bankname"],encodedCategory ];
            
            
            
            
            
            
            
            [bodyy appendData:[bodydataa dataUsingEncoding:NSASCIIStringEncoding]];
            
            
            
            
            [requestt setHTTPBody:bodyy];
            
            NSString *bodyofrequestt = [[NSString alloc]initWithData:bodyy encoding:NSUTF8StringEncoding];
            
            
            NSLog(@"request body is of selection after bank:%@", bodyofrequestt);
            
            NSLog(@"actual request is after bank selection:%@", [requestt allHTTPHeaderFields]);
            
            
            
            
            NSString *postLengthh = [NSString stringWithFormat:@"%lu", (unsigned long)[bodyy length]];
            [requestt setValue:postLengthh forHTTPHeaderField:@"Content-Length"];
            
            
            
            //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                progress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
                progress.labelText = @"Processing..";
            });
            
            
            
            
            if ([self connectedToInternet]) {
                
                NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
                
                [[session dataTaskWithRequest:requestt completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
                    
                    
                    
                    NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                    
                    
                    
                    NSLog(@"response from server is after bank selection:%@ \nand data from the server isafter bank selection%@", response,
                          data_server);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        
                        progress.hidden = YES;
                        [progress removeFromSuperview];
                        
                       
                        
                        
                    });
                    
                }]resume];
                
                
            }
            

            
            cell.userInteractionEnabled =NO;
            
            
            NSString *getsalarystatus = [NSString stringWithFormat:@"%@",GET_SALARYSTATUS];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
            
            [request setURL:[NSURL URLWithString:getsalarystatus]];
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[MyPref valueForKey:@"api_key"] forHTTPHeaderField:@"Authorization-Basic"] ;
            
            
            //Setting body for the request
            NSDictionary *params = [[NSDictionary alloc]init ];
            
            NSDate *currentDate = [NSDate date];
            
            
            NSLog(@"current date of the device is :%@", currentDate);
            
            NSCalendar *hijiri = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            
            NSDateFormatter *format=[[NSDateFormatter alloc ] init];
            
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
            [format setLocale:locale];
            
            
            [format setDateStyle:NSDateFormatterLongStyle];
            [format setTimeStyle:NSDateFormatterNoStyle];
            [format setCalendar:hijiri];
            
            [format setDateFormat:@"yyyy-MM-dd HH:mm:ss" ];
            
            
            NSLog(@"date in english is :%@", [format stringFromDate:[NSDate date]]);
            
            
            
            NSString *date = [format stringFromDate:currentDate];
            
            NSLog(@"string from date is:%@",date);
            
            
            date= [date substringToIndex:7];
            
            NSLog(@"date with year and month%@", date);
            
            NSString *month = [date substringFromIndex:5];
            
            NSLog(@"month from [date is :%@",month);
            
            NSString *year = [date substringToIndex:4];
            
            NSLog(@"year from the date is :%@", year);
            
            params = [NSDictionary dictionaryWithObjectsAndKeys:[MyPref valueForKey:@"bank"],@"bank",[MyPref valueForKey:@"category"],@"category",month,@"month",year,@"year" ,nil];
            
        
            NSString *encodedBank = [self encodeToPercentEscapeString:[MyPref valueForKey:@"bankname"]];
            
            
            NSLog(@"encoded category is :%@", encodedBank);
            
            NSMutableData *body = [NSMutableData data];
            
//            NSString *string = [NSString strin]
            
            NSString *bodydata = [NSString stringWithFormat:@"bank=%@&category=%@&month=%@&year=%@",encodedBank,encodedCategory,month, year ];
            
            
            NSLog(@"body data for the user is for bank name n all:%@ and bankname in the shared preferences bank name is :%@",bodydata,[MyPref valueForKey:@"bankname"] );
            
            
            
            
            
            [body appendData:[bodydata dataUsingEncoding:NSASCIIStringEncoding]];
            
            
            
                        
            [request setHTTPBody:body];
          
            NSString *bodyofrequest = [[NSString alloc]initWithData:body encoding:NSUTF8StringEncoding];
            
            
            NSLog(@"request body is:%@", bodyofrequest);
            
            NSLog(@"actual request is :%@", [request allHTTPHeaderFields]);
            
            
            
            
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            
            
            
            //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
            
            [[BankList sharedManager]addView:nil :@"home"];
            dispatch_async(dispatch_get_main_queue(), ^{
                progress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
                progress.labelText = @"Processing..";
            });
            
            if ([self connectedToInternet]) {
            
            NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
                
                NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                
                
                
                NSLog(@"response from server is :%@ \nand data from the server is%@", response,
                      data_server);
                
                
                NSDictionary *response_ictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                
                
                NSLog(@"response for bank data server is ;%@",response_ictionary);
                
                NSArray *salaryStatus = [response_ictionary valueForKey:@"salary"];
                
                NSDictionary *actualresponse = [salaryStatus lastObject];
                
                //Setting bank color
                NSString *bankColor=[NSString stringWithFormat:@"%@", [actualresponse valueForKey:@"color"]];
                bankColor = [bankColor stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                
                  NSLog(@"bank color string is :%@", bankColor);
                
                [MyPref setValue:bankColor forKey:@"bankcolor"];
                [MyPref synchronize];
                
                
                
                NSString  *arabicBank;
                
                
                if ([actualresponse valueForKey:@"bank_keyword"] !=nil) {
                    
                
                
                 arabicBank = [NSString stringWithCString:[[actualresponse valueForKey:@"bank_keyword"] cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
                    }
                
                NSLog(@"arabic bank string is :%@", arabicBank);
                
                
                
                [MyPref setValue:arabicBank forKey:@"bank"];
                [MyPref synchronize];
                
                
                NSString  *arabicCategory;
                
                if ([actualresponse valueForKey:@"category_keyword"] !=nil) {
                    arabicCategory = [NSString stringWithCString:[[actualresponse valueForKey:@"category_keyword"] cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
                }
                
                
                [MyPref setValue:arabicCategory forKey:@"category"];
                [MyPref synchronize];
                
                NSLog(@"response for status from the server is :%@", response_ictionary);
                
                
                NSDictionary *salary = [response_ictionary valueForKey:@"salary"];
                
                 NSLog(@"value for salary credited or not is :%@", [salary valueForKey:@"is_salary_credited"]);
                NSArray *salaryState = [[NSArray alloc]init];
                salaryState = [salary valueForKey:@"is_salary_credited"];
                
                
                NSLog(@"value for salary credited or not is :%@", [salaryState lastObject]);
                
                
               int salary_credited = (int)[[salaryState lastObject]integerValue] ;
                
                
                NSLog(@"salary credit string is :%d", salary_credited);
                
                
                if (salary_credited == 1) {
                    
                    
                    NSLog(@"making salary state 1");
                    
                    [MyPref setValue: [NSString stringWithFormat:@"%d", salary_credited] forKey:@"salarystate"];
                    [MyPref synchronize];
                    
                    
                }
                else
                {
                    [MyPref setValue:@"0" forKey:@"salarystate"];
                    [MyPref synchronize];
                    
                    
                }

                
                
                
                
                
                
               
                
                
                for (UIView *view in [[[UIApplication sharedApplication] keyWindow].rootViewController.view subviews]) {
                    [view removeFromSuperview];
                    
                }
                [self removeFromSuperview];
                
                UIView *homeBackView = [[UIView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*10, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*90)];
                
                homeBackView.backgroundColor = [UIColor clearColor];
                
                [MyPref setValue:@"0" forKey:@"firstlogin"];
                [MyPref synchronize];
                
                
                
                Home *recruiters = [[Home alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*10, ([[UIScreen mainScreen] bounds].size.height/100)*20, ([[UIScreen mainScreen] bounds].size.width/100)*80, ([[UIScreen mainScreen] bounds].size.height/100)*70)];
                
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[BackButton sharedManager] goBack:recruiters :@"home"];
                    [[BankList sharedManager]addView:recruiters :@"home"];
                    recruiters.center = homeBackView.center;
                    
                    
                    
                    [recruiters salary_Status_View];
                    
                    progress.hidden = YES;
                    [progress removeFromSuperview];
                    hud.hidden = YES;
                    
                    
                    
                    [MyPref setValue:@"0" forKey:@"isNewUser"];
                    [MyPref synchronize];
                    
                });
                
                
            }]resume];

            banks_list = nil;
            [self setCatagory:@""];
            }
            else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    progress.hidden = YES;
                    [progress removeFromSuperview];
                    progress.hidden = YES;
                    [progress removeFromSuperview];
                    
                    
                    
                    
                });
                


                
                
            }
            
    
    
    }
    }
    else{
        
        [[BankList sharedManager]addView:nil :cell.textLabel.text];
        dispatch_async(dispatch_get_main_queue(), ^{
            progress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
            progress.labelText = @"Processing..";
        });
        

        
        
    for (NSDictionary *text in banks_list) {
        NSLog(@"text string is :%@", text);
        
        if ([[text valueForKey:@"name"] isEqualToString:[MyPref valueForKey:@"selectBank"]]) {
            [self setCatagory:@"recruiterss"];
            
            [MyPref setValue:cell.textLabel.text forKey:@"currentbankname"];
            [MyPref synchronize];
            
            
             UIColor *color = cell.backgroundView.backgroundColor;
            [MyPref setValue:[self heStringForColor:color] forKey:@"currentbankcolor"];
            [MyPref synchronize];
            
            
        }
        
    }
        
        NSLog(@"selected bank is :%@", [MyPref valueForKey:@"selectBank"]);
        
    
    if ([catagory isEqualToString:@"recruiterss"]) {
        
        NSLog(@"recruiters after selecting bank");
        
       
        
        
        
        
       NSDictionary *cell_elements = [banks_list objectAtIndex:indexPath.section];
        NSLog(@"cell elements in the bank selectedare :%@", cell_elements);
        
        NSLog(@"cell bank is :%@", cell.textLabel.text);
        
        
        
        
        NSString *icon_path = [cell_elements valueForKey:@"icon"];
        [MyPref setValue:icon_path forKey:@"bankicon"];
        [MyPref synchronize];
        
        
        NSString *getsalarystatus = [NSString stringWithFormat:@"%@",GET_SALARYSTATUS];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
        
        [request setURL:[NSURL URLWithString:getsalarystatus]];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[MyPref valueForKey:@"api_key"] forHTTPHeaderField:@"Authorization-Basic"] ;
        
        
        //Setting body for the request
        NSDictionary *params = [[NSDictionary alloc]init ];
        
        NSDate *currentDate = [NSDate date];
        
        
        NSLog(@"current date of the device is :%@", currentDate);
        
        NSCalendar *hijiri = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        NSDateFormatter *format=[[NSDateFormatter alloc ] init];
        
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
        [format setLocale:locale];
        
        
        [format setDateStyle:NSDateFormatterLongStyle];
        [format setTimeStyle:NSDateFormatterNoStyle];
        [format setCalendar:hijiri];
        
        [format setDateFormat:@"yyyy-MM-dd HH:mm:ss" ];
        
        
        NSLog(@"date in english is :%@", [format stringFromDate:[NSDate date]]);
        
        
        
        NSString *date = [format stringFromDate:currentDate];
        
        NSLog(@"string from date is:%@",date);
        
        
        date= [date substringToIndex:7];
        
        NSLog(@"date with year and month%@", date);
        
        NSString *month = [date substringFromIndex:5];
        
        NSLog(@"month from [date is :%@",month);
        
        NSString *year = [date substringToIndex:4];
        
        NSLog(@"year from the date is :%@", year);
        
        
        params = [NSDictionary dictionaryWithObjectsAndKeys:[MyPref valueForKey:@"bank"],@"bank",[MyPref valueForKey:@"category"],@"category",month,@"month",year,@"year" ,nil];
        
        
        
        
        NSMutableData *body = [NSMutableData data];
        
        
        NSString *bodydata = [NSString stringWithFormat:@"bank=%@&month=%@&year=%@",[MyPref valueForKey:@"selectBank"],month,year ];
        
        
        
        NSLog(@"bank name in the afte selecting bank api%@",bodydata );
        
        
        
        
        [body appendData:[bodydata dataUsingEncoding:NSASCIIStringEncoding]];
        
        
        
        
        [request setHTTPBody:body];
        
        NSString *bodyofrequest = [[NSString alloc]initWithData:body encoding:NSUTF8StringEncoding];
        
        
        NSLog(@"request body is of selection after bank:%@", bodyofrequest);
        
        NSLog(@"actual request is after bank selection:%@", [request allHTTPHeaderFields]);
        
        
        
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        
        
        //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
        
               if ([self connectedToInternet]) {
            
            
                
                
                
                
                
                
           
        
        
        NSString *icon_url = [NSString stringWithFormat:@"%@/%@",BASE_URL, icon_path ];
        
        
        NSLog(@"url to load the bank logo:%@", icon_url);
        
        
         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
             
             NSURL *icon = [NSURL URLWithString:icon_url];
             
             NSData *icondata = [NSData dataWithContentsOfURL:icon];
             
             local_icon_image_path = [NSString stringWithFormat:@"%@/cellimage", kDocumentDirectoryPath];
             
             
           
                 
                 
                 for (UIView *view in [[[UIApplication sharedApplication] keyWindow].rootViewController.view subviews]) {
                     [view removeFromSuperview];
                     
                 }
                 [self removeFromSuperview];
             
             NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
             
             [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
                 
                 NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                 
                 
                 
                 NSLog(@"response from server is after bank selection:%@ \nand data from the server isafter bank selection%@", response,
                       data_server);
                 
                 
                 response_ictionary_after_bank = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                 
                 
                 NSLog(@"response for status from the server is after bank selection:%@", response_ictionary_after_bank);
                 
                 
                 
                 
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     
                     progress.hidden = YES;
                     [progress removeFromSuperview];
                     progress.hidden = YES;
                     [progress removeFromSuperview];
                     
                     
                     
                     
                 });

                 
                 RecruiterList *recruit = [[RecruiterList alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*10, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*100) catagory:@"graduate" :response_ictionary_after_bank];
                 
                 
                 [MyPref setValue:@"1" forKey:@"monthof"];
                 [MyPref synchronize];
                 
//                 recruit.cates_array =graduate_array;
             
//                 recruit.afterbank_recruiter = response_ictionary_after_bank;
                 
                 
                 
             if (![[NSFileManager defaultManager] fileExistsAtPath:local_icon_image_path]) {
                 
                 NSLog(@"saving file locally");
                 [[NSFileManager defaultManager] createDirectoryAtPath:local_icon_image_path withIntermediateDirectories:NO attributes:nil error:nil];
                 local_icon_image_path = [local_icon_image_path stringByAppendingFormat:@"/%@", icon_path];
                 
                  if (![[NSFileManager defaultManager] fileExistsAtPath:local_icon_image_path]) {
                 [[NSFileManager defaultManager] createFileAtPath:local_icon_image_path contents:icondata attributes:nil];
                 
                      
                    
                      
                      
                 dispatch_async(dispatch_get_main_queue(), ^{
                 
                     [MyPref setValue:@"1" forKey:@"bankselect"];
                     [MyPref synchronize];
                     
                     
                     
                     
                     [[BackButton sharedManager] goBack:recruit :@"graduates"];
                     
                     NSLog(@"title to share with list is :%@", cell.textLabel.text);
                     
                     
                     [[BankList sharedManager]addView:recruit :cell.textLabel.text];
                     
//                     [tableView reloadData];

                     progress.hidden = YES;
                     [progress removeFromSuperview];
                     
                 
                 });
                  }
                 else
                 {
                     
                     
                     
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         [MyPref setValue:@"1" forKey:@"bankselect"];
                         [MyPref synchronize];
                         
                         NSLog(@"title to share with list is :%@", cell.textLabel.text);
                         [[BackButton sharedManager] goBack:recruit :@"graduates"];
                         [[BankList sharedManager]addView:recruit :cell.textLabel.text];
                         
                         progress.hidden = YES;
                         [progress removeFromSuperview];
                         
                         
                     });

                 }
                 
                 
             }
             else{
                 
                 local_icon_image_path = [local_icon_image_path stringByAppendingFormat:@"/%@", icon_path];
                 
                 if (![[NSFileManager defaultManager] fileExistsAtPath:local_icon_image_path]) {
                     [[NSFileManager defaultManager] createFileAtPath:local_icon_image_path contents:icondata attributes:nil];
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [MyPref setValue:@"1" forKey:@"bankselect"];
                         [MyPref synchronize];
                         
                         NSLog(@"title to share with list is :%@", cell.textLabel.text);
                         
                         [[BackButton sharedManager] goBack:recruit :@"graduates"];
                         [[BankList sharedManager]addView:recruit :cell.textLabel.text];
                         
                         progress.hidden = YES;
                         [progress removeFromSuperview];
                         
                         
                     });
                 }
                 else
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         [MyPref setValue:@"1" forKey:@"bankselect"];
                         [MyPref synchronize];
                         NSLog(@"title to share with list is :%@", cell.textLabel.text);
                         
                         [[BackButton sharedManager] goBack:recruit :@"graduates"];
                         [[BankList sharedManager]addView:recruit :cell.textLabel.text];
                         progress.hidden = YES;
                         [progress removeFromSuperview];
                         
                         
                         
                         
                     });
                     
                 }

                 
                 
                 
                 
                 
                 
                 
             }
             
             
             }]resume];
         
         });
                
            
            
            
            
            
            
            
            
            
            
            
            banks_list = nil;
            [self setCatagory:@""];
        }
               else{
                   dispatch_async(dispatch_get_main_queue(), ^{
                       
                       
                       progress.hidden = YES;
                       [progress removeFromSuperview];
                       progress.hidden = YES;
                       [progress removeFromSuperview];
                       
                       
                       
                       
                   });

               }
        
        }
        
        }
    
    
         
    NSDictionary *currentCateDict =  [cates_array objectAtIndex:indexPath.section];
    
    
    NSLog(@"current selected category is :%@",currentCateDict );
    
    [MyPref setValue:[currentCateDict valueForKey:@"category"] forKey:@"currCategory"];
    [MyPref synchronize];
    
    
  if ([catagory isEqualToString:@"recruiters"]) {
  
  [[BankList sharedManager]addView:nil :cell.textLabel.text];
      
      dispatch_async(dispatch_get_main_queue(), ^{
          progress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
          progress.labelText = @"Processing..";
      });
      
      
      NSString *getcate = [NSString stringWithFormat:@"%@",GET_RECRUITER];
      
              NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
      
              [request setURL:[NSURL URLWithString:getcate]];
              [request setHTTPMethod:@"POST"];
              [request setValue:@"aplication/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
      
       NSString *bodydata = [NSString stringWithFormat:@"category=%@",[self encodeToPercentEscapeString:[currentCateDict valueForKey:@"category"]]];
      
      
//      bodydata = [self encodeToPercentEscapeString:bodydata];
      
      
      NSLog(@"body data is :%@", bodydata);
      
      
      
       NSMutableData *bodyy = [NSMutableData data];
      
//      [bodyy appendData:[bodydata dataUsingEncoding:NSASCIIStringEncoding]];
//      
//      [request setHTTPBody:bodyy];
      
      
      NSString *bodyofrequestt = [[NSString alloc]initWithData:bodyy encoding:NSUTF8StringEncoding];
      
      
      
      
      NSLog(@"request body is of selection after bank:%@", bodyofrequestt);
      
      
      NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
      
      [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
          
          NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
          
          
          
          NSLog(@"response from server is after bank selection:%@ \nand data from the server isafter bank selection%@", response,
                data_server);
          
          
          response_ictionary_after_bank = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
          
          
          NSLog(@"response for status from the server is after bank selection:%@", response_ictionary_after_bank);
          
          dispatch_async(dispatch_get_main_queue(), ^{
          
          NSArray *cae_array = [response_ictionary_after_bank valueForKey:@"recruiter"];
          
          
          
          
          for (NSDictionary *filter in cae_array) {
//              NSLog(@"filter dict is :%@", filter);
//              NSLog(@"english ministery in it is :%@ and category to compare is :%@ and act is :%@",[filter valueForKey:@"ministry_eng"],[currentCateDict valueForKey:@"category"],[MyPref valueForKey:@"currCategory"] );
              
              
              if ([[filter valueForKey:@"ministry_eng"] isEqualToString:[MyPref valueForKey:@"currCategory"] ]) {
                  if ([act_cae_array count] == 0) {
                      
                      act_cae_array = [NSArray arrayWithObject:filter];
                      
                  }
                  else{
                      act_cae_array = [act_cae_array arrayByAddingObject:filter];
                      
                  }
                      
                  
              }
              
          }
          
          
              
              
              progress.hidden = YES;
              [progress removeFromSuperview];
              
              
              
              
          });
          
          
          
                  for (UIView *view in [[[UIApplication sharedApplication] keyWindow].rootViewController.view subviews]) {
                      [view removeFromSuperview];
          
                  }
                  [self removeFromSuperview];
                  ListView *recruiters = [[ListView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*2, ([[UIScreen mainScreen] bounds].size.height/100)*8, ([[UIScreen mainScreen] bounds].size.width/100)*88, ([[UIScreen mainScreen] bounds].size.height/100)*80) catagory:@"Otherrecruiters"];
                  recruiters.catagory = @"graduates";
                      recruiters.graduates_array =act_cae_array;
                        [recruiters setSearchArray];
                      dispatch_async(dispatch_get_main_queue(), ^{
                          
                          NSLog(@"category name is :%@", cell.textLabel.text);
                          
                          
                  [[BackButton sharedManager] goBack:recruiters :@"recruiters"];
                  [[BankList sharedManager]addView:recruiters :cell.textLabel.text];
                          progress.hidden = YES;
                          [progress removeFromSuperview];
                          
                      
                      });
      
      
      
        
      
      }]resume];
      
      
      
      
      
      
  
  }
    
    
//        NSString *getcate = [NSString stringWithFormat:@"%@",GET_CATE];
//
//        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
//
//        [request setURL:[NSURL URLWithString:getcate]];
//        [request setHTTPMethod:@"POST"];
//        [request setValue:@"aplication/json" forHTTPHeaderField:@"Content-Type"];
//        //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            progress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
//            progress.labelText = @"Processing..";
//        });
//        NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//        
//        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
//            
//            NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
//            
//            
//            
//            NSLog(@"response from server is :%@ \nand data from the server is%@", response,
//                  data_server);
//            
//            
//            cate_list = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
//            
//            
//                    NSLog(@"bank list from the server is :%@", cate_list);
//            
//            cate_array = [cate_list valueForKey:@"category"];
//            NSLog(@"catagory data in array %@", cate_array);
//
//        
//        for (UIView *view in [[[UIApplication sharedApplication] keyWindow].rootViewController.view subviews]) {
//            [view removeFromSuperview];
//            
//        }
//        [self removeFromSuperview];
//        ListView *recruiters = [[ListView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*2, ([[UIScreen mainScreen] bounds].size.height/100)*20, ([[UIScreen mainScreen] bounds].size.width/100)*95, ([[UIScreen mainScreen] bounds].size.height/100)*80) catagory:@"recruiters"];
//        recruiters.catagory = @"recruiters";
//            recruiters.cates_array =cate_array;
//            dispatch_async(dispatch_get_main_queue(), ^{
//        [[BackButton sharedManager] goBack:recruiters :@"recruiters"];
//        [[BankList sharedManager]addView:recruiters :@"CATAGORY"];
//                progress.hidden = YES;
//                [progress removeFromSuperview];
//                
//            
//            });
//            
//        
//        }]resume];
//        
//        
    
//
        
          
         

}


-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
//        NSLog(@"height for footer is called");
    
    return ([[UIScreen mainScreen] bounds].size.height/100)*0.7;
    
    
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    NSLog(@"setting color for the footer of the section in uitable view");
    
    
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    
    
    return view;
    
    
}



-(void) filterForSearchText:(NSString *)text scope:(NSString *)scope
{
    NSLog(@"filtering search for the text %@", text);
    if ([catagory isEqualToString:@"graduates"]) {
    
   
    
    NSPredicate *filteredPredicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", text];
    filteredResult = [NSMutableArray arrayWithArray:[graduateSearch filteredArrayUsingPredicate:filteredPredicate]];
         NSLog(@"filter result array is :%@ and filter predicate is :%@", filteredResult, filteredPredicate);
        filteredPredicate = nil;
        
        
    
        
    }
    else{
        
        NSPredicate *filteredPredicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", text];
        filteredResult = [NSMutableArray arrayWithArray:[graduateSearch filteredArrayUsingPredicate:filteredPredicate]];
        
        NSLog(@"filter result array is :%@ and filter predicate is :%@", filteredResult, filteredPredicate);
        filteredPredicate = nil;
        
        
        
        
    }
    
    
    NSLog(@"filter array count is :%d",(int) [filteredResult count]);
    
    
}






//Search bar delegate methods



-(BOOL) searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    NSLog(@"begin editing");
    self.frame = [UIScreen mainScreen].bounds;
    bankslist.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*2, ([[UIScreen mainScreen] bounds].size.height/100)*26, ([[UIScreen mainScreen] bounds].size.width/100)*95, ([[UIScreen mainScreen] bounds].size.height/100)*70);
    searchbar.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width/100)*-3.5, ([UIScreen mainScreen].bounds.size.height/100)*10, ([UIScreen mainScreen].bounds.size.width/100)*105, ([UIScreen mainScreen].bounds.size.height/100)*5);
    
    
    if (self == nil) {
        [searchbar resignFirstResponder];
        
    }
    
    
    return YES;
    
}


-(BOOL) searchBarShouldEndEditing:(UISearchBar *)searchBar{
    
    
    
    
    NSLog(@"ending editing");
    
    
    self.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*8, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*95);
    
    bankslist.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*2, ([[UIScreen mainScreen] bounds].size.height/100)*18, ([[UIScreen mainScreen] bounds].size.width/100)*95, ([[UIScreen mainScreen] bounds].size.height/100)*70);
    searchbar.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width/100)*-3.5, ([UIScreen mainScreen].bounds.size.height/100)*2, ([UIScreen mainScreen].bounds.size.width/100)*105, ([UIScreen mainScreen].bounds.size.height/100)*5);
    
    
     [searchbar resignFirstResponder];
    return YES;
    
}



-(NSString *)heStringForColor:(UIColor *)color
{
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    
    
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    
    NSString *hexString = [NSString stringWithFormat:@"%02X%02X%02X",(int)(r * 255),(int)(g * 255),(int)(b * 255)];
    
    
    return hexString;
    
    
    
}

-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSLog(@"text is changing in the dlegate of the search bar");
    
    
    progress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
    progress.labelText = @"Searching...";
    
    filteredResult = nil;
    
    
    filteredResult = [[NSMutableArray alloc]init];
    
    
    if (searchText.length == 0) {
        
        isFiltered =YES;
       
        
        
        [self filterForSearchText:searchText scope:@""];

        
        [bankslist reloadData];
        progress.hidden = YES;
        [progress removeFromSuperview];
        progress = nil;
        
    }
    else
    {
        NSLog(@"search bar has some fo the text in it");
        
        isFiltered = true;
        

        
        
        
        [self filterForSearchText:searchText scope:@""];
        
        
        
        [bankslist reloadData];
        
        
        progress.hidden = YES;
        [progress removeFromSuperview];
        progress = nil;
        
        
        
        
    }
    
    
    
     NSLog(@"search text is :%@", searchText);
    
    
    if ([searchText isEqualToString:@"\n"]) {
        
        NSLog(@"search text is :%@", searchText);
        
        
        
        [searchbar resignFirstResponder];
        return;
        
        
    }
 
    
    
    
    
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    [searchbar resignFirstResponder];
    
//    [searchCrasherror ]
    
}


-(void) searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchbar resignFirstResponder];
    
}

- (BOOL)connectedToInternet
{
    NSString *urlString = @"http://www.google.com/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    
    return ([response statusCode] == 200) ? YES : NO;
}



//Method to increase height of cell in uitableview
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([catagory isEqualToString:@"bank_salary"])
    {
        
       
    return ([[UIScreen mainScreen] bounds].size.height/100)*8;
    
}

        return ([[UIScreen mainScreen] bounds].size.height/100)*7;

}

// Encode a string to embed in an URL.
-(NSString *) encodeToPercentEscapeString:(NSString *)string {
   
    return  CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                            (CFStringRef) string,
                                            NULL,
                                            (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                            kCFStringEncodingUTF8));
}

-(void) dissmissKeyboard
{
    
//    [searchCrasherror removeFromSuperview];
    
    [searchbar resignFirstResponder];
    
   
    
}
-(void)dismiis
{
   
    NSLog(@"dissmiss called");
    
    
    
    [searchbar resignFirstResponder];
//     [searchCrasherror removeFromSuperview];
}


-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([searchbar isFirstResponder] && [touch view] != searchbar) {
        self.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*8, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*95);
        
         bankslist.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*2, ([[UIScreen mainScreen] bounds].size.height/100)*18, ([[UIScreen mainScreen] bounds].size.width/100)*95, ([[UIScreen mainScreen] bounds].size.height/100)*70);
       searchbar.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width/100)*-3.5, ([UIScreen mainScreen].bounds.size.height/100)*2, ([UIScreen mainScreen].bounds.size.width/100)*105, ([UIScreen mainScreen].bounds.size.height/100)*5);
        
        [searchbar resignFirstResponder];
        
    }
}



-(void) setSearchArray 
{
    NSLog(@"setting search array here");
    
    if ([catagory isEqualToString:@"recruiters"]) {
        
        for (int i=0; i<[cates_array count]; i++) {
            NSDictionary *cate_dict = [cates_array objectAtIndex:i];
             NSString  *arabicBank = [NSString stringWithCString:[[cate_dict valueForKey:@"keyword"] cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
            
            if ([graduateSearch count] == 0) {
                graduateSearch = nil;
                
                graduateSearch =[[NSMutableArray alloc]initWithObjects:arabicBank, nil];
                
                
                
            }
            else{
                [graduateSearch addObject:arabicBank];
                
            }
            
        }
        
        NSLog(@"search array is :%@", graduateSearch);
        
        
        
    }
    
    
    else if ([catagory isEqualToString:@"graduates"])
    {
         for (int i=0; i<[graduates_array count]; i++) {
        
          NSDictionary *graduate = [graduates_array objectAtIndex:i];
        
        if ([graduateSearch count] == 0) {
            graduateSearch =[[NSMutableArray alloc]initWithObjects:[graduate valueForKey:@"name"], nil];
            
            
            
        }
        else{
            [graduateSearch addObject:[graduate valueForKey:@"name"]];
            
        }
         
         }
    }
    
    
}



//-(void) searchBar:(UISearchBar *)searchBar textDidChange:(nonnull NSString *)searchText
//{
//    
//    
//    NSLog(@"search bar text changing according to the search text");
//    
//    
//    
//}

@end
