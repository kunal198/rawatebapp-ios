//
//  Home.m
//  Rwewtab
//
//  Created by Neuerung on 4/30/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import "Home.h"
#import "BankList.h"
#import "ListView.h"
#import "BackButton.h"
#import "UIColor+Additions.h"
#import "LogInView.h"
#define GET_SALARYSTATUS @"http://admin.elatikprojects.com/api/v1/get_SalaryStatus_new"

@implementation Home

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    
    if (self) {
        NSLog(@"we are in home method to define rect for home class");
        
        
        UIImageView *background = [[UIImageView alloc]init];
        
        background.frame = self.bounds;
        
        
        
        background.backgroundColor=[UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.5f];
        
//        background.alpha = 0.4f;
        
        [self addSubview:background];
        
        
       /* NSString *postRegisterUser = [NSString stringWithFormat:@"%@", GET_SALARYSTATUS];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
        
        [request setURL:[NSURL URLWithString:postRegisterUser]];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"aplication/json" forHTTPHeaderField:@"Content-Type"];
        //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
        
        
        NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
            
            NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            
            
            NSLog(@"response from server is :%@ \nand data from the server is%@", response,
                  data_server);
            
            
            salary_status = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
            
            //        NSLog(@"bank list from the server is :%@", bank_list);
            
            salary_array = [salary_status valueForKey:@"banks"];
            
            NSLog(@"banks array is :%@", salary_array);
            
            
            for (UIView *view in [[[UIApplication sharedApplication] keyWindow].rootViewController.view subviews]) {
                [view removeFromSuperview];
                
            }
            [self removeFromSuperview];
            
            
            ListView *bank_salary = [[ListView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*2, ([[UIScreen mainScreen] bounds].size.height/100)*20, ([[UIScreen mainScreen] bounds].size.width/100)*95, ([[UIScreen mainScreen] bounds].size.height/100)*80) catagory:@"bank_salary"];
            bank_salary.catagory = @"bank_salary";
            
            bank_salary.banks_list = salary_array;
            dispatch_async(dispatch_get_main_queue(), ^{
                [[BankList sharedManager]addView:bank_salary :@"BANK LIST"];
                
                [[BackButton sharedManager] goBack:bank_salary :@"bank_salary"];
                
            });
            
            
        }]resume]; */

    }
    
    
    return self;
    
}

-(void) salary_Status_View
{
    NSLog(@"design view for salary staus");
//     isapplogin = true;
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isapplogin"];
    
    UIImageView *submit= [[UIImageView alloc]initWithFrame:CGRectMake((self.bounds.size.width/100)*30, (self.bounds.size.height/100)*10, (self.bounds.size.width/100)*30, (self.bounds.size.width/100)*30)];
    submit.center = CGPointMake(self.bounds.size.width/2, (self.bounds.size.height/100)*25);
    
    UILabel *salarysta = [[UILabel alloc]initWithFrame:CGRectMake((self.bounds.size.width/100)*5, (self.bounds.size.height/100)*33, (self.bounds.size.width/100)*90, (self.bounds.size.height/100)*15)];
    
    
    UILabel *salarycat = [[UILabel alloc]initWithFrame:CGRectMake((self.bounds.size.width/100)*5, (self.bounds.size.height/100)*41, (self.bounds.size.width/100)*90, (self.bounds.size.height/100)*15)];
    
    
    UILabel *nameofmonth = [[UILabel alloc]initWithFrame:CGRectMake((self.bounds.size.width/100)*5, (self.bounds.size.height/100)*53, (self.bounds.size.width/100)*90, (self.bounds.size.height/100)*8)];
    
    
     UILabel *bankname = [[UILabel alloc]initWithFrame:CGRectMake((self.bounds.size.width/100)*15, (self.bounds.size.height/100)*73, (self.bounds.size.width/100)*60, (self.bounds.size.height/100)*10)];
    
    
     UILabel *monthin = [[UILabel alloc]initWithFrame:CGRectMake((self.frame.size.width/100)*20 ,(self.frame.size.height/100)*63, (self.frame.size.width/100)*80, (self.frame.size.height/100)*10)];
    
    
    CGRect frame = bankname.frame;
    frame.size.width = [[MyPref valueForKey:@"bank"] sizeWithFont:[UIFont fontWithName:@"Helvetica" size:([UIScreen mainScreen].bounds.size.height/100)*2]].width;
    
    frame.size.width =frame.size.width +([UIScreen mainScreen].bounds.size.width/100)*15;
    
    bankname.frame = frame;
    
    bankname.center = CGPointMake(self.bounds.size.width/2, (self.bounds.size.height/100)*77);
    
    
    
    [monthin setText:NSLocalizedString(@"In", nil)];
    monthin.center = CGPointMake((self.bounds.size.width/100)*50, (self.bounds.size.height/100)*64);
    
    [monthin setFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]];
    
    monthin.textAlignment = NSTextAlignmentCenter;
    
    monthin.textColor = [UIColor whiteColor];
    
    
    [self addSubview:monthin];
    
    
    submit.contentMode = UIViewContentModeScaleAspectFit;

    
    NSDate *currentDate = [NSDate date];
    
    
    NSLog(@"current date of the device is :%@", currentDate);
    
    NSDateFormatter *format=[[NSDateFormatter alloc ] init];
    
    [format setDateFormat:@"yyyy-MM-dd HH:mm:ss" ];
    
    
    NSString *date = [format stringFromDate:currentDate];
    
    NSLog(@"string from date is:%@",date);
    
    
    date= [date substringToIndex:7];
    
    NSLog(@"date with year and month%@", date);
    
    NSString *month = [date substringFromIndex:5];
    
    NSDateFormatter *monthformatter = [[NSDateFormatter alloc] init];
    
    [monthformatter setDateFormat:@"MM"];
    
    NSDate *monthdate = [monthformatter dateFromString:month];
    
    
    NSDateFormatter *monthname = [[NSDateFormatter alloc]init];
   
    //[monthname setDateStyle:NSDateFormatterLongStyle];
    
    [monthname setDateFormat:@"MMMM"];
    
    
    
    
    NSString *monthwithname = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"Month of", nil),NSLocalizedString([monthname stringFromDate:monthdate], nil)];
    
    NSLog(@"month name is in home page is:%@ and english month is%@ and month date to get month is :%@", monthwithname, [monthname stringFromDate:monthdate],monthdate );
    
    
    
    
    
    
    
    
    NSLog(@"month from [date is :%@",month);
    
    NSString *year = [date substringToIndex:4];
    
    NSLog(@"year from the date is :%@", year);

    
    
    bankname.textColor = [UIColor whiteColor];
    NSLog(@"value of salary credit is :%d", [[MyPref valueForKey:@"salarystate"] integerValue]);
    
    
    if([[MyPref valueForKey:@"salarystate"] isEqualToString:@"1"])
    {
       
    
        submit.image = [UIImage imageNamed:@"icon_diposited.png"];
        
        
        [self addSubview:submit];
        
        
        
        [salarysta setText:NSLocalizedString(@"Salary is credited", nil)];
        
        [self addSubview:salarysta];
        
        //        [salarysta setBackgroundColor:[UIColor whiteColor]];
        
        [salarysta setFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]];
        
        [salarysta setTextColor:[UIColor whiteColor]];
        
        
        salarysta.textAlignment = NSTextAlignmentCenter;
        
        
        
        
        salarycat.text =[MyPref valueForKey:@"category"];
        [self addSubview:salarycat];
        
        [salarycat setTextColor:[UIColor yellowColor]];
        
        [salarycat setText:[MyPref valueForKey:@"category"]];
        
        
        [salarycat setFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]];
        
        
        salarycat.textAlignment = NSTextAlignmentCenter;
        
        
        
        //        nameofmonth.text = monthwithname;
        [self addSubview:nameofmonth];
        
        [nameofmonth setTextColor:[UIColor whiteColor]];
        
        [nameofmonth setText:monthwithname];
        
        [nameofmonth setFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]];
        
        nameofmonth.textAlignment = NSTextAlignmentCenter;
        
        
        
        //        bankname.text = [MyPref valueForKey:@"bank"];
        
        //        bankname.backgroundColor = [UIColor blueColor];
        
        
        [self addSubview:bankname];
        
        
        [bankname setText:[MyPref valueForKey:@"bank"]];
        
        
        
        
        
        [bankname setBackgroundColor:[UIColor colorWithHexString:[MyPref valueForKey:@"bankcolor"]]];
        bankname.alpha = 0.7f;
        
        [bankname setFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]];
        
        
        bankname.textAlignment = NSTextAlignmentCenter;
        
    
    
    
    
    //    sharingview.layer.cornerRadius=5;  //To make it without c
        
        
   
    }

    
    else{
        
        
        submit.image = [UIImage imageNamed:@"icon_not_submit.png"];
        
        
        [self addSubview:submit];

        
        
        [salarysta setText:NSLocalizedString(@"Salary is not submitted", nil)];
        
        [self addSubview:salarysta];
        
//        [salarysta setBackgroundColor:[UIColor whiteColor]];
        
        [salarysta setFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]];
        
        [salarysta setTextColor:[UIColor whiteColor]];
        
        
        salarysta.textAlignment = NSTextAlignmentCenter;
        
        
        
        
        salarycat.text =[MyPref valueForKey:@"category"];
        [self addSubview:salarycat];
        
        [salarycat setTextColor:[UIColor yellowColor]];
        
        [salarycat setText:[MyPref valueForKey:@"category"]];
        
        
        [salarycat setFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]];
        
        
        salarycat.textAlignment = NSTextAlignmentCenter;
        
        salarycat.lineBreakMode=NSLineBreakByTruncatingTail;
        
        
        
        salarycat.numberOfLines=0;
        
//        [salarycat sizeToFit];
        
//        nameofmonth.text = monthwithname;
        [self addSubview:nameofmonth];
        
        [nameofmonth setTextColor:[UIColor whiteColor]];
        
        [nameofmonth setText:monthwithname];
        
        [nameofmonth setFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]];
        
        nameofmonth.textAlignment = NSTextAlignmentCenter;
        
        
        
//        bankname.text = [MyPref valueForKey:@"bank"];
        
//        bankname.backgroundColor = [UIColor blueColor];
        
        
        [self addSubview:bankname];
        
        
        [bankname setText:[MyPref valueForKey:@"bank"]];
        
        
       
       
        
        [bankname setBackgroundColor:[UIColor colorWithHexString:[MyPref valueForKey:@"bankcolor"]]];
        bankname.alpha = 0.7f;
        
        [bankname setFont:[UIFont fontWithName:@"Helvetica" size:textSizeLarge]];
  
    
    bankname.textAlignment = NSTextAlignmentCenter;
    
    }
    
    
    
//    sharingview.layer.cornerRadius=5;  //To make it without corners
    
   
    
    
    
    
    
    
    
    
}

@end
