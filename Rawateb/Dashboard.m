//
//  Dashboard.m
//  Rwewtab
//
//  Created by Rohit Mishra on 4/18/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import "Dashboard.h"
#import "ListView.h"
#import "BankList.h"
#import "LogInView.h"
#import "BackButton.h"
#import "AdvertisementView.h"
#import "Home.h"
#define BANK_REGISTER @"http://admin.elatikprojects.com/api/v1/get_banks"
#define GET_CATE @"http://admin.elatikprojects.com/api/v1/get_cate"
#define GET_GRADUATES @"http://admin.elatikprojects.com/api/v1/get_graduate"
#define GET_RECRUITER @"http://admin.elatikprojects.com/api/v1/get_recruiter"
#define GET_BANNER @"http://admin.elatikprojects.com/api/v1/get_banner"
#define GET_SALARYSTATUS @"http://admin.elatikprojects.com/api/v1/getSalaryStatus_new"





@implementation Dashboard
@synthesize bank_list, banks_array;


-(id) initWithFrame:(CGRect)frame
{
    
    self =[super initWithFrame:frame];
    
    if (self) {
        
        NSLog(@"dashboard appeared");
        
        self.backgroundColor =[UIColor blackColor];
        
        dashboardTable = [[UITableView alloc]initWithFrame:self.bounds];
        dashboardTable.separatorColor =[UIColor colorWithRed:1.0/255 green:18.0/255 blue:36.0/255 alpha:1.0f];
       
        dashboardTable.backgroundColor=[UIColor colorWithRed:1.0/255 green:18.0/255 blue:36.0/255 alpha:1.0f];
        dashboardTable.delegate=self;
        dashboardTable.dataSource=self;
        
        [dashboardTable setScrollEnabled:NO];
        
        
         [self addSubview:dashboardTable];
        
        //        [dashboardTable registerClass:[UITableView class] forCellReuseIdentifier:@"cell"];
        
        rowTitles = [[NSArray alloc]initWithObjects:NSLocalizedString(@"Home", nil),NSLocalizedString(@"Bank Salary", nil),NSLocalizedString(@"Recruiters", nil),NSLocalizedString(@"Graduates", nil),NSLocalizedString(@"Advertisement", nil),NSLocalizedString(@"Mail", nil),NSLocalizedString(@"Sign Out", nil), nil];
        imageArray =[ [NSArray alloc]initWithObjects:@"icon_home.png",@"icon_bank_salary",@"icon_recruiter.png",@"icon_graduate.png", @"icon_promotion.png",@"icon_mail.png",@"icon_exit.png",nil];
        
        
       
        
        
        
    }
    return self;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"table created");
    
    
    static NSString *cellidentifier = @"cell";
    
    
    UITableViewCell *cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    
    
    
    UIImageView *rightImageView;
   
 
    
    
    if (indexPath.row == [[MyPref valueForKey:@"selection"] integerValue]) {
        
        
        [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        
        
        UIView *selectionview = [[UIView alloc]init];
        
        selectionview.backgroundColor = [UIColor colorWithRed:255.0/255 green:192.0/255 blue:0.0/255 alpha:0.85f];
        
        
        cell.selectedBackgroundView = selectionview;

    }
    
    UILabel *cellText = [[UILabel alloc]initWithFrame:CGRectMake((self.bounds.size.width/100)*5 ,(cell.bounds.size.height/100)*7, (self.bounds.size.width/100)*70, (cell.bounds.size.height/100)*80)];

    
    UILabel *advertiseCount = [[UILabel alloc]initWithFrame:CGRectMake((self.bounds.size.width/100)*34 ,(cell.bounds.size.height/100)*35, (self.bounds.size.width/100)*6, (cell.bounds.size.height/100)*30)];
    
    
    if (cell) {
        NSLog(@"cell elements are");
        
        
     
        NSLog(@"index path of the row is :%d", (int)indexPath.row);
        
        advertiseCount.backgroundColor = [UIColor colorWithRed:255.0/255 green:192.0/255 blue:0.0/255 alpha:0.85f];
        
        
        advertiseCount.text = [MyPref valueForKey:@"promotionCount"];
        
        advertiseCount.textColor = [UIColor blackColor];
        
        advertiseCount.textAlignment = NSTextAlignmentCenter;
        
        advertiseCount.font =  [UIFont fontWithName:@"Helvetica" size:textSizeExtraSmall];
        
        advertiseCount.layer.cornerRadius = 0.8f;
        
        
        cellText.text = [rowTitles objectAtIndex:indexPath.row];
        cellText.textColor =[UIColor whiteColor];
        cellText.font = [UIFont fontWithName:@"Helvetica" size:textSizeMedium];
        
        
        cellText.textAlignment = NSTextAlignmentRight;
        
        
        rightImageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:[imageArray objectAtIndex:indexPath.row]]];
      
        rightImageView.frame = CGRectMake(0, 2, ([UIScreen mainScreen].bounds.size.width/100)*5, ([UIScreen mainScreen].bounds.size.height/100)*5);
        
        rightImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        cell.accessoryView = rightImageView;
        
        cell.accessoryView.contentMode = UIViewContentModeScaleAspectFit;
        
        cell.accessoryView.clipsToBounds = YES;
        
        
        
        
    }
    
    
    
    cell.backgroundColor = [UIColor colorWithRed:1.0/255 green:18.0/255 blue:36.0/255 alpha:1.0f];
    if (indexPath.row == 4) {
        [cell.contentView addSubview:advertiseCount];
    }
    
      [cell.contentView addSubview:cellText];

    return cell;
    
}


//Setting tableview row height
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return ([UIScreen mainScreen].bounds.size.height/100)*7;
}



//Action on row selection
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"index path row is :%d", (int)indexPath.row);
    
    [MyPref setValue:[NSString stringWithFormat:@"%d", (int)indexPath.row] forKey:@"selection"];
    [MyPref synchronize];
    
    
    [tableView reloadData];
    
    
    if (indexPath.row == 1) {
        
        [self bank_salary];
        
        
    }
   
    else if (indexPath.row == 2) {
        
        [self recruiters];
        
        
    }
    else if (indexPath.row == 3) {
        
        [self graduates];
        
        
    }
    else if (indexPath.row == 4) {
        
        [self advertisement];
        
        
    }
    else if (indexPath.row == 5) {
        
        [self mail];
        
        
    }
    else if (indexPath.row == 6) {
        
        [self signOut];
        
        
    }
    else if (indexPath.row == 0) {
        
        [self home];
        
        
    }
    
    
}


//Bank salary api

-(void)bank_salary
{
    NSLog(@"we are in bank salary");
   
    [MyPref setValue:@"0" forKey:@"internal_bank_recruiter_list"];
    [MyPref synchronize];
    NSString *postRegisterUser = [NSString stringWithFormat:@"%@", BANK_REGISTER];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    
    [request setURL:[NSURL URLWithString:postRegisterUser]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"aplication/json" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
    
    if ([MyPref valueForKey:@"bank_salary"] !=nil) {
        
        
        ListView *bank_salary = [[ListView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*2, ([[UIScreen mainScreen] bounds].size.height/100)*20, ([[UIScreen mainScreen] bounds].size.width/100)*95, ([[UIScreen mainScreen] bounds].size.height/100)*80) catagory:@"bank_salary"];
        bank_salary.catagory = @"bank_salary";
        
      
        
        NSLog(@"shared preferences bank salary is :%@", [MyPref valueForKey:@"bank_salary"]);
        
        
        
        bank_salary.banks_list = [MyPref valueForKey:@"bank_salary"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[BankList sharedManager]addView:bank_salary :@"BANK LIST"];
            
            [[BackButton sharedManager] goBack:bank_salary :@"bank_salary"];
            
            progress.hidden = YES;
            
            [progress removeFromSuperview];
            
            progress = nil;
        });

        
    }
    else{
        [[BankList sharedManager]addView:nil :@"BANK LIST"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        progress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
        progress.labelText = @"Processing..";
    });
    
        if ([self connectedToInternet]) {
            
       
    
    NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
        
        NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        
        
        NSLog(@"response from server is :%@ \nand data from the server is%@", response,
              data_server);
        
        
        bank_list = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
        
//        NSLog(@"bank list from the server is :%@", bank_list);
        
        banks_array = [bank_list valueForKey:@"banks"];
        
        NSLog(@"banks array is :%@", banks_array);
        
        
        for (UIView *view in [[[UIApplication sharedApplication] keyWindow].rootViewController.view subviews]) {
            [view removeFromSuperview];
            
        }
         [self removeFromSuperview];
        
        
        ListView *bank_salary = [[ListView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*2, ([[UIScreen mainScreen] bounds].size.height/100)*20, ([[UIScreen mainScreen] bounds].size.width/100)*95, ([[UIScreen mainScreen] bounds].size.height/100)*80) catagory:@"bank_salary"];
        bank_salary.catagory = @"bank_salary";
        
        [MyPref setValue:banks_array forKey:@"bank_salary"];
        [MyPref synchronize];
        
        NSLog(@"shared preferences bank salary is :%@", [MyPref valueForKey:@"bank_salary"]);
        
        
        
        bank_salary.banks_list = banks_array;
        dispatch_async(dispatch_get_main_queue(), ^{
        [[BankList sharedManager]addView:bank_salary :@"BANK LIST"];
            
            [[BackButton sharedManager] goBack:bank_salary :@"bank_salary"];
        
            progress.hidden = YES;
            
            [progress removeFromSuperview];
            
            progress = nil;
        });
        
        
    }]resume];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You are not connected to internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }
    
    
    
}




-(void) recruiters
{
    [MyPref setValue:@"0" forKey:@"internal_bank_recruiter_list"];
    [MyPref synchronize];
    
    NSLog(@"recruiters");
    
    
    if ([MyPref valueForKey:@"get_category"]!=nil) {
        
        
        NSLog(@"category array in shared preferences is :%@", [MyPref valueForKey:@"get_category"]);
        
        
        ListView *recruiters = [[ListView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*2, ([[UIScreen mainScreen] bounds].size.height/100)*20, ([[UIScreen mainScreen] bounds].size.width/100)*95, ([[UIScreen mainScreen] bounds].size.height/100)*80) catagory:@"recruiters"];
        recruiters.catagory = @"recruiters";
        recruiters.cates_array =[MyPref valueForKey:@"get_category"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[BackButton sharedManager] goBack:recruiters :@"recruiters"];
            [[BankList sharedManager]addView:recruiters :@"CATAGORY"];
            
            
           
            
        });

        
        
    }
    else{
    NSString *getcate = [NSString stringWithFormat:@"%@",GET_CATE];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    
    [request setURL:[NSURL URLWithString:getcate]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"aplication/json" forHTTPHeaderField:@"Content-Type"];
    //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
    
    [[BankList sharedManager]addView:nil :@"CATAGORY"];
    dispatch_async(dispatch_get_main_queue(), ^{
        progress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
        progress.labelText = @"Processing..";
    });
    
        if ([self connectedToInternet]) {
            
        
        
    NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
        
        NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        
        
        NSLog(@"response from server is :%@ \nand data from the server is%@", response,
              data_server);
        
        
        recruiter_list = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
        
        NSLog(@"bank list from the server is :%@", recruiter_list);
        
        recruiter_array = [recruiter_list valueForKey:@"category"];
        
        [MyPref setValue:recruiter_array forKey:@"get_category"];
        [MyPref synchronize];
        
        
        NSLog(@"catagory data in array %@", recruiter_array);
        
        
        for (UIView *view in [[[UIApplication sharedApplication] keyWindow].rootViewController.view subviews]) {
            [view removeFromSuperview];
            
        }
        [self removeFromSuperview];
        ListView *recruiters = [[ListView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*2, ([[UIScreen mainScreen] bounds].size.height/100)*20, ([[UIScreen mainScreen] bounds].size.width/100)*95, ([[UIScreen mainScreen] bounds].size.height/100)*80) catagory:@"recruiters"];
        recruiters.catagory = @"recruiters";
        recruiters.cates_array =recruiter_array;
        [recruiters setSearchArray];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[BackButton sharedManager] goBack:recruiters :@"recruiters"];
            [[BankList sharedManager]addView:recruiters :@"CATAGORY"];
            
            
            progress.hidden = YES;
            [progress removeFromSuperview];
            progress = nil;
            
        });
        
        
    }]resume];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You are not connected to internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    
    }

    
}


-(void) graduates
{
    NSLog(@"graduates called");
    
    [MyPref setValue:@"0" forKey:@"internal_bank_recruiter_list"];
    [MyPref synchronize];
    
    NSString *getcate = [NSString stringWithFormat:@"%@",GET_GRADUATES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    
    [request setURL:[NSURL URLWithString:getcate]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"aplication/json" forHTTPHeaderField:@"Content-Type"];
    //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
    [[BankList sharedManager]addView:nil :@"NEW GRADUATE LIST"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        progress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
        progress.labelText = @"Processing..";
    });
    
    
    if([self connectedToInternet])
    {
    NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
        
        NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        
        
        NSLog(@"response from server is :%@ \nand data from the server is%@", response,
              data_server);
        
        
        graduate_list = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
        
        NSLog(@"bank list from the server is :%@", graduate_list);
        
        graduate_array = [graduate_list valueForKey:@"graduate"];
        NSLog(@"catagory data in array %@", graduate_array);
        
        
        for (UIView *view in [[[UIApplication sharedApplication] keyWindow].rootViewController.view subviews]) {
            [view removeFromSuperview];
            
        }
        [self removeFromSuperview];
        ListView *recruiters = [[ListView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*10, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*100) catagory:@"graduates"];
        
//        CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*10, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*5)
        
        
        recruiters.catagory = @"graduates";
        recruiters.graduates_array =graduate_array;
        [recruiters setSearchArray];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            [[BackButton sharedManager] goBack:recruiters :@"graduates"];
            [[BankList sharedManager]addView:recruiters :@"NEW GRADUATE LIST"];
            
            
            progress.hidden = YES;
            
            [progress removeFromSuperview];
            
            progress = nil;
           
                 
            
            
        });
        
        
    }]resume];

    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You are not connected to internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
   
}



-(void) advertisement
{
    NSLog(@"advertisement called");
    [MyPref setValue:@"0" forKey:@"internal_bank_recruiter_list"];
    [MyPref synchronize];
    
    
    [MyPref setValue:@"0" forKey:@"start"];
    [MyPref synchronize];
    
    NSString *getbanner = [NSString stringWithFormat:@"%@",GET_BANNER];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    
    [request setURL:[NSURL URLWithString:getbanner]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"aplication/json" forHTTPHeaderField:@"Content-Type"];
    //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
    
    [[BankList sharedManager]addView:nil :@""];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        progress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
        progress.labelText = @"Processing..";
    });
    if([self connectedToInternet])
    {
    
    NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
        
        NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        
        
        NSLog(@"response from server is :%@ \nand data from the server is%@", response,
              data_server);
        
        
        advertise_list = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
        
        NSLog(@"banner list from the server is :%@", advertise_list);
        
        NSMutableArray  *adverstise_arrayy = [advertise_list valueForKey:@"banner"];
        
        NSLog(@"catagory data in array %@", adverstise_arrayy);
        
        
        for (UIView *view in [[[UIApplication sharedApplication] keyWindow].rootViewController.view subviews]) {
            [view removeFromSuperview];
            
        }
        //        [self removeFromSuperview];
        
        
        
        
        
        [self setBackgroundColor:[UIColor whiteColor]];
        
        [self removeFromSuperview];
        
         UIView *homeBackView = [[UIView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*10, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*90)];
        
        AdvertisementView *advertise = [[AdvertisementView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0,([[UIScreen mainScreen] bounds].size.height/100)*10 , ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*90) :@"promotion" add_arrray:adverstise_arrayy ];
       
        
       
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[BackButton sharedManager] goBack:advertise :@"advertisement"];
            [[BankList sharedManager]addView:advertise :@""];
            
            [advertise slidingImageView];
             advertise.center = homeBackView.center;
            
            
            progress.hidden = YES;
            [progress removeFromSuperview];
            progress = nil;
            
        });
        
        
    }]resume];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You are not connected to internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    
   
}



-(void) mail
{
    NSLog(@"mail called");
    [self removeFromSuperview];
    
    [MyPref setValue:@"0" forKey:@"internal_bank_recruiter_list"];
    [MyPref synchronize];
    
    NSArray *reciepents=[NSArray arrayWithObjects:@"rawateb.app@gmail.com", nil];
    mailcontroller=[[MFMailComposeViewController alloc]init];
    
   
    [mailcontroller setToRecipients:reciepents];
    
    
    
    NSLog(@"reciepents of the mail are %@", mailcontroller);
    
    
    [mailcontroller setSubject:@""];
//    NSLog(@"subject of the email is :%@", element3.subject);
//    NSLog(@"the element title in the eamil is :%@", element3.Title);
    
    
    
   
    
    mailcontroller.mailComposeDelegate=self;
   
   UIViewController *mainview= [[[UIApplication sharedApplication]keyWindow ]rootViewController];
    
    [mainview presentViewController:mailcontroller animated:YES completion:nil];
}



-(void) signOut
{
    NSLog(@"signOut called");
    for (UIView *view in [[[UIApplication sharedApplication] keyWindow].rootViewController.view subviews]) {
        [view removeFromSuperview];
        
    }
    [self removeFromSuperview];
    
    LogInView *login=[[LogInView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    
     [LogInView addView:login];
    
    [MyPref setValue:@"0" forKey:@"login"];
    [MyPref synchronize];
    [MyPref setValue:@"0" forKey:@"internal_bank_recruiter_list"];
    [MyPref synchronize];
    
}


-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSLog(@"we are in finishing result of email");
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [mailcontroller dismissViewControllerAnimated:YES completion:NULL];
}


-(void) home
{
    
    [MyPref setInteger:0 forKey:@"banner_enable"];
    [MyPref synchronize];
    
    
    
    
    [MyPref setInteger:0 forKey:@"banner"];
    [MyPref synchronize];
    
    [MyPref setValue:@"0" forKey:@"internal_bank_recruiter_list"];
    [MyPref synchronize];
    NSLog(@"we are in home method");
    NSString *getsalarystatus = [NSString stringWithFormat:@"%@",GET_SALARYSTATUS];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    
    [request setURL:[NSURL URLWithString:getsalarystatus]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[MyPref valueForKey:@"api_key"] forHTTPHeaderField:@"Authorization-Basic"] ;
    
    
    //Setting body for the request
    NSDictionary *params = [[NSDictionary alloc]init ];
    
    
    NSDate *currentDate = [NSDate date];
    
    
    NSLog(@"current date of the device is :%@", currentDate);
    
    
    NSCalendar *hijiri = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateFormatter *format=[[NSDateFormatter alloc ] init];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
    [format setLocale:locale];
    
    
    [format setDateStyle:NSDateFormatterLongStyle];
    [format setTimeStyle:NSDateFormatterNoStyle];
    [format setCalendar:hijiri];
    
    [format setDateFormat:@"yyyy-MM-dd HH:mm:ss" ];
    
    
    NSLog(@"date in english is :%@", [format stringFromDate:[NSDate date]]);
    
    
    
    NSString *date = [format stringFromDate:currentDate];
    
    NSLog(@"string from date is:%@",date);
    
    
    date= [date substringToIndex:7];
    
    NSLog(@"date with year and month%@", date);
    
    NSString *month = [date substringFromIndex:5];
    
    NSLog(@"month from [date is :%@",month);
    
    NSString *year = [date substringToIndex:4];
    
    NSLog(@"year from the date is :%@", year);

    
    
    params = [NSDictionary dictionaryWithObjectsAndKeys:[MyPref valueForKey:@"bank"],@"bank",[MyPref valueForKey:@"category"],@"category",month,@"month",year,@"year" ,nil];
    
    
    
    
    NSMutableData *body = [NSMutableData data];
    
    NSLog(@"full string of hindi category is ;%@",[MyPref valueForKey:@"category_hindi"]  );
    
    
    
//    NSRange range = [[MyPref valueForKey:@"category_hindi"] rangeOfString:@"&"];
    
    
    NSString *encodedCategory;
    
//    if (range.length > 0) {
//        NSLog(@"cate gory has empersand");
         encodedCategory = [self encodeToPercentEscapeString:[MyPref valueForKey:@"category_hindi"]];
        
//    }
//    else
//    {
//        NSLog(@"do not have any kind of emp");
//         encodedCategory = [MyPref valueForKey:@"category_hindi"];
//    }
    
    
    
    
    NSLog(@"encoded category is :%@", encodedCategory);
    
    
    NSString *encodedBank = [self encodeToPercentEscapeString:[MyPref valueForKey:@"bankname"]];
    
    
    NSLog(@"encoded category is :%@", encodedBank);
    
    

    
    
    NSString *bodydata = [NSString stringWithFormat:@"bank=%@&category=%@&month=%@&year=%@",encodedBank,encodedCategory,month, year ];
    
    
    NSLog(@"request parameters for new get salary status are:%@", bodydata);
    
    
    
    
    
    [body appendData:[bodydata dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    
    
    [request setHTTPBody:body];
    
    NSString *bodyofrequest = [[NSString alloc]initWithData:body encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"request body of get salary status is is:%@", bodyofrequest);
    
    NSLog(@"actual request is :%@", [request allHTTPHeaderFields]);
    
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    
    //    [request setValue:[NSString stringWithFormat:@"%@", email.text] forHTTPHeaderField:@"name"];
            [[BankList sharedManager]addView:nil :@"home"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        progress = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow].rootViewController.view animated:YES];
        progress.labelText = @"Processing..";
    });
    
    if ([self connectedToInternet]) {
        
    
    
    NSURLSession *session= [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *err){
        
        NSString *data_server = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        
        
        NSLog(@"response from server is :%@ \nand data from the server is%@", response,
              data_server);
        
        
        NSDictionary *response_ictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
        
        NSLog(@"response for status from the server is :%@", response_ictionary);
        
        NSArray *salaryStatus = [response_ictionary valueForKey:@"salary"];
        
        NSDictionary *actualresponse = [salaryStatus lastObject];
        
        //Setting bank color
        NSString *bankColor=[NSString stringWithFormat:@"%@", [actualresponse valueForKey:@"color"]];
        bankColor = [bankColor stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        
        NSLog(@"bank color string is :%@", bankColor);
        
                             
        
        [MyPref setValue:bankColor forKey:@"bankcolor"];
        [MyPref synchronize];
        
        
        
        NSString  *arabicBank;
        
        
        
        
        if ([actualresponse valueForKey:@"bank_keyword"] !=nil) {
        arabicBank = [NSString stringWithCString:[[actualresponse valueForKey:@"bank_keyword"] cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
        }
        NSLog(@"arabic bank string is :%@", arabicBank);
        
        [MyPref setValue:arabicBank forKey:@"bank"];
        [MyPref synchronize];
        
        
        
        
         NSString  *arabicCategory;
        
        if ([actualresponse valueForKey:@"category_keyword"] !=nil) {
            arabicCategory = [NSString stringWithCString:[[actualresponse valueForKey:@"category_keyword"] cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
        }
        
        
        [MyPref setValue:arabicCategory forKey:@"category"];
        [MyPref synchronize];

        
        
        NSDictionary *salary = [response_ictionary valueForKey:@"salary"];
        
        NSArray *salaryState = [[NSArray alloc]init];
        salaryState = [salary valueForKey:@"is_salary_credited"];
        
        
        NSLog(@"value for salary credited or not is :%@", [salaryState lastObject]);
        
        int salary_credited = (int)[[salaryState lastObject] integerValue];
        
        
        NSLog(@"salary credit string is :%d", salary_credited);
        
        
        if (salary_credited == 1) {
            
            
            NSLog(@"making salary state 1");
            
            [MyPref setValue: [NSString stringWithFormat:@"%d", salary_credited] forKey:@"salarystate"];
            [MyPref synchronize];
            
            
        }
        else
        {
            [MyPref setValue:@"0" forKey:@"salarystate"];
            [MyPref synchronize];
            
            
        }
            
        
        
        
        
        
        
        
        
        
        
        for (UIView *view in [[[UIApplication sharedApplication] keyWindow].rootViewController.view subviews]) {
            [view removeFromSuperview];
            
        }
        [self removeFromSuperview];
        
        UIView *homeBackView = [[UIView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*0, ([[UIScreen mainScreen] bounds].size.height/100)*10, ([[UIScreen mainScreen] bounds].size.width/100)*100, ([[UIScreen mainScreen] bounds].size.height/100)*90)];
        
        homeBackView.backgroundColor = [UIColor clearColor];
        
        Home *recruiters = [[Home alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*10, (homeBackView.bounds.size.height/100)*5, ([[UIScreen mainScreen] bounds].size.width/100)*80, (homeBackView.bounds.size.height/100)*90)];
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[BackButton sharedManager] goBack:recruiters :@"home"];
            [[BankList sharedManager]addView:recruiters :@"home"];
            
            recruiters.center = homeBackView.center;
            
            
            [recruiters salary_Status_View];
            
            progress.hidden = YES;
            [progress removeFromSuperview];
            
            progress = nil;
            
            
        });
        
        
    }]resume];
    
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You are not connected to internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

// Encode a string to embed in an URL.
-(NSString *) encodeToPercentEscapeString:(NSString *)string {
    
    return  CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                      (CFStringRef) string,
                                                                      NULL,
                                                                      (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                                      kCFStringEncodingUTF8));
}


- (BOOL)connectedToInternet
{
    NSString *urlString = @"http://www.google.com/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    
    return ([response statusCode] == 200) ? YES : NO;
}



@end
