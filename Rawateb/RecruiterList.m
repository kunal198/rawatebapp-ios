//
//  RecruiterList.m
//  Rawateb
//
//  Created by Neuerung on 5/19/16.
//  Copyright (c) 2016 Rohit Mishra. All rights reserved.
//

#import "RecruiterList.h"
#import "SharingImageView.h"

@implementation RecruiterList
@synthesize banks_list, cates_array, cellarray,afterbank_recruiter, share;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id) initWithFrame:(CGRect)frame catagory:(NSString *)cataagory :(NSDictionary *)afterbank
{
    self =[super initWithFrame:frame];
    if(self)
    {
        
        
        if (![[MyPref valueForKey:@"selection"] isEqualToString:@"0"]) {
            
            
          
                
                NSLog(@"having catagory graduates");
                
                
                bankslist = [[UITableView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width/100)*2, ([[UIScreen mainScreen] bounds].size.height/100)*20, ([[UIScreen mainScreen] bounds].size.width/100)*95, ([[UIScreen mainScreen] bounds].size.height/100)*70)];
                
                
//                searchbar =   [[UISearchBar alloc]initWithFrame:CGRectMake((self.bounds.size.width/100)*0, (self.bounds.size.height/100)*0, (self.bounds.size.width/100)*100, (self.bounds.size.height/100)*5)];
//                
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    
//                    searchbar.barStyle = UISearchBarStyleProminent;
//                    searchbar.searchBarStyle = UISearchBarIconSearch;
//                    
//                    searchbar.barTintColor = [UIColor clearColor];
//                    
//                    [searchbar isTranslucent];
//                    searchbar.delegate = self;
//                    
//                    
//                    [self addSubview:searchbar];
//                });
            
                
                
           
            [self addSubview:bankslist];
        }
        self.backgroundColor = [UIColor clearColor];
        
        bankslist.backgroundColor = [UIColor clearColor];
        
        bankslist.delegate=self;
        
        bankslist.dataSource = self;
        bankslist.allowsSelection = NO;
        
        
        cates_array = [afterbank valueForKey:@"salary"];
        
        NSLog(@"cates array in the recruiter list is :%@", cates_array);
       
        
        
        
        searchbar.delegate = self;
        
    }
    return self;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
//    if ([filteredResult count] !=0) {
//        return [filteredResult count];
//        
//    }
//    else{
//        
//        return [cates_array count];
//    }
    
    return 1;
    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([filteredResult count] !=0) {
        return [filteredResult count];
        
    }
    else{
        
        return [cates_array count];
    }

//    return 1;
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier = @"cell";
    
    
    
    [MyPref setValue:@"0" forKey:@"bankselect"];
    [MyPref synchronize];
    
    UITableViewCell *cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    
    
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [tableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    
    cell.backgroundColor = [UIColor clearColor];
    
    
    share = [[UIImageView alloc]initWithFrame:CGRectMake((cell.bounds.size.width/100)*2.5, (cell.bounds.size.height/100)*23, (cell.bounds.size.width/100)*7, (cell.bounds.size.height/100)*70)];
    
    
    share.contentMode = UIViewContentModeScaleAspectFit;
    
    share.tag = indexPath.section;
    
    
    share.userInteractionEnabled = YES;
    
    
    cell.contentView.userInteractionEnabled = YES;
    
    
    sharing_recognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sharing:)];
    
    
    [share addGestureRecognizer:sharing_recognizer];
    
    
    
    
     UIImageView *salary_state = [[UIImageView alloc]initWithFrame:CGRectMake((cell.bounds.size.width/100)*12, (cell.bounds.size.height/100)*25, (cell.bounds.size.width/100)*7, (cell.bounds.size.height/100)*70)];
    
    
    
    
    
    salary_state.contentMode = UIViewContentModeScaleAspectFit;
    
    
    
UILabel *date = [[UILabel alloc]initWithFrame:CGRectMake((cell.bounds.size.width/100)*2.5 ,(cell.bounds.size.height/100)*4.5, (cell.bounds.size.width/100)*30, (cell.bounds.size.height/100)*18)];
    
    date.textColor = [UIColor colorWithRed:255.0/255 green:192.0/255 blue:0.0/255 alpha:0.85f];
    
    //        salarysta.frame = frame;
    
    [date setFont:[UIFont fontWithName:@"Helvetica" size:([[UIScreen mainScreen] bounds].size.height/100)*1.1]];
    
    date.textAlignment = NSTextAlignmentLeft;
//    [date setBackgroundColor:[UIColor blackColor]];
    
//    NSLog(@"height of the screen is ")
    
    
    
    UIImageView *bank_icon = [[UIImageView alloc]initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width/100)*100-([UIScreen mainScreen].bounds.size.width/100)*18, 0, (cell.bounds.size.width/100)*14, cell.bounds.size.height+(cell.bounds.size.height/100)*30)];
    
    bank_icon.contentMode = UIViewContentModeScaleAspectFit;
    
    
    share.image = [UIImage imageNamed:@"icon_share.png"];
    
    
    share.contentMode = UIViewContentModeScaleAspectFit;
    
    NSDictionary *cate_dict = [cates_array objectAtIndex:indexPath.section];
    
    NSString *slaarycredit = [NSString stringWithFormat:@"%@",[cate_dict valueForKey:@"is_salary_credited"] ];
    
    NSLog(@"string from the dictionary is :%@", slaarycredit);
    
    
    
    
    if([slaarycredit isEqualToString:@"1"])
    {
        
        
        salary_state.image = [UIImage imageNamed:@"icon_diposited.png"];
        
        
        
        [date setText:[cate_dict valueForKey:@"salary_date"]];
        
         NSLog(@"date from after bank dictionary :%@", [cate_dict valueForKey:@"salary_date"]);
        
        
       
        
        
    }
    else{
        
        
        
        salary_state.image = [UIImage imageNamed:@"icon_not_submit.png"];
        
    }
    
    NSString *local_icon_image_path = [NSString stringWithFormat:@"%@/cellimage/%@", kDocumentDirectoryPath,[MyPref valueForKey:@"bankicon"]];
    bank_icon.image = [UIImage imageWithContentsOfFile:local_icon_image_path];
    
    
   
    
    

    UILabel *cellText = [[UILabel alloc]initWithFrame:CGRectMake((cell.bounds.size.width/100)*20 ,(cell.bounds.size.height/100)*2, ([UIScreen mainScreen].bounds.size.width/100)*60, (cell.bounds.size.height/100)*100)];
    
    cellText.textAlignment = NSTextAlignmentCenter;
    cellText.lineBreakMode=NSLineBreakByTruncatingTail;
    
    
    
    cellText.numberOfLines=0;
    
    
    if ([filteredResult count] !=0) {
        
        NSString  *arabicBank;
        if([filteredResult objectAtIndex:indexPath.section] != NULL)
        {
        
         arabicBank = [NSString stringWithCString:[[filteredResult objectAtIndex:indexPath.section] cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
        }
        
        NSLog(@"arabic bank string is :%@", arabicBank);
        
        
        
        cellText.textColor =  [UIColor whiteColor];
        cellText.textAlignment = NSTextAlignmentRight;
       
        
        
        UIView *backgroundView = [[UIView alloc]init];
        backgroundView.backgroundColor = [UIColor blackColor];
        backgroundView.alpha = 0.4f;
        
        cell.backgroundView = backgroundView;
        
        
        cellText.font = [UIFont fontWithName:@"Helvetica" size:textSizeLarge];
        tableView.separatorColor =  [UIColor clearColor];
        
        cellText.text  = arabicBank;
//        cell.imageView.image = [UIImage imageWithContentsOfFile:local_icon_image_path];
        
        
    }
    else{
    
    
        
        
    NSDictionary *cate_dict = [cates_array objectAtIndex:indexPath.section];
    
       
        
        if ([cellarray count] == 0) {
            cellarray =[[NSMutableArray alloc]initWithObjects:[cate_dict valueForKey:@"category"], nil];
            
            
            
        }
        else{
            [cellarray addObject:[cate_dict valueForKey:@"category"]];
            
        }
        
        NSLog(@"category keyword of crash is :%@", [cate_dict valueForKey:@"category_keyword"]);
        NSString  *arabicBank ;
        @try {
            
            
                 if([cate_dict valueForKey:@"category_keyword"] != nil && ![[cate_dict valueForKey:@"category_keyword"] isEqualToString:@"null"])
                 {
          arabicBank = [NSString stringWithCString:[[cate_dict valueForKey:@"category_keyword"] cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
        
                 }
        else
        {
            NSLog(@"string is zero and not crashing");
            
        }
        
    } @catch (NSException *exception) {
        NSLog(@"exception occured because of null string %@", exception);
        
    } @finally {
        NSLog(@"finallly executed");
        
    }

    
        cellText.text =arabicBank;
        
   cellText.textColor =  [UIColor whiteColor];
        cellText.textAlignment = NSTextAlignmentRight;
//        [cellText sizeToFit];
        
        
        
    if ([tableData count]  == 0) {
        tableData=[[NSMutableArray alloc]init];
        
        
        [tableData addObject:[cate_dict valueForKey:@"category"]];
        
    }
    else{
        
        [tableData addObject:[cate_dict valueForKey:@"category"]];
        
    }
    
        UIView *backgroundView = [[UIView alloc]init];
        backgroundView.backgroundColor = [UIColor blackColor];
        backgroundView.alpha = 0.4f;
        
        cell.backgroundView = backgroundView;
    
    
    cellText.font = [UIFont fontWithName:@"Helvetica" size:textSizeLarge];
    tableView.separatorColor =  [UIColor clearColor];
        
        
//        cell.imageView.image = [UIImage imageWithContentsOfFile:local_icon_image_path];
        
    }
    
    
    //removing all views from the ceelll content view
    
    for (UIView *subview1 in [cell.contentView subviews])
    {
        if ([subview1 isKindOfClass:[UILabel class]])
        {
            [subview1 removeFromSuperview];
        }
        
        else if ([subview1 isKindOfClass:[UIImageView class]])
        {
            [subview1 removeFromSuperview];
            
        }
        else
        {
            [subview1 removeFromSuperview];
            
        }
        
        
    }      //All the subvie
    
    
    
    
    //adding all view to the cell content view again after removing for no conflict
    
    [cell.contentView addSubview:share];
    [cell.contentView addSubview:salary_state];
    [cell.contentView addSubview:bank_icon];
    [cell.contentView addSubview:cellText];
    
    if([slaarycredit isEqualToString:@"1"])
    {
         [cell.contentView addSubview:date];
        
        
    }
    
    
    
    
    return cell;
    
}



//Search bar delegate methods



-(BOOL) searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    
    
    return YES;
    
}


-(BOOL) searchBarShouldEndEditing:(UISearchBar *)searchBar{
    
    
    return YES;
    
}





-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSLog(@"text is changing in the dlegate of the search bar");
    
    
    
    if (searchText.length == 0) {
        
        isFiltered =FALSE;
        
        
        
    }
    else
    {
        NSLog(@"search bar has some fo the text in it");
        
        isFiltered = true;
        
        
        
        filteredResult = [[NSMutableArray alloc]init];
        
        
        [self filterForSearchText:searchText scope:@""];
        
        
        
        [bankslist reloadData];
        
        
        
        
        
        
        
    }
    
    
    
    NSLog(@"search text is :%@", searchText);
    
    
    if ([searchText isEqualToString:@"\n"]) {
        
        NSLog(@"search text is :%@", searchText);
        
        [searchbar resignFirstResponder];
        return;
        
        
    }
    
    
    
    
    
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    [searchbar resignFirstResponder];
    
    
    
}


-(void) filterForSearchText:(NSString *)text scope:(NSString *)scope
{
    NSLog(@"filtering search for the text %@", text);
    
    
    NSLog(@"cellarray to search whole table is :%@", cellarray);
    
    
    NSPredicate *filteredPredicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", text];
    filteredResult = [NSMutableArray arrayWithArray:[cellarray filteredArrayUsingPredicate:filteredPredicate]];
    
    
   
    
    
    NSLog(@"filtered result array is :%@", filteredResult);
    
    
    
    
}


-(void) sharing :(UITapGestureRecognizer * )sender

{
    
    NSLog(@"sender view is:%@", sender);
    
    
    UIImageView *sahr = (UIImageView *)sender.view;
    
    
    
    NSLog(@"we are in the image sharing with share tag is :%d and sender tag is :%d", (int)share.tag, (int)sahr.tag);
    
    
    NSDictionary *cate_dict = [cates_array objectAtIndex:sahr.tag];
    
    NSLog(@"cate dict in sharing is :%@", cate_dict);
    
    
     NSString *slaarycredit = [NSString stringWithFormat:@"%@",[cate_dict valueForKey:@"is_salary_credited"] ];
    
     NSString  *arabicCategory = [NSString stringWithCString:[[cate_dict valueForKey:@"category_keyword"] cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
    
    
    [MyPref setValue:arabicCategory forKey:@"currentCate"];
    [MyPref synchronize];
    
    
    
    if(![slaarycredit isEqualToString:@"1"])
    {
        
    
    
    SharingImageView *sharre = [[SharingImageView alloc]initWithFrame:[UIScreen mainScreen].bounds :@"0"];
        
        sharre.salaryiscredited = @"0";
        
        
        
    [[[UIApplication sharedApplication] keyWindow].rootViewController.view  addSubview:sharre];
    
    
        
}
    else{
        SharingImageView *sharre = [[SharingImageView alloc]initWithFrame:[UIScreen mainScreen].bounds :@"1"];
        
        sharre.salaryiscredited = @"1";
        
        
        
        [[[UIApplication sharedApplication] keyWindow].rootViewController.view  addSubview:sharre];
    }
    
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    
    
    
    NSLog(@"height for footer is called");
    
    return ([[UIScreen mainScreen] bounds].size.height/100)*0.7;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    NSLog(@"setting color for the footer of the section in uitable view");
    
    
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    
    
    return view;
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        
        return ([[UIScreen mainScreen] bounds].size.height/100)*10;
    }


@end
