//
//  RecruiterList.h
//  Rawateb
//
//  Created by Neuerung on 5/19/16.
//  Copyright (c) 2016 Rohit Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface RecruiterList : UIView<UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>
{
    UITableView *bankslist;
    NSDictionary *cate_list;
    NSArray *cate_array;
    
    //bank
    NSDictionary *bank_list;
    NSArray *banks_array;
    
    
    //Gesture recognizer for the sharing is :
    
    
    UITapGestureRecognizer *sharing_recognizer;
    
    
    
    //Components for searching in uitableview
    NSMutableArray *filteredResult;
    NSMutableArray *tableData;
    
    
    UISearchBar *searchbar;
    //    UISearchController *searchDisplayController;
    
    MBProgressHUD *progress;
    
    
    //bool variable fo r changing the data in the table view
    
    
    BOOL isFiltered;
    
    
    
    
}
-(id) initWithFrame:(CGRect)frame catagory:(NSString *)cataagory :(NSDictionary *)afterbank;
@property(nonatomic, strong) NSString *catagory;
@property (nonatomic, strong) NSArray *banks_list;
@property (nonatomic, strong) NSArray *cates_array;
@property (nonatomic, strong)NSDictionary *afterbank_recruiter;
@property (nonatomic, strong)UIImageView *share;


@property (nonatomic, strong) NSArray *graduates_array;
@property (nonatomic, strong) NSMutableArray *cellarray;

@property (nonatomic)BOOL internal_calling_recruiter_flag;

@end
