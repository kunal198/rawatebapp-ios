//
//  LogInView.h
//  Rwewtab
//
//  Created by Neuerung on 4/8/16.
//  Copyright (c) 2016 Neuerung. All rights reserved.
//

#import <UIKit/UIKit.h>
extern BOOL isapplogin;
@interface LogInView : UIView<UITextViewDelegate, NSURLConnectionDataDelegate, UIWebViewDelegate>
{
    UITextView *email;
    UIImage *image;
    NSMutableData *receivedData;
    UIView *backwebview;
    
    
}

-(void) logIn;
+(void) addView :(UIView *)view;
@property (nonatomic, strong) UIWebView *webview;

@end
